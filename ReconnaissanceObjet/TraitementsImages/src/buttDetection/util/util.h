/**
 * \file        util.h
 * \brief       all the functiun used for the project
 * \author      Arif Abdek-Karim
 * \version     0.1
 * \date        Oct. 28th 2019
 *
 *
 *
 *
 */

 #ifndef UTIL_H
 #define UTIL_H


 #include <stdio.h>
 #include "def.h"
 #include "nrio.h"
 #include "nrarith.h"
 #include "nralloc.h"
 #include <math.h>
 #include <opencv2/opencv.hpp>

 using namespace cv;
 using namespace std;

 /**
  * \brief Fuction to transform a matrice into an unsigned char in grey level
  * \param imageToTransformIntoUnsigned matrice to transorfm into a unsigned char
  */
  void MatToUnsignedInGreyLvel( Mat imageToTransformIntoUnsigned, unsigned char **ucImageFromCameraInGreyLevel );

 /**
  * \brief Fuction to transform an unsigned char into a matrice in black and white
  * \param matToTransformInUC matrice to transorfm into a unsigned char
  * \param finaleMatriceTransformed matrice transformed
  */
  void UnsignedToMatInBlackAndWhite( unsigned char **ucToTransformIntoMat, Mat finaleMatriceTransformed );

 /**
  * \brief Fuction to transform a matrice into an unsigned char  in grey level
  * \param matToTransformInUC matrice to transorfm into a unsigned char
  * \param finaleMatriceTransformed matrice transformed
  */

  void UnsignedToMatInGreyLevel( unsigned char **matToTransformInUC, Mat matSobelDetection );

  void UnsignedToMatInRealGreyLevel( unsigned char **matToTransformInUC, Mat finaleMatriceTransformed );

  void UnsignedToMat(  Mat imageFinale, unsigned char **matToTransformInUC, int rows, int cols );

 /**
  * \brief Fuction to transform a matrice into an unsigned char  in grey level
  * \param matToTransformInUC matrice to transorfm into a unsigned char
  * \param finaleMatriceTransformed matrice transformed
  */
  Mat UnsignedToMatRed( unsigned char **matToTransformInUC );

 /*
  * \brief Fuction to binarise an image
  * \param matToTransformInUC matrice to transorfm into a unsigned char
  * \param finaleMatriceTransformed matrice transformed
  */
  unsigned char** binarisation( Mat imageToBinarise , int seuilBinarisation );

  void sortTab( int tab[], int sizeTab );

  int calculateRoHough( int x,int y, int theta);

  void dilatation(unsigned char **I, unsigned char**Resultat, int rows, int cols);

  void erosion( unsigned char **I, unsigned char**Resultat, int rows, int cols );

  void Openning(  Mat originImage, Mat FinalImage, int nbOpening );

  void Closing(  Mat originImage, Mat FinalImage, int nbClosing );

  void nErode ( int nbErode , Mat imEnter, Mat imExit );

  void nDilate( int nbDilate , Mat imEnter, Mat imExit );

  int min(int x,int y);
  int max(int x,int y);


 #endif
