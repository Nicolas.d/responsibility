#include <iostream>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "../util/util.h"
#include "../probahisto/probahisto.h"
#include "../colorDetection/colorDetection.h"
#include "../rectangleDetection/rectangleDetection.h"
#include <math.h>
#include <opencv2/opencv.hpp>
#include <stdbool.h>

#define SEUIL_PROBA_HISTO							  	0.25
#define SIZE_RED_RECTANGLE                 10
#define SIZE_CONCENTRATION                 30

#define SEUIL_PIXEL_ACCEPTED							250
#define tailleTabRegion                   5
#define TAILLE_MIN_REGION                 1100
#define TAILLE_MAX_REGION                 2500

static bool bIsimageEmpty( Mat image )
{
  if( image.empty() )
  {
    cout << "Path incorrectly or filename wrong, image not read!" << std::endl;
    return true;
  }
  return false;
}

static bool bIsCameraOpen( VideoCapture cap )
{
  if( !cap.isOpened() )
  {
    printf("error");
    return false;
  }
}

static Mat initMat(int rows, int cols )
{
  Mat image( rows, cols, CV_8UC3, Scalar(0,0,0) );
  return image;
}

int main( int argc, char** argv )
{
  // printf("test 1 \n" );
  Mat ImageMegotref = imread("./main/images/imref.jpg",CV_LOAD_IMAGE_COLOR);
  if( bIsimageEmpty( ImageMegotref ) )
    return -1;

  float refHistR[ 256 ] = { 0 };
  float refHistG[ 256 ] = { 0 };
  float refHistB[ 256 ] = { 0 };
  calculateAllHistogram( ImageMegotref , refHistR, refHistG, refHistB, ImageMegotref.rows, ImageMegotref.cols );

  int bplan, gplan, rplan;
  int zeros, un, deux, trois, pixel, cinq, six, sept, huit, nbPixels;
  int barycentreX = 0, barycentreY = 0, cpt = 0;

  float fps, probahisto;
  Mat ImageMegot, imageFinale, imNoirEtBlanc, imBorder, segemented;
  InfoRegion *infoRegion = (InfoRegion *)malloc(sizeof(InfoRegion));
  Point barycentre;

  clock_t t1, t2;

  VideoCapture cap(2);//open default camera
  if( bIsCameraOpen( cap ) )
    return -1;
  for(;;)
  {
    cap >> ImageMegot;
    // printf("test 2\n" );
    t1 = clock();

    imageFinale = ImageMegot.clone();
    imBorder = initMat( ImageMegot.rows, ImageMegot.cols );
    imNoirEtBlanc = initMat( ImageMegot.rows, ImageMegot.cols );
    segemented = initMat( ImageMegot.rows, ImageMegot.cols );

    nbPixels = ImageMegot.rows * ImageMegot.cols;
    for (int i = 0; i < ImageMegot.rows; ++i)
    {
      for (int j = 0; j < ImageMegot.cols; ++j)
      {
        bplan = ImageMegot.at<Vec3b>(i,j)[0];
        gplan = ImageMegot.at<Vec3b>(i,j)[1];
        rplan = ImageMegot.at<Vec3b>(i,j)[2];
        if ( rplan < SEUIL_PIXEL_ACCEPTED && gplan < SEUIL_PIXEL_ACCEPTED && bplan < SEUIL_PIXEL_ACCEPTED )
        {
          probahisto = (float) ( refHistR[ rplan ] * refHistG[ gplan ] * refHistB[ bplan ] )* nbPixels*6;
          if ( probahisto > SEUIL_PROBA_HISTO )
          {
          //	imageFinale[i][j] = 255;
            imNoirEtBlanc.at<Vec3b>(i,j)[0] = 255;
            imNoirEtBlanc.at<Vec3b>(i,j)[1] = 255;
            imNoirEtBlanc.at<Vec3b>(i,j)[2] = 255;
          }
        }
      }
    }

    // // printf("test3\n" );
    nErode ( 20 , imNoirEtBlanc, imBorder );
    nDilate( 5 , imBorder, imBorder );
    // printf(" rerere\n" );
    SegmentationEvo( imBorder, segemented, ImageMegot.rows, ImageMegot.cols );
    // printf("fgfgfg\n" );
    //
    int tmp1=0;
    int etiquette=0;
    int region[1000] = { 0 };

    // printf("test 1 \n" );
    for( int i = 0; i < segemented.rows; i++)
    {
      for( int j = 0; j < segemented.cols; j++)
      {
        int currentValue = segemented.at<Vec3b>(i,j)[0];
    	  if ( currentValue != 0  && currentValue != 255  )
        {
     		  for ( int l = 0; l < etiquette+1; ++l )
          {
    				if ( currentValue == region[l] )
            {
              tmp1 = 1;
              break;
            }
            else
            {
              tmp1 = 0;
            }
       		}

          if ( tmp1 == 0 ){
      				region[etiquette] = currentValue;
             // printf("(value etiquette = %d  )\n", currentValue );
      				etiquette++;
      		}
      	}
      }
    }

    for ( int m = 0; m < etiquette; ++m )
    {
		  informations( region[m] , infoRegion, segemented, ImageMegot );
      if ( infoRegion->taille > TAILLE_MIN_REGION  && infoRegion->taille < TAILLE_MAX_REGION   )
      {
        // RechercheEtiquette( region[m], imageFinale, segemented, segemented.rows , segemented.cols );
        barycentre.x = infoRegion->y;
        barycentre.y =infoRegion->x;
        circle(imageFinale, ( barycentre ), 30, (255, 255, 0), 1);
      // }
      }
    }

    t2 = clock();
		fps = (float)(t2-t1)/CLOCKS_PER_SEC;
    fps = fps * 1000;
    printf("FPS = %f \n", fps );
    imshow("default", ImageMegot);
    // printf("test 6 \n" );
    imshow("Red", imageFinale);
    // imshow("Nb", imNoirEtBlanc);
    // printf("test 7 \n" );
    imshow("Ettiquetage ", segemented);

  //free(region);
    if( waitKey(33) == 27 )
      break;
  }
  return 0;
}
