
/**
 * \file        util.h
 * \brief       all the functiun used for the project
 * \author      Arif Abdek-Karim
 * \version     0.1
 * \date        Oct. 28th 2019
 *
 *
 *
 *
 */

 #ifndef PROBAHISTO_H
 #define PROBAHISTO_H

 #include "../util/util.h"

 typedef struct InfoRegion{
 		int x; 					    //centre de gravité en x
 		int y; 					    //centre de gravité en y
 		int R; 					//plan r
 		int G; 					//plan g
 		int B; 					//plan b
 		float moyNiveauGris; 		//moyenne des niveaux de gris
 		int taille; 				//nombre de pixels de la region
 		int minX; 		    		// valeur min en x
 		int maxX; 		    		// valeur max en x
 		int minY; 		    		// valeur min en y
 		int maxY; 		    		//valeur max en y
 };

float calculateGrey( Mat image, int rows, int cols  );
void normalizeValueTab( float tab[256], int nbPixel );
void calculateAllHistogram( Mat image , float tabR[ 256 ], float tabG[ 256 ], float tabB[ 256 ], int rows, int cols );
void printByteValue( float tab[ 256 ], char histo );
void borderDetectionProba( Mat image, Mat border, int rows, int cols );
void SegmentationEvo( Mat imageNB, Mat segemented, int rows, int cols );
void RechercheEtiquette( int valeur, Mat imageOrigin, Mat imageSegemented,int rows, int cols );
void calculateAllHistogramYUV( Mat imageYUV , float tabY[256], float tabU[256], float tabV[256], int rows, int cols );
void drawRedRectanleMat( Mat matReturned, int x, int y, int rows, int cols );
void informations( int numeroRegion, InfoRegion *infoRegion, Mat imageGris, Mat imageDefault );
#endif
