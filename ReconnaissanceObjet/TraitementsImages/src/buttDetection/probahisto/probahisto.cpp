#include "probahisto.h"

#define SEUIL_PIXEL_ACCEPTED							250
#define SEUIL_CPT_ACCEPTED								100
#define TreshHoldBorderDecetionCOLOR			80
#define SIZE_RED_RECTANGLE								30
// g++ -o test test.cpp `pkg-config --libs opencv`



int minValue(int x,int y)
{
	if( x > y )
		return y;
	else
		return x;
}

int maxValue(int x,int y)
{
	if( x > y )
		return x;
	else
		return y;
}

void normalizeValueTab( float tab[256], int nbPixel )
{
	for (int i = 0; i < 256; ++i)
	{
		tab[i] = tab[i]/nbPixel;
	}
}

void normalizeValueTabYUV( float tab[256], int nbPixel )
{
	for (int i = 0; i < 256; ++i)
	{
		tab[i] = tab[i]/nbPixel;
	}
}

float calculateGrey( Mat image, int rows, int cols  )
{
	float moyGrey = 0;
	int bplan = 0, gplan = 0, rplan = 0, cpt = 0;

	for ( int i = 0; i < rows; ++i )
	{
		for (int j = 0; j < cols; ++j)
		{
			if ( rplan != 255 && gplan != 255 && bplan != 255 )
			{
					cpt++;
				bplan = image.at<Vec3b>(i,j)[0];
				gplan = image.at<Vec3b>(i,j)[1];
				rplan = image.at<Vec3b>(i,j)[2];
				moyGrey += ( bplan + gplan + rplan )/3;
			}
		}
	}
	moyGrey = moyGrey/cpt;
return moyGrey;
}

void calculateAllHistogram( Mat image , float tabR[ 256 ], float tabG[ 256 ], float tabB[ 256 ], int rows, int cols )
{
  int bplan = 0, gplan = 0, rplan = 0, cpt = 0;
	for ( int i = 0; i < rows; ++i )
	{
		for (int j = 0; j < cols; ++j)
		{
      bplan = image.at<Vec3b>(i,j)[0];
      gplan = image.at<Vec3b>(i,j)[1];
      rplan = image.at<Vec3b>(i,j)[2];
      //printf(" %d %d %d \n",bplan, rplan, gplan );
      // if( rplan != 234 && gplan != 18 && bplan != 178 )
      if ( rplan != 255 && gplan != 255 && bplan != 255 )
      {
        cpt++;
				tabR[ rplan ]++;
				tabG[ gplan ]++;
				tabB[ bplan ]++;
			}
		}
	}
  normalizeValueTab( tabR, cpt );
  normalizeValueTab( tabG, cpt );
  normalizeValueTab( tabB, cpt );
}

void calculateAllHistogramYUV( Mat imageYUV , float tabY[256], float tabU[256], float tabV[256], int rows, int cols )
{
  int uplan = 0, vplan = 0, yplan = 0, cpt = 0;

	for ( int i = 0; i < rows; ++i )
	{
		for (int j = 0; j < cols; ++j)
		{
      yplan = imageYUV.at<Vec3b>(i,j)[0];
      uplan = imageYUV.at<Vec3b>(i,j)[1];
      vplan = imageYUV.at<Vec3b>(i,j)[2];
      if ( yplan != 255 && uplan != 128 && vplan != 128 )
      {
        cpt++;
				tabU[ uplan + 128 ]++;
				tabV[ vplan + 128 ]++;
				tabY[ yplan ]++;
			}

		}
	}
  normalizeValueTabYUV( tabU, cpt );
  normalizeValueTabYUV( tabV, cpt );
  normalizeValueTabYUV( tabY, cpt );
}
void printByteValue( float tab[ 256 ], char histo )
{
	for ( int i = 0; i < 256; ++i )
	{
		printf(" Histogram %c, value %d = %f  \n", histo, i, tab[ i ]);
	}
}
// imageYUVNB = 234 G = 18 B = 178

void borderDetectionProba( Mat image, Mat border, int rows, int cols )
{
//  0 1 2
//	3 4 5
//	6 7 8
	int grX = 0;
	int grY = 0;
	int gr = 0;
  for( int i=1; i<rows;i++)
  {
    for( int j=1;j<cols;j++)
    {
			int zeros = image.at<Vec3b>(i-1,j-1)[0];
			int un 		= image.at<Vec3b>(i-1,j)[0];
			int deux  = image.at<Vec3b>(i-1,j+1)[0];
			int trois = image.at<Vec3b>(i,j-1)[0];
			int quatre = image.at<Vec3b>(i,j)[0];
			int cinq	= image.at<Vec3b>(i,j+1)[0];
			int six   = image.at<Vec3b>(i+1,j-1)[0];
			int sept = image.at<Vec3b>(i+1,j)[0];
			int huit  = image.at<Vec3b>(i+1,j+1)[0];
			grX = ( -zeros + deux - 2*trois + 2*cinq + six - huit  )/6;
			grY = ( -zeros - 2* un - deux + six + 2*sept + huit  )/6;
      gr = ( abs( grX ) + abs( grY ) )/2;
      if ( gr > TreshHoldBorderDecetionCOLOR ) {
        border.at<Vec3b>(i,j)[0] = 255;
				border.at<Vec3b>(i,j)[1] = 255;
				border.at<Vec3b>(i,j)[2] = 255;
      }else
				border.at<Vec3b>(i,j)[0] = 0;
				border.at<Vec3b>(i,j)[1] = 0;
				border.at<Vec3b>(i,j)[2] = 0;

   }
 }
}

void SegmentationEvo( Mat imageNB, Mat segemented, int rows, int cols ){

int attc=0,attb=0,atta=0;//initialisation des attributs
int cpt=0;
int **tab = ( int** )malloc( imageNB.rows * sizeof( int* ) );//tableau d'étiquette, segmentation
for (size_t i = 0; i < imageNB.rows; i++) {
	tab[i] = (int*) malloc(imageNB.cols * sizeof(int));
	for (size_t j = 0; j < imageNB.cols; j++) {
		tab[i][j] = 0;
	}
}
int p=0;
int tmpB,tmpA=0;//variable temporaire

// tab = imatrix( 0 ,imageNB.rows,0,imageNB.cols);
// tab =

//segmentation
for( int i=0+1; i<imageNB.rows;i++){
		for( int j=0+1;j<imageNB.cols;j++){
		//initialisation des valeurs des attibuts
			attc = imageNB.at<Vec3b>(i,j)[0];
			attb = imageNB.at<Vec3b>(i-1,j)[0];
			atta = imageNB.at<Vec3b>(i,j-1)[0];
		//Algorithme intuitif du cours
			if( attc == 255 )
			{
				if( attc == atta && attc != attb )
					tab[i][j] = tab[i][j-1];
				else if( attc == attb && attc != atta )
					tab[i][j] = tab[i-1][j];
				else if( attc != attb && attc != atta )
				{
						p++;
					//nouvelle étiquette
					tab[i][j] = p;
				}
				else if( attc == attb && attc == atta && tab[i][j-1]==tab[i-1][j] )
					tab[i][j] = tab[i-1][j];
				else if(attc == attb && attc == atta && tab[i][j-1]!=tab[i-1][j] )
				{
					tab[i][j] = tab[i-1][j];
					tmpB=tab[i-1][j];
					tmpA=tab[i][j-1];
					//retour en arriere et changement des etiquettes
					//jusqu'à i
					for( int k = 1; k < i; k++ )
					{
						for( int l = 1; l < j; l++ )
						{
             				if( tab[k][l] == tmpA )
								tab[k][l] = tmpB;
						}
					}
					//fint par les colones de la ligne i
					for( int l = 1; l < j; l++ )
					{
             			if( tab[i][l] == tmpA )
							tab[i][l] = tmpB;
					}
				}
			}
		}
}
float inter;
	for( int i = 1; i < imageNB.rows; i++ )
	{
			for( int j = 1; j < imageNB.cols; j++ ){
				inter = tab[i][j];
				segemented.at<Vec3b>(i,j)[0] = (unsigned char)((inter / p)*255);
				segemented.at<Vec3b>(i,j)[1] = (unsigned char)((inter / p)*255);
				segemented.at<Vec3b>(i,j)[2] = (unsigned char)((inter / p)*255);
			}
	}
	for ( int i = 0; i < imageNB.rows; i++) {
		free(tab[i]);
	}
	free(tab);
}

void RechercheEtiquette( int valeur, Mat imageOrigin, Mat imageSegemented,int rows, int cols )
{

int xa=0;
int xb=0;
int xc=0;
int xd=255;
int ya=255;
int yb=0;
int yc=0;
int yd=0;

	for( int k = 0; k < imageOrigin.rows; k++ ){
		for(int l = 0; l < imageOrigin.cols; l++ ){
			if( imageSegemented.at<Vec3b>(k,l)[0] == valeur ){
				//recherche de mon point A
				if(k<ya){
					ya=k;
					xa=l;
				}
				//recherche de mon point B
				if(l>xb){
					xb=l;
					yb=k;
				}
				//recherche de mon point C
				if(k>yc){
					yc=k;
					xc=l;
				}
				//recherche de mon point D
				if(l<xd){
					xd=l;
					yd=k;
				}
			}
		}	//nb_max (etiquette/nb_max)*255
	}
	//dessin de la zone qui entoure
	//zone haute
	for( int m = xd; m < xb+1; m++ )
	{
		imageOrigin.at<Vec3b>(ya,m)[0]= 0;
		imageOrigin.at<Vec3b>(ya,m)[1]= 255;
		imageOrigin.at<Vec3b>(ya,m)[2]= 0;
	}
	//zone basse
	for( int m = xd; m < xb+1; m++)
	{
		imageOrigin.at<Vec3b>(yc,m)[0]= 0;
		imageOrigin.at<Vec3b>(yc,m)[1]= 255;
		imageOrigin.at<Vec3b>(yc,m)[2]= 0;
	}
	//zone gauche
	for( int m = ya; m < yc+1; m++ )
	{
		imageOrigin.at<Vec3b>(m,xd)[0]= 0;
		imageOrigin.at<Vec3b>(m,xd)[1]= 255;
		imageOrigin.at<Vec3b>(m,xd)[2]= 0;
	}
	//zone droite
	for( int m = ya; m < yc+1; m++ )
	{
		imageOrigin.at<Vec3b>(m,xb)[0]= 0;
		imageOrigin.at<Vec3b>(m,xb)[1]= 255;
		imageOrigin.at<Vec3b>(m,xb)[2]= 0;
	}
}

void drawRedRectanleMat( Mat matReturned, int x, int y, int rows, int cols )
{

  if ( x > SIZE_RED_RECTANGLE && y > SIZE_RED_RECTANGLE && x < rows - SIZE_RED_RECTANGLE && y < cols -SIZE_RED_RECTANGLE )
  {
    int xa = x - SIZE_RED_RECTANGLE;   // A B C D
    int ya = y - SIZE_RED_RECTANGLE;

    int xb = x + SIZE_RED_RECTANGLE;
    int yb = y - SIZE_RED_RECTANGLE;

    int xc = x + SIZE_RED_RECTANGLE;
    int yc = y + SIZE_RED_RECTANGLE;

    int xd = x - SIZE_RED_RECTANGLE;
    int yd = y + SIZE_RED_RECTANGLE;

   // printf("dans draw simple avant b1\n");
  	//zone haute
  	for( int m = xa ; m < xb+1; m++ )
    {
      matReturned.at<Vec3b>(ya,m)[0]= 0;
      matReturned.at<Vec3b>(ya,m)[1]= 255;
      matReturned.at<Vec3b>(ya,m)[2]= 0;
  	}
     // printf("dans draw simple avant b2\n");

  	//zone basse
  	for( int m = xd ; m<xc+1; m++ )
    {
      matReturned.at<Vec3b>(yc,m)[0]= 0;
      matReturned.at<Vec3b>(yc,m)[1]= 255;
      matReturned.at<Vec3b>(yc,m)[2]= 0;
  	}
  	//zone gauche
     // printf("dans draw simple avant b3\n");

  	for( int m = ya; m < yd+1; m++ ){
      matReturned.at<Vec3b>(m,xd)[0]= 0;
      matReturned.at<Vec3b>(m,xd)[1]= 255;
      matReturned.at<Vec3b>(m,xd)[2]= 0;
  	}
  	//zone droite
     // printf("dans draw simple avant b4\n");

  	for( int m = yb; m < yc+1; m++ )
    {
      matReturned.at<Vec3b>(m,xb)[0]= 0;
      matReturned.at<Vec3b>(m,xb)[1]= 255;
      matReturned.at<Vec3b>(m,xb)[2]= 0;
  	}
   // printf("dans draw simple avant b5\n");
  }
}

void informations( int numeroRegion, InfoRegion *infoRegion, Mat imageGris, Mat imageDefault ){
	int i,j;
	int  nbx,nby =0;
	infoRegion->taille = 0;
	infoRegion->x=0;
	infoRegion->y=0;
	// infoRegion->R=0;
	// infoRegion->G=0;
	// infoRegion->B=0;
	infoRegion->moyNiveauGris=0;
	infoRegion->taille=0;
	// infoRegion->minX=8522455885;
	// infoRegion->minY=8989898989;
	// infoRegion->maxX=0;
	// infoRegion->maxY=0;

  for( i= 0 ; i < imageGris.rows; i++){
		for(j=0; j<imageGris.cols; j++){
			if ( imageGris.at<Vec3b>(i,j)[0] == numeroRegion ){
				infoRegion->taille++;
				nbx+=i;
				nby+=j;
				// infoRegion->minX=minValue(j,infoRegion->minX);
				// infoRegion->minY=minValue(i,infoRegion->minY);
				// infoRegion->maxX=maxValue(j,infoRegion->maxX);
				// infoRegion->maxY=maxValue(i,infoRegion->maxX);
			}
		}
	}
	infoRegion->x=nbx/(infoRegion->taille);
	infoRegion->y=nby/(infoRegion->taille);
	// // printf(" taille = %d\n", infoRegion->taille );
	//
  // for(i=0; i<imageGris.rows; i++){
	// 	for(j=0; j<imageGris.cols; j++){
	// 		if ( imageGris.at<Vec3b>(i,j)[0] == numeroRegion ){
	// 			// infoRegion->R+= imageDefault.at<Vec3b>(i,j)[2];
	// 			// infoRegion->G+= imageDefault.at<Vec3b>(i,j)[1];
	// 			// infoRegion->B+= imageDefault.at<Vec3b>(i,j)[0];
	// 			infoRegion->moyNiveauGris += ( imageDefault.at<Vec3b>(i,j)[2] + imageDefault.at<Vec3b>(i,j)[1] +
	// 		imageDefault.at<Vec3b>(i,j)[0] )/3;
	// 		}
	// 	}
	// }
	// infoRegion->R= infoRegion->R/infoRegion->taille;
	// infoRegion->G= infoRegion->G/infoRegion->taille;
	// infoRegion->B= infoRegion->B/infoRegion->taille;
	// infoRegion->moyNiveauGris = infoRegion->moyNiveauGris/infoRegion->taille;
}
