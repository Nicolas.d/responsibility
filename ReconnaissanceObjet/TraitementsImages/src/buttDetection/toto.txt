Flat profile:

Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total           
 time   seconds   seconds    calls  ms/call  ms/call  name    
 45.32      0.63     0.63 266404314     0.00     0.00  cv::Vec<unsigned char, 3>& cv::Mat::at<cv::Vec<unsigned char, 3> >(int, int)
 26.98      1.01     0.38 266404314     0.00     0.00  cv::Vec<unsigned char, 3>::operator[](int)
 13.67      1.20     0.19                             main
 12.23      1.37     0.17       32     5.31    12.24  SegmentationEvo(cv::Mat, cv::Mat, int, int)
  1.80      1.39     0.03      258     0.10     0.10  cv::MatStep::operator[](int)
  0.00      1.39     0.00      992     0.00     0.00  cv::_InputArray::init(int, void const*)
  0.00      1.39     0.00      704     0.00     0.00  cv::_InputArray::~_InputArray()
  0.00      1.39     0.00      704     0.00     0.00  cv::Size_<int>::Size_()
  0.00      1.39     0.00      611     0.00     0.00  cv::Mat::release()
  0.00      1.39     0.00      419     0.00     0.00  cv::Mat::~Mat()
  0.00      1.39     0.00      418     0.00     0.00  cv::MatSize::MatSize(int*)
  0.00      1.39     0.00      418     0.00     0.00  cv::MatStep::MatStep(unsigned long)
  0.00      1.39     0.00      416     0.00     0.00  cv::_InputArray::_InputArray(cv::Mat const&)
  0.00      1.39     0.00      289     0.00     0.00  cv::Mat::Mat()
  0.00      1.39     0.00      288     0.00     0.00  cv::_InputArray::_InputArray()
  0.00      1.39     0.00      288     0.00     0.00  cv::_OutputArray::_OutputArray(cv::Mat&)
  0.00      1.39     0.00      288     0.00     0.00  cv::_OutputArray::~_OutputArray()
  0.00      1.39     0.00      258     0.00     0.00  cv::MatStep::operator[](int) const
  0.00      1.39     0.00      160     0.00     0.00  cv::Vec<double, 4>::Vec()
  0.00      1.39     0.00      160     0.00     0.00  cv::Matx<double, 4, 1>::Matx()
  0.00      1.39     0.00      160     0.00     0.00  cv::Point_<int>::Point_(int, int)
  0.00      1.39     0.00      129     0.00     0.19  cv::Mat::Mat(cv::Mat const&)
  0.00      1.39     0.00       97     0.00     0.00  cv::String::String(char const*)
  0.00      1.39     0.00       97     0.00     0.00  cv::String::~String()
  0.00      1.39     0.00       96     0.00     0.00  cv::Scalar_<double>::Scalar_(double)
  0.00      1.39     0.00       64     0.00     0.00  cv::Scalar_<double>::all(double)
  0.00      1.39     0.00       64     0.00     0.00  cv::Scalar_<double>::Scalar_(double, double, double, double)
  0.00      1.39     0.00       64     0.00     0.00  cv::morphologyDefaultBorderValue()
  0.00      1.39     0.00       32     0.00     0.00  Openning(cv::Mat, cv::Mat, int)
  0.00      1.39     0.00       32     0.00     0.00  cv::Mat::clone() const
  0.00      1.39     0.00       32     0.00     0.00  imatrix
  0.00      1.39     0.00        9     0.00     0.00  cvflann::anyimpl::base_any_policy::~base_any_policy()
  0.00      1.39     0.00        3     0.00     0.00  normalizeValueTab(float*, int)
  0.00      1.39     0.00        1     0.00     0.00  _GLOBAL__sub_I__Z15drawRedRectanleN2cv3MatEiiii
  0.00      1.39     0.00        1     0.00     0.00  _GLOBAL__sub_I__Z17MatToUnsignedInBWN2cv3MatEPPh
  0.00      1.39     0.00        1     0.00     0.00  _GLOBAL__sub_I__Z17normalizeValueTabPfi
  0.00      1.39     0.00        1     0.00     0.00  _GLOBAL__sub_I__Z20vspecialBinarisationN2cv3MatEPPh11colorChooseS3_ii
  0.00      1.39     0.00        1     0.00     0.00  _GLOBAL__sub_I__Z8drawLine5pointS_PPhi
  0.00      1.39     0.00        1     0.00     0.05  calculateAllHistogram(cv::Mat, float*, float*, float*, int, int)
  0.00      1.39     0.00        1     0.00     0.00  __static_initialization_and_destruction_0(int, int)
  0.00      1.39     0.00        1     0.00     0.00  __static_initialization_and_destruction_0(int, int)
  0.00      1.39     0.00        1     0.00     0.00  __static_initialization_and_destruction_0(int, int)
  0.00      1.39     0.00        1     0.00     0.00  __static_initialization_and_destruction_0(int, int)
  0.00      1.39     0.00        1     0.00     0.00  __static_initialization_and_destruction_0(int, int)
  0.00      1.39     0.00        1     0.00     0.00  __static_initialization_and_destruction_0(int, int)
  0.00      1.39     0.00        1     0.00     0.00  cvflann::anyimpl::typed_base_any_policy<cv::String>::~typed_base_any_policy()
  0.00      1.39     0.00        1     0.00     0.00  cvflann::anyimpl::typed_base_any_policy<cvflann::anyimpl::empty_any>::~typed_base_any_policy()
  0.00      1.39     0.00        1     0.00     0.00  cvflann::anyimpl::typed_base_any_policy<cvflann::flann_algorithm_t>::~typed_base_any_policy()
  0.00      1.39     0.00        1     0.00     0.00  cvflann::anyimpl::typed_base_any_policy<cvflann::flann_centers_init_t>::~typed_base_any_policy()
  0.00      1.39     0.00        1     0.00     0.00  cvflann::anyimpl::typed_base_any_policy<char const*>::~typed_base_any_policy()
  0.00      1.39     0.00        1     0.00     0.00  cvflann::anyimpl::typed_base_any_policy<bool>::~typed_base_any_policy()
  0.00      1.39     0.00        1     0.00     0.00  cvflann::anyimpl::typed_base_any_policy<float>::~typed_base_any_policy()
  0.00      1.39     0.00        1     0.00     0.00  cvflann::anyimpl::typed_base_any_policy<int>::~typed_base_any_policy()
  0.00      1.39     0.00        1     0.00     0.00  cvflann::anyimpl::typed_base_any_policy<unsigned int>::~typed_base_any_policy()
  0.00      1.39     0.00        1     0.00     0.00  cv::Mat::empty() const
  0.00      1.39     0.00        1     0.00     0.00  cv::Mat::total() const

 %         the percentage of the total running time of the
time       program used by this function.

cumulative a running sum of the number of seconds accounted
 seconds   for by this function and those listed above it.

 self      the number of seconds accounted for by this
seconds    function alone.  This is the major sort for this
           listing.

calls      the number of times this function was invoked, if
           this function is profiled, else blank.

 self      the average number of milliseconds spent in this
ms/call    function per call, if this function is profiled,
	   else blank.

 total     the average number of milliseconds spent in this
ms/call    function and its descendents per call, if this
	   function is profiled, else blank.

name       the name of the function.  This is the minor sort
           for this listing. The index shows the location of
	   the function in the gprof listing. If the index is
	   in parenthesis it shows where it would appear in
	   the gprof listing if it were to be printed.

Copyright (C) 2012-2018 Free Software Foundation, Inc.

Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.

		     Call graph (explanation follows)


granularity: each sample hit covers 2 byte(s) for 0.72% of 1.39 seconds

index % time    self  children    called     name
                                                 <spontaneous>
[1]    100.0    0.19    1.20                 main [1]
                0.49    0.00 207624126/266404314     cv::Vec<unsigned char, 3>& cv::Mat::at<cv::Vec<unsigned char, 3> >(int, int) [2]
                0.17    0.22      32/32          SegmentationEvo(cv::Mat, cv::Mat, int, int) [3]
                0.29    0.00 207624126/266404314     cv::Vec<unsigned char, 3>::operator[](int) [4]
                0.00    0.03     129/129         cv::Mat::Mat(cv::Mat const&) [6]
                0.00    0.00       1/1           calculateAllHistogram(cv::Mat, float*, float*, float*, int, int) [7]
                0.00    0.00     259/419         cv::Mat::~Mat() [971]
                0.00    0.00      97/97          cv::String::String(char const*) [983]
                0.00    0.00      97/97          cv::String::~String() [984]
                0.00    0.00      97/289         cv::Mat::Mat() [975]
                0.00    0.00      96/288         cv::_OutputArray::_OutputArray(cv::Mat&) [977]
                0.00    0.00      96/288         cv::_OutputArray::~_OutputArray() [978]
                0.00    0.00      96/416         cv::_InputArray::_InputArray(cv::Mat const&) [974]
                0.00    0.00      96/704         cv::_InputArray::~_InputArray() [968]
                0.00    0.00      32/32          cv::Mat::clone() const [990]
                0.00    0.00      32/32          Openning(cv::Mat, cv::Mat, int) [989]
                0.00    0.00       1/1           cv::Mat::empty() const [1013]
-----------------------------------------------
                0.00    0.00   12636/266404314     calculateAllHistogram(cv::Mat, float*, float*, float*, int, int) [7]
                0.14    0.00 58767552/266404314     SegmentationEvo(cv::Mat, cv::Mat, int, int) [3]
                0.49    0.00 207624126/266404314     main [1]
[2]     45.3    0.63    0.00 266404314         cv::Vec<unsigned char, 3>& cv::Mat::at<cv::Vec<unsigned char, 3> >(int, int) [2]
-----------------------------------------------
                0.17    0.22      32/32          main [1]
[3]     28.2    0.17    0.22      32         SegmentationEvo(cv::Mat, cv::Mat, int, int) [3]
                0.14    0.00 58767552/266404314     cv::Vec<unsigned char, 3>& cv::Mat::at<cv::Vec<unsigned char, 3> >(int, int) [2]
                0.08    0.00 58767552/266404314     cv::Vec<unsigned char, 3>::operator[](int) [4]
                0.00    0.00      32/32          imatrix [8]
-----------------------------------------------
                0.00    0.00   12636/266404314     calculateAllHistogram(cv::Mat, float*, float*, float*, int, int) [7]
                0.08    0.00 58767552/266404314     SegmentationEvo(cv::Mat, cv::Mat, int, int) [3]
                0.29    0.00 207624126/266404314     main [1]
[4]     27.0    0.38    0.00 266404314         cv::Vec<unsigned char, 3>::operator[](int) [4]
-----------------------------------------------
                0.03    0.00     258/258         cv::Mat::Mat(cv::Mat const&) [6]
[5]      1.8    0.03    0.00     258         cv::MatStep::operator[](int) [5]
-----------------------------------------------
                0.00    0.03     129/129         main [1]
[6]      1.8    0.00    0.03     129         cv::Mat::Mat(cv::Mat const&) [6]
                0.03    0.00     258/258         cv::MatStep::operator[](int) [5]
                0.00    0.00     258/258         cv::MatStep::operator[](int) const [979]
                0.00    0.00     129/418         cv::MatSize::MatSize(int*) [972]
                0.00    0.00     129/418         cv::MatStep::MatStep(unsigned long) [973]
-----------------------------------------------
                0.00    0.00       1/1           main [1]
[7]      0.0    0.00    0.00       1         calculateAllHistogram(cv::Mat, float*, float*, float*, int, int) [7]
                0.00    0.00   12636/266404314     cv::Vec<unsigned char, 3>& cv::Mat::at<cv::Vec<unsigned char, 3> >(int, int) [2]
                0.00    0.00   12636/266404314     cv::Vec<unsigned char, 3>::operator[](int) [4]
                0.00    0.00       3/3           normalizeValueTab(float*, int) [992]
-----------------------------------------------
                0.00    0.00      32/32          SegmentationEvo(cv::Mat, cv::Mat, int, int) [3]
[8]      0.0    0.00    0.00      32         imatrix [8]
-----------------------------------------------
                0.00    0.00     288/992         cv::_InputArray::_InputArray() [976]
                0.00    0.00     288/992         cv::_OutputArray::_OutputArray(cv::Mat&) [977]
                0.00    0.00     416/992         cv::_InputArray::_InputArray(cv::Mat const&) [974]
[967]    0.0    0.00    0.00     992         cv::_InputArray::init(int, void const*) [967]
-----------------------------------------------
                0.00    0.00      96/704         main [1]
                0.00    0.00     288/704         cv::_OutputArray::~_OutputArray() [978]
                0.00    0.00     320/704         Openning(cv::Mat, cv::Mat, int) [989]
[968]    0.0    0.00    0.00     704         cv::_InputArray::~_InputArray() [968]
-----------------------------------------------
                0.00    0.00     288/704         cv::_InputArray::_InputArray() [976]
                0.00    0.00     416/704         cv::_InputArray::_InputArray(cv::Mat const&) [974]
[969]    0.0    0.00    0.00     704         cv::Size_<int>::Size_() [969]
-----------------------------------------------
                0.00    0.00     611/611         cv::Mat::~Mat() [971]
[970]    0.0    0.00    0.00     611         cv::Mat::release() [970]
-----------------------------------------------
                0.00    0.00     160/419         Openning(cv::Mat, cv::Mat, int) [989]
                0.00    0.00     259/419         main [1]
[971]    0.0    0.00    0.00     419         cv::Mat::~Mat() [971]
                0.00    0.00     611/611         cv::Mat::release() [970]
-----------------------------------------------
                0.00    0.00     129/418         cv::Mat::Mat(cv::Mat const&) [6]
                0.00    0.00     289/418         cv::Mat::Mat() [975]
[972]    0.0    0.00    0.00     418         cv::MatSize::MatSize(int*) [972]
-----------------------------------------------
                0.00    0.00     129/418         cv::Mat::Mat(cv::Mat const&) [6]
                0.00    0.00     289/418         cv::Mat::Mat() [975]
[973]    0.0    0.00    0.00     418         cv::MatStep::MatStep(unsigned long) [973]
-----------------------------------------------
                0.00    0.00      96/416         main [1]
                0.00    0.00     320/416         Openning(cv::Mat, cv::Mat, int) [989]
[974]    0.0    0.00    0.00     416         cv::_InputArray::_InputArray(cv::Mat const&) [974]
                0.00    0.00     416/704         cv::Size_<int>::Size_() [969]
                0.00    0.00     416/992         cv::_InputArray::init(int, void const*) [967]
-----------------------------------------------
                0.00    0.00      32/289         cv::Mat::clone() const [990]
                0.00    0.00      97/289         main [1]
                0.00    0.00     160/289         Openning(cv::Mat, cv::Mat, int) [989]
[975]    0.0    0.00    0.00     289         cv::Mat::Mat() [975]
                0.00    0.00     289/418         cv::MatSize::MatSize(int*) [972]
                0.00    0.00     289/418         cv::MatStep::MatStep(unsigned long) [973]
-----------------------------------------------
                0.00    0.00     288/288         cv::_OutputArray::_OutputArray(cv::Mat&) [977]
[976]    0.0    0.00    0.00     288         cv::_InputArray::_InputArray() [976]
                0.00    0.00     288/704         cv::Size_<int>::Size_() [969]
                0.00    0.00     288/992         cv::_InputArray::init(int, void const*) [967]
-----------------------------------------------
                0.00    0.00      32/288         cv::Mat::clone() const [990]
                0.00    0.00      96/288         main [1]
                0.00    0.00     160/288         Openning(cv::Mat, cv::Mat, int) [989]
[977]    0.0    0.00    0.00     288         cv::_OutputArray::_OutputArray(cv::Mat&) [977]
                0.00    0.00     288/288         cv::_InputArray::_InputArray() [976]
                0.00    0.00     288/992         cv::_InputArray::init(int, void const*) [967]
-----------------------------------------------
                0.00    0.00      32/288         cv::Mat::clone() const [990]
                0.00    0.00      96/288         main [1]
                0.00    0.00     160/288         Openning(cv::Mat, cv::Mat, int) [989]
[978]    0.0    0.00    0.00     288         cv::_OutputArray::~_OutputArray() [978]
                0.00    0.00     288/704         cv::_InputArray::~_InputArray() [968]
-----------------------------------------------
                0.00    0.00     258/258         cv::Mat::Mat(cv::Mat const&) [6]
[979]    0.0    0.00    0.00     258         cv::MatStep::operator[](int) const [979]
-----------------------------------------------
                0.00    0.00      64/160         cv::Scalar_<double>::Scalar_(double, double, double, double) [987]
                0.00    0.00      96/160         cv::Scalar_<double>::Scalar_(double) [985]
[980]    0.0    0.00    0.00     160         cv::Vec<double, 4>::Vec() [980]
                0.00    0.00     160/160         cv::Matx<double, 4, 1>::Matx() [981]
-----------------------------------------------
                0.00    0.00     160/160         cv::Vec<double, 4>::Vec() [980]
[981]    0.0    0.00    0.00     160         cv::Matx<double, 4, 1>::Matx() [981]
-----------------------------------------------
                0.00    0.00     160/160         Openning(cv::Mat, cv::Mat, int) [989]
[982]    0.0    0.00    0.00     160         cv::Point_<int>::Point_(int, int) [982]
-----------------------------------------------
                0.00    0.00      97/97          main [1]
[983]    0.0    0.00    0.00      97         cv::String::String(char const*) [983]
-----------------------------------------------
                0.00    0.00      97/97          main [1]
[984]    0.0    0.00    0.00      97         cv::String::~String() [984]
-----------------------------------------------
                0.00    0.00      96/96          Openning(cv::Mat, cv::Mat, int) [989]
[985]    0.0    0.00    0.00      96         cv::Scalar_<double>::Scalar_(double) [985]
                0.00    0.00      96/160         cv::Vec<double, 4>::Vec() [980]
-----------------------------------------------
                0.00    0.00      64/64          cv::morphologyDefaultBorderValue() [988]
[986]    0.0    0.00    0.00      64         cv::Scalar_<double>::all(double) [986]
                0.00    0.00      64/64          cv::Scalar_<double>::Scalar_(double, double, double, double) [987]
-----------------------------------------------
                0.00    0.00      64/64          cv::Scalar_<double>::all(double) [986]
[987]    0.0    0.00    0.00      64         cv::Scalar_<double>::Scalar_(double, double, double, double) [987]
                0.00    0.00      64/160         cv::Vec<double, 4>::Vec() [980]
-----------------------------------------------
                0.00    0.00      64/64          Openning(cv::Mat, cv::Mat, int) [989]
[988]    0.0    0.00    0.00      64         cv::morphologyDefaultBorderValue() [988]
                0.00    0.00      64/64          cv::Scalar_<double>::all(double) [986]
-----------------------------------------------
                0.00    0.00      32/32          main [1]
[989]    0.0    0.00    0.00      32         Openning(cv::Mat, cv::Mat, int) [989]
                0.00    0.00     320/416         cv::_InputArray::_InputArray(cv::Mat const&) [974]
                0.00    0.00     320/704         cv::_InputArray::~_InputArray() [968]
                0.00    0.00     160/160         cv::Point_<int>::Point_(int, int) [982]
                0.00    0.00     160/289         cv::Mat::Mat() [975]
                0.00    0.00     160/288         cv::_OutputArray::_OutputArray(cv::Mat&) [977]
                0.00    0.00     160/288         cv::_OutputArray::~_OutputArray() [978]
                0.00    0.00     160/419         cv::Mat::~Mat() [971]
                0.00    0.00      96/96          cv::Scalar_<double>::Scalar_(double) [985]
                0.00    0.00      64/64          cv::morphologyDefaultBorderValue() [988]
-----------------------------------------------
                0.00    0.00      32/32          main [1]
[990]    0.0    0.00    0.00      32         cv::Mat::clone() const [990]
                0.00    0.00      32/289         cv::Mat::Mat() [975]
                0.00    0.00      32/288         cv::_OutputArray::_OutputArray(cv::Mat&) [977]
                0.00    0.00      32/288         cv::_OutputArray::~_OutputArray() [978]
-----------------------------------------------
                0.00    0.00       1/9           cvflann::anyimpl::typed_base_any_policy<cvflann::anyimpl::empty_any>::~typed_base_any_policy() [1005]
                0.00    0.00       1/9           cvflann::anyimpl::typed_base_any_policy<char const*>::~typed_base_any_policy() [1008]
                0.00    0.00       1/9           cvflann::anyimpl::typed_base_any_policy<int>::~typed_base_any_policy() [1011]
                0.00    0.00       1/9           cvflann::anyimpl::typed_base_any_policy<float>::~typed_base_any_policy() [1010]
                0.00    0.00       1/9           cvflann::anyimpl::typed_base_any_policy<bool>::~typed_base_any_policy() [1009]
                0.00    0.00       1/9           cvflann::anyimpl::typed_base_any_policy<cvflann::flann_algorithm_t>::~typed_base_any_policy() [1006]
                0.00    0.00       1/9           cvflann::anyimpl::typed_base_any_policy<cvflann::flann_centers_init_t>::~typed_base_any_policy() [1007]
                0.00    0.00       1/9           cvflann::anyimpl::typed_base_any_policy<unsigned int>::~typed_base_any_policy() [1012]
                0.00    0.00       1/9           cvflann::anyimpl::typed_base_any_policy<cv::String>::~typed_base_any_policy() [1004]
[991]    0.0    0.00    0.00       9         cvflann::anyimpl::base_any_policy::~base_any_policy() [991]
-----------------------------------------------
                0.00    0.00       3/3           calculateAllHistogram(cv::Mat, float*, float*, float*, int, int) [7]
[992]    0.0    0.00    0.00       3         normalizeValueTab(float*, int) [992]
-----------------------------------------------
                0.00    0.00       1/1           __libc_csu_init [1251]
[993]    0.0    0.00    0.00       1         _GLOBAL__sub_I__Z15drawRedRectanleN2cv3MatEiiii [993]
                0.00    0.00       1/1           __static_initialization_and_destruction_0(int, int) [1001]
-----------------------------------------------
                0.00    0.00       1/1           __libc_csu_init [1251]
[994]    0.0    0.00    0.00       1         _GLOBAL__sub_I__Z17MatToUnsignedInBWN2cv3MatEPPh [994]
                0.00    0.00       1/1           __static_initialization_and_destruction_0(int, int) [1002]
-----------------------------------------------
                0.00    0.00       1/1           __libc_csu_init [1251]
[995]    0.0    0.00    0.00       1         _GLOBAL__sub_I__Z17normalizeValueTabPfi [995]
                0.00    0.00       1/1           __static_initialization_and_destruction_0(int, int) [1003]
-----------------------------------------------
                0.00    0.00       1/1           __libc_csu_init [1251]
[996]    0.0    0.00    0.00       1         _GLOBAL__sub_I__Z20vspecialBinarisationN2cv3MatEPPh11colorChooseS3_ii [996]
                0.00    0.00       1/1           __static_initialization_and_destruction_0(int, int) [999]
-----------------------------------------------
                0.00    0.00       1/1           __libc_csu_init [1251]
[997]    0.0    0.00    0.00       1         _GLOBAL__sub_I__Z8drawLine5pointS_PPhi [997]
                0.00    0.00       1/1           __static_initialization_and_destruction_0(int, int) [1000]
-----------------------------------------------
                                   1             __static_initialization_and_destruction_0(int, int) [998]
                0.00    0.00       1/1           __libc_csu_init [1251]
[998]    0.0    0.00    0.00       1+1       __static_initialization_and_destruction_0(int, int) [998]
                                   1             __static_initialization_and_destruction_0(int, int) [998]
-----------------------------------------------
                0.00    0.00       1/1           _GLOBAL__sub_I__Z20vspecialBinarisationN2cv3MatEPPh11colorChooseS3_ii [996]
[999]    0.0    0.00    0.00       1         __static_initialization_and_destruction_0(int, int) [999]
-----------------------------------------------
                0.00    0.00       1/1           _GLOBAL__sub_I__Z8drawLine5pointS_PPhi [997]
[1000]   0.0    0.00    0.00       1         __static_initialization_and_destruction_0(int, int) [1000]
-----------------------------------------------
                0.00    0.00       1/1           _GLOBAL__sub_I__Z15drawRedRectanleN2cv3MatEiiii [993]
[1001]   0.0    0.00    0.00       1         __static_initialization_and_destruction_0(int, int) [1001]
-----------------------------------------------
                0.00    0.00       1/1           _GLOBAL__sub_I__Z17MatToUnsignedInBWN2cv3MatEPPh [994]
[1002]   0.0    0.00    0.00       1         __static_initialization_and_destruction_0(int, int) [1002]
-----------------------------------------------
                0.00    0.00       1/1           _GLOBAL__sub_I__Z17normalizeValueTabPfi [995]
[1003]   0.0    0.00    0.00       1         __static_initialization_and_destruction_0(int, int) [1003]
-----------------------------------------------
                0.00    0.00       1/1           cvflann::anyimpl::big_any_policy<cv::String>::~big_any_policy() [1139]
[1004]   0.0    0.00    0.00       1         cvflann::anyimpl::typed_base_any_policy<cv::String>::~typed_base_any_policy() [1004]
                0.00    0.00       1/9           cvflann::anyimpl::base_any_policy::~base_any_policy() [991]
-----------------------------------------------
                0.00    0.00       1/1           cvflann::anyimpl::big_any_policy<cvflann::anyimpl::empty_any>::~big_any_policy() [1148]
[1005]   0.0    0.00    0.00       1         cvflann::anyimpl::typed_base_any_policy<cvflann::anyimpl::empty_any>::~typed_base_any_policy() [1005]
                0.00    0.00       1/9           cvflann::anyimpl::base_any_policy::~base_any_policy() [991]
-----------------------------------------------
                0.00    0.00       1/1           cvflann::anyimpl::big_any_policy<cvflann::flann_algorithm_t>::~big_any_policy() [1157]
[1006]   0.0    0.00    0.00       1         cvflann::anyimpl::typed_base_any_policy<cvflann::flann_algorithm_t>::~typed_base_any_policy() [1006]
                0.00    0.00       1/9           cvflann::anyimpl::base_any_policy::~base_any_policy() [991]
-----------------------------------------------
                0.00    0.00       1/1           cvflann::anyimpl::big_any_policy<cvflann::flann_centers_init_t>::~big_any_policy() [1166]
[1007]   0.0    0.00    0.00       1         cvflann::anyimpl::typed_base_any_policy<cvflann::flann_centers_init_t>::~typed_base_any_policy() [1007]
                0.00    0.00       1/9           cvflann::anyimpl::base_any_policy::~base_any_policy() [991]
-----------------------------------------------
                0.00    0.00       1/1           cvflann::anyimpl::small_any_policy<char const*>::~small_any_policy() [1176]
[1008]   0.0    0.00    0.00       1         cvflann::anyimpl::typed_base_any_policy<char const*>::~typed_base_any_policy() [1008]
                0.00    0.00       1/9           cvflann::anyimpl::base_any_policy::~base_any_policy() [991]
-----------------------------------------------
                0.00    0.00       1/1           cvflann::anyimpl::small_any_policy<bool>::~small_any_policy() [1185]
[1009]   0.0    0.00    0.00       1         cvflann::anyimpl::typed_base_any_policy<bool>::~typed_base_any_policy() [1009]
                0.00    0.00       1/9           cvflann::anyimpl::base_any_policy::~base_any_policy() [991]
-----------------------------------------------
                0.00    0.00       1/1           cvflann::anyimpl::small_any_policy<float>::~small_any_policy() [1194]
[1010]   0.0    0.00    0.00       1         cvflann::anyimpl::typed_base_any_policy<float>::~typed_base_any_policy() [1010]
                0.00    0.00       1/9           cvflann::anyimpl::base_any_policy::~base_any_policy() [991]
-----------------------------------------------
                0.00    0.00       1/1           cvflann::anyimpl::small_any_policy<int>::~small_any_policy() [1203]
[1011]   0.0    0.00    0.00       1         cvflann::anyimpl::typed_base_any_policy<int>::~typed_base_any_policy() [1011]
                0.00    0.00       1/9           cvflann::anyimpl::base_any_policy::~base_any_policy() [991]
-----------------------------------------------
                0.00    0.00       1/1           cvflann::anyimpl::small_any_policy<unsigned int>::~small_any_policy() [1212]
[1012]   0.0    0.00    0.00       1         cvflann::anyimpl::typed_base_any_policy<unsigned int>::~typed_base_any_policy() [1012]
                0.00    0.00       1/9           cvflann::anyimpl::base_any_policy::~base_any_policy() [991]
-----------------------------------------------
                0.00    0.00       1/1           main [1]
[1013]   0.0    0.00    0.00       1         cv::Mat::empty() const [1013]
                0.00    0.00       1/1           cv::Mat::total() const [1014]
-----------------------------------------------
                0.00    0.00       1/1           cv::Mat::empty() const [1013]
[1014]   0.0    0.00    0.00       1         cv::Mat::total() const [1014]
-----------------------------------------------

 This table describes the call tree of the program, and was sorted by
 the total amount of time spent in each function and its children.

 Each entry in this table consists of several lines.  The line with the
 index number at the left hand margin lists the current function.
 The lines above it list the functions that called this function,
 and the lines below it list the functions this one called.
 This line lists:
     index	A unique number given to each element of the table.
		Index numbers are sorted numerically.
		The index number is printed next to every function name so
		it is easier to look up where the function is in the table.

     % time	This is the percentage of the `total' time that was spent
		in this function and its children.  Note that due to
		different viewpoints, functions excluded by options, etc,
		these numbers will NOT add up to 100%.

     self	This is the total amount of time spent in this function.

     children	This is the total amount of time propagated into this
		function by its children.

     called	This is the number of times the function was called.
		If the function called itself recursively, the number
		only includes non-recursive calls, and is followed by
		a `+' and the number of recursive calls.

     name	The name of the current function.  The index number is
		printed after it.  If the function is a member of a
		cycle, the cycle number is printed between the
		function's name and the index number.


 For the function's parents, the fields have the following meanings:

     self	This is the amount of time that was propagated directly
		from the function into this parent.

     children	This is the amount of time that was propagated from
		the function's children into this parent.

     called	This is the number of times this parent called the
		function `/' the total number of times the function
		was called.  Recursive calls to the function are not
		included in the number after the `/'.

     name	This is the name of the parent.  The parent's index
		number is printed after it.  If the parent is a
		member of a cycle, the cycle number is printed between
		the name and the index number.

 If the parents of the function cannot be determined, the word
 `<spontaneous>' is printed in the `name' field, and all the other
 fields are blank.

 For the function's children, the fields have the following meanings:

     self	This is the amount of time that was propagated directly
		from the child into the function.

     children	This is the amount of time that was propagated from the
		child's children to the function.

     called	This is the number of times the function called
		this child `/' the total number of times the child
		was called.  Recursive calls by the child are not
		listed in the number after the `/'.

     name	This is the name of the child.  The child's index
		number is printed after it.  If the child is a
		member of a cycle, the cycle number is printed
		between the name and the index number.

 If there are any cycles (circles) in the call graph, there is an
 entry for the cycle-as-a-whole.  This entry shows who called the
 cycle (as parents) and the members of the cycle (as children.)
 The `+' recursive calls entry shows the number of function calls that
 were internal to the cycle, and the calls entry for each member shows,
 for that member, how many times it was called from other members of
 the cycle.

Copyright (C) 2012-2018 Free Software Foundation, Inc.

Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.

Index by function name

 [993] _GLOBAL__sub_I__Z15drawRedRectanleN2cv3MatEiiii [977] cv::_OutputArray::_OutputArray(cv::Mat&) [985] cv::Scalar_<double>::Scalar_(double)
 [994] _GLOBAL__sub_I__Z17MatToUnsignedInBWN2cv3MatEPPh [978] cv::_OutputArray::~_OutputArray() [988] cv::morphologyDefaultBorderValue()
 [995] _GLOBAL__sub_I__Z17normalizeValueTabPfi [2] cv::Vec<unsigned char, 3>& cv::Mat::at<cv::Vec<unsigned char, 3> >(int, int) [991] cvflann::anyimpl::base_any_policy::~base_any_policy()
 [996] _GLOBAL__sub_I__Z20vspecialBinarisationN2cv3MatEPPh11colorChooseS3_ii [970] cv::Mat::release() [1004] cvflann::anyimpl::typed_base_any_policy<cv::String>::~typed_base_any_policy()
 [997] _GLOBAL__sub_I__Z8drawLine5pointS_PPhi [6] cv::Mat::Mat(cv::Mat const&) [1005] cvflann::anyimpl::typed_base_any_policy<cvflann::anyimpl::empty_any>::~typed_base_any_policy()
   [3] SegmentationEvo(cv::Mat, cv::Mat, int, int) [975] cv::Mat::Mat() [1006] cvflann::anyimpl::typed_base_any_policy<cvflann::flann_algorithm_t>::~typed_base_any_policy()
 [992] normalizeValueTab(float*, int) [971] cv::Mat::~Mat() [1007] cvflann::anyimpl::typed_base_any_policy<cvflann::flann_centers_init_t>::~typed_base_any_policy()
   [7] calculateAllHistogram(cv::Mat, float*, float*, float*, int, int) [980] cv::Vec<double, 4>::Vec() [1008] cvflann::anyimpl::typed_base_any_policy<char const*>::~typed_base_any_policy()
 [998] __static_initialization_and_destruction_0(int, int) [4] cv::Vec<unsigned char, 3>::operator[](int) [1009] cvflann::anyimpl::typed_base_any_policy<bool>::~typed_base_any_policy()
 [999] __static_initialization_and_destruction_0(int, int) [981] cv::Matx<double, 4, 1>::Matx() [1010] cvflann::anyimpl::typed_base_any_policy<float>::~typed_base_any_policy()
 [1000] __static_initialization_and_destruction_0(int, int) [969] cv::Size_<int>::Size_() [1011] cvflann::anyimpl::typed_base_any_policy<int>::~typed_base_any_policy()
 [1001] __static_initialization_and_destruction_0(int, int) [982] cv::Point_<int>::Point_(int, int) [1012] cvflann::anyimpl::typed_base_any_policy<unsigned int>::~typed_base_any_policy()
 [1002] __static_initialization_and_destruction_0(int, int) [983] cv::String::String(char const*) [990] cv::Mat::clone() const
 [1003] __static_initialization_and_destruction_0(int, int) [984] cv::String::~String() [1013] cv::Mat::empty() const
 [989] Openning(cv::Mat, cv::Mat, int) [972] cv::MatSize::MatSize(int*) [1014] cv::Mat::total() const
 [967] cv::_InputArray::init(int, void const*) [973] cv::MatStep::MatStep(unsigned long) [979] cv::MatStep::operator[](int) const
 [974] cv::_InputArray::_InputArray(cv::Mat const&) [5] cv::MatStep::operator[](int) [8] imatrix
 [976] cv::_InputArray::_InputArray() [986] cv::Scalar_<double>::all(double) [1] main
 [968] cv::_InputArray::~_InputArray() [987] cv::Scalar_<double>::Scalar_(double, double, double, double)
