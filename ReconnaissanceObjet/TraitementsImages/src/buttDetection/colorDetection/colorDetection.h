/**
 * \file        colorDetection.h
 * \brief       detect a butt with some the color destriptor
 * \author      Arif Abdek-Karim
 * \version     0.1
 * \date        Oct. 28th 2019
 *
 *
 *
 *
 */


 #ifndef COLORDETECTION_H
 #define COLORDETECTION_H

 #include "../util/util.h"

#define whiteButtR                                  255
#define whiteButtG                                  255
#define whiteButtB                                  255
#define orangeButtR                                 191
#define orangeButtG                                 153
#define orangeButtB                                 78

typedef struct colorChoose{
  int bplan;
  int gplan;
  int rplan;
}colorChoose_t;

typedef struct CigaretteButt{
  int CigaretteButtRplan;
  int CigaretteButtGplan;
  int CigaretteButtBplan;
  float averageColor;

}CigaretteButt_t;

typedef struct descriptorButt
{
  int x;
  int y;
  int size;
}descriptorButt_t;

/**
 * \brief Fuction to binarise image with  cigarette Butt color only
 * \param originImage the original image from the caméra
 * \param binarisedImage the value from the image binarised
 */
 float colorDistance( unsigned char ** currentPixel, colorChoose_t color );

 /**
  * \brief Fuction to binarise image with  cigarette Butt color only
  * \param originImage the original image from the caméra
  * \param binarisedImage the value from the image binarised
  */
  void vspecialBinarisation( Mat originImage, unsigned char ** binarisedImage, colorChoose_t colorWhite, colorChoose_t colorOrange,  int rows, int cols );

  void hsvBinarisation( Mat originImage,  unsigned char ** binarisedImage, colorChoose_t colorOrange, int rows, int cols );
  /**
   * \brief Fuction to supress the noise ( use opening and closure )
   * \param binarisedImage the value from the image binarised
   * \param noiseSuppressedImage the value from the image without noise
   */
   void noiseSuppression( Mat originImage, Mat FinalImage, int nbOpening, int nbClosing );

   void imageValue( unsigned char **segmentedImage, int tab[10], int rows, int cols );
   void zoneDescriptor( unsigned char **segmentedImage, int numberRegion, descriptorButt_t* butt, int rows, int cols );

        /**
   * \brief segmentation function to separate distinct zones
   * \param noiseSuppressedImage the value from the image without noise
   * \param binarisedImage the value from the image segemented
   */
  void segmentationEvo( unsigned char ** noiseSuppressedImage, unsigned char ** segementateImage, int rows, int cols );

  void borderDetection2( Mat image, unsigned char ** borderDetection, int rows, int cols );
  /**
   * \brief check if the region can be considered like a cigarette butt
   * \param numberRegion the region to check
   * \param cigaretteDescriptor description of the cigaretteButt Type
   */
  bool bIsRegionCigaretteButt( int numberRegion, CigaretteButt_t cigaretteDescriptor );

  void drawRectanle( int valeur, Mat matReturned, unsigned char **segmentedImage,int rows, int cols );

  void drawSmpleRectanle( int valeur, Mat matReturned, int x, int y , unsigned char **segmentedImage,int rows, int cols );

  #endif
