/**
 * \file        colorDetection.cpp
 * \brief
 * \author      Arif Abdek-Karim
 * \version     0.1
 * \date        Oct. 28th 2019
 *
 *
 *
 *
 */


 #include "colorDetection.h"

 #define COLOR_WHITE_DISTANCE_TRESHOLD                   125
 #define COLOR_ORANGE_DISTANCE_TRESHOLD_RGB              60
 #define COLOR_ORANGE_DISTANCE_TRESHOLD_HSV              500
 #define COLOR_ORANGE_DISTANCE_TRESHOLD_HSV_V            20

 #define N_MAX                                           500
 #define TreshHoldBorderDecetionCOLOR                    20
 #define SIZE_RED_RECTANGLE                              30


 void vspecialBinarisation( Mat originImage, unsigned char ** binarisedImage, colorChoose_t colorWhite, colorChoose_t colorOrange, int rows, int cols )
{
  int distanceColorWhite = 0;
  int distanceColorOrange = 0;

  for( int i=0; i <= rows; i++)
  {
    for(int j=0; j <= cols; j++)
    {
      if ( i == 0 || j == 0 || i == rows || j == cols) {
        binarisedImage[i][j] = 0;
      }
      int b = originImage.at<Vec3b>(i,j)[0];
      int g = originImage.at<Vec3b>(i,j)[1];
      int r = originImage.at<Vec3b>(i,j)[2];
    //  distanceColorWhite  = sqrt( pow( ( b - colorWhite.bplan  ),2) + pow( ( g - colorWhite.gplan ),2 ) + pow( ( r - colorWhite.rplan  ),2) );
      distanceColorOrange = sqrt( pow( ( b - colorOrange.bplan  ),2) + pow( ( g - colorOrange.gplan ),2 ) + pow( ( r - colorOrange.rplan  ),2) );

      if( distanceColorOrange < COLOR_ORANGE_DISTANCE_TRESHOLD_RGB )
        binarisedImage[i][j] = (b+g+r) /3;
      else
        binarisedImage[i][j] = 0;
    }
  }
}

void hsvBinarisation( Mat originImage,  unsigned char ** binarisedImage, colorChoose_t colorOrange, int rows, int cols )
{
  for( int i = 0; i <= rows; i++)
  {
    for(int j = 0; j <= cols; j++)
    {
      int h = originImage.at<Vec3b>(i,j)[0];
      int s = originImage.at<Vec3b>(i,j)[1];
      int v = originImage.at<Vec3b>(i,j)[2];
      int distanceColorOrange = sqrt( pow( ( h - colorOrange.bplan  ),2) + pow( ( s - colorOrange.gplan ),2 ) );
    //  printf("distance = %d, et v = %d\n", distanceColorOrange, v);
      if ( distanceColorOrange < 40 )
      {
        if( v < 90 && v > 50 )
        {
          binarisedImage[i][j] = 255;
        }
        else
          binarisedImage[i][j] = 0;

      }
      else
        binarisedImage[i][j] = 0;

    }
  }
}
void noiseSuppression( Mat originImage, Mat FinalImage, int nbOpening, int nbClosing )
{
  Openning( originImage, FinalImage, nbOpening );
  Closing( originImage, FinalImage, nbClosing );
}

void segmentationEvo( unsigned char ** noiseSuppressedImage, unsigned char ** segmentateImage, int rows, int cols )
{

  unsigned char **segmentedImage;
  int abdel[1000];//image binarisé
  int i,j;
  int attc=0,attb=0,atta=0;//initialisation des attributs
  int tmp=0;
  int k,l;
  int p=0;
  int tmpB,tmpA=0;//variable temporaire


  //allocation memoire
  segmentedImage=bmatrix(0,rows,0,cols);

  for(i=0;i< rows ;i++)
  {
  	for(j=0;j<cols ;j++)
    {
  	   segmentedImage[i][j] = 0;
  	   segmentateImage[i][j]=0;
    }
  }

  for(int m=0;m<N_MAX;m++)
  {
  	abdel[m]=m;
  }

  for( i=1; i<rows;i++){
  		for( j=1;j<cols;j++){
  		//initialisation des valeurs des attibuts
  			attc=noiseSuppressedImage[i][j];
  			attb=noiseSuppressedImage[i-1][j];
  			atta=noiseSuppressedImage[i][j-1];
  		//Algorithme evolué du cours
  			if(attc==255){
  				if(attc == atta && attc != attb)
  					segmentedImage[i][j]=segmentedImage[i][j-1];
  				else if(attc == attb && attc != atta)
  					segmentedImage[i][j]=abdel[segmentedImage[i-1][j]];
  				else if(attc != attb && attc != atta){
  						p++;
  					//nouvelle étiquette
  					segmentedImage[i][j]=p;
  				}
  				else if( attc == attb && attc == atta && segmentedImage[i][j-1]==segmentedImage[i-1][j])
  					segmentedImage[i][j]=segmentedImage[i-1][j];
  				else if(attc == attb && attc == atta && segmentedImage[i][j-1]!=segmentedImage[i-1][j]){
  					segmentedImage[i][j]=min(abdel[segmentedImage[i-1][j]],segmentedImage[i][j-1]);
  					abdel[segmentedImage[i][j]]=segmentedImage[i][j];
  					abdel[segmentedImage[i][j-1]]=segmentedImage[i][j];
  					abdel[max(abdel[segmentedImage[i-1][j]],segmentedImage[i][j-1])]=segmentedImage[i][j];
  				}
  			}


  		}
  }
  int cpt=0;
  for(int m=1;m<p;m++){
  	if(abdel[m]==m){
  		cpt++;
  		abdel[m]=cpt;
  	}else{
  		abdel[m]= abdel[abdel[m]];
  	}
  }
  float inter;
	for( i=0+1; i<rows;i++){
	   for( j=0+1;j<cols;j++){
  	    segmentedImage[i][j]=abdel[segmentedImage[i][j]];
  			inter =segmentedImage[i][j];
  			segmentateImage[i][j]= ( byte )((inter / cpt)*255);
  	 }
  }
}

void borderDetection2( Mat image, unsigned char ** borderDetection, int rows, int cols )
{
  unsigned char ** tabTransition, **tabTransitionX, **tabTransitionY, **binarisedImage;
  tabTransition	  =	bmatrix( 0, rows, 0, cols);
  tabTransitionX	=	bmatrix( 0, rows, 0, cols);
  tabTransitionY	=	bmatrix( 0, rows, 0, cols);
  binarisedImage = bmatrix( 0,rows, 0, cols );
  binarisation( image , binarisedImage, 2 );

  for( int i=1; i<rows;i++)
  {
    for( int j=1;j<cols;j++)
    {
        tabTransitionX[i][j] = ( binarisedImage[i-1][j-1] - binarisedImage[i-1][j+1] +
        2*binarisedImage[i][j-1] -2*binarisedImage[i][j+1]+
        binarisedImage[i+1][j-1] - binarisedImage[i+1][j+1])/6;

        tabTransitionY[i][j] = ( binarisedImage[i-1][j-1] + binarisedImage[i-1][j] + binarisedImage[i-1][j+1] -
          binarisedImage[i-1][j-1] - binarisedImage[i-1][j] - binarisedImage[i-1][j+1] ) /6;


        tabTransition[i][j] = ( abs( tabTransitionX[i][j] )+abs( tabTransitionY[i][j] ) )/2;
        if ( tabTransition[i][j] > TreshHoldBorderDecetionCOLOR ) {
          borderDetection[i][j] = 255;
        }else
          borderDetection[i][j] = 0;
   }
 }
 free_bmatrix(tabTransition,0,rows,0,cols);
 free_bmatrix(tabTransitionX,0,rows,0,cols);
 free_bmatrix(tabTransitionY,0,rows,0,cols);
 free_bmatrix(binarisedImage,0,rows,0,cols);

}


void imageValue( unsigned char **segmentedImage, int tab[10], int rows, int cols )
{
  int cpt = 0;
  bool contains = false;
  for( int k=1; k<rows ;k++)
  {
    for( int l=1; l<cols; l++)
    {
      int valueImage = segmentedImage[k][l];
      if ( valueImage != 0  && valueImage != 255)
      {
        for ( int i = 0; i < cpt; i++ ) {
          if ( valueImage == tab[i] )
          {
            contains = true;
          }
          else
            contains == false;
        }
        if ( contains == false) {
          tab[cpt] = valueImage;
          cpt++;
        }
      }
      contains = false;
    }
  }
}

void zoneDescriptor( unsigned char **segmentedImage, int numberRegion, descriptorButt_t* butt, int rows, int cols )
{
  int size = 0, x = 0, y = 0;
  butt->x = 0;
  butt->y = 0;
  butt->size = 0;

  for( int k=0; k<rows ;k++)
  {
		for( int l=0; l<cols; l++)
    {
      if( segmentedImage[k][l] == numberRegion )
      {
        butt->size++;
        x += k;
        y += l;
      }
    }
  }
  if ( butt->size != 0 ) {
    butt->x = x / (butt->size);
    butt->y = y / (butt->size);
  }
  else
  {
    butt->x = 0;
    butt->y = 0;
  }
  // printf(" mon x en sortie = %d et mon y = %d\n",   butt->x,   butt->y );

}
//foncion qui en fonction de la valeur d'étiquette donné en entrée va renvoyer le centre d'interêt et la zone d'intrêt
void drawRectanle( int valeur, Mat matReturned, unsigned char **segmentedImage,int rows, int cols )
{
    int xa=0;
    int xb=0;
    int xc=0;
    int xd=255;
    int ya=255;
    int yb=0;
    int yc=0;
    int yd=0;

	for( int k=0; k<rows ;k++)
  {
		for( int l=0; l<cols; l++)
    {
			if( segmentedImage[k][l] == valeur )
      {
				//recherche de mon point A
				if( k<ya )
        {
					ya=k;
					xa=l;
				}
				//recherche de mon point B
				if( l>xb )
        {
					xb=l;
					yb=k;
				}
				//recherche de mon point C
				if( k>yc )
        {
					yc=k;
					xc=l;
				}
				//recherche de mon point D
				if( l<xd )
        {
					xd=l;
					yd=k;
				}
			}
		}
	}	//nb_max (etiquette/nb_max)*255

	//zone haute
	for( int m=xd;m<xb+1;m++)
  {
    matReturned.at<Vec3b>(ya,m)[0]= 0;
    matReturned.at<Vec3b>(ya,m)[1]= 0;
    matReturned.at<Vec3b>(ya,m)[2]= 255;
	}
	//zone basse
	for(int m=xd;m<xb+1;m++)
  {
    matReturned.at<Vec3b>(yc,m)[0]= 0;
    matReturned.at<Vec3b>(yc,m)[1]= 0;
    matReturned.at<Vec3b>(yc,m)[2]= 255;
	}
	//zone gauche
	for(int m=ya;m<yc+1;m++){
    matReturned.at<Vec3b>(m,xd)[0]= 0;
    matReturned.at<Vec3b>(m,xd)[1]= 0;
    matReturned.at<Vec3b>(m,xd)[2]= 255;
	}
	//zone droite
	for(int m=ya;m<yc+1;m++)
  {
    matReturned.at<Vec3b>(m,xb)[0]= 0;
    matReturned.at<Vec3b>(m,xb)[1]= 0;
    matReturned.at<Vec3b>(m,xb)[2]= 255;
	}
}

void drawSmpleRectanle( int valeur, Mat matReturned, int x, int y , unsigned char **segmentedImage,int rows, int cols )
{

  if ( x > SIZE_RED_RECTANGLE && y > SIZE_RED_RECTANGLE && x < rows - SIZE_RED_RECTANGLE && y < cols -SIZE_RED_RECTANGLE )
  {
    int xa = x - SIZE_RED_RECTANGLE;   // A B C D
    int ya = y - SIZE_RED_RECTANGLE;

    int xb = x + SIZE_RED_RECTANGLE;
    int yb = y - SIZE_RED_RECTANGLE;

    int xc = x + SIZE_RED_RECTANGLE;
    int yc = y + SIZE_RED_RECTANGLE;

    int xd = x - SIZE_RED_RECTANGLE;
    int yd = y + SIZE_RED_RECTANGLE;

   printf("dans draw simple avant b1\n");
  	//zone haute
  	for( int m = xa ; m < xb+1; m++ )
    {
      matReturned.at<Vec3b>(ya,m)[0]= 0;
      matReturned.at<Vec3b>(ya,m)[1]= 0;
      matReturned.at<Vec3b>(ya,m)[2]= 255;
  	}
     printf("dans draw simple avant b2\n");

  	//zone basse
  	for( int m = xd ; m<xc+1; m++ )
    {
      matReturned.at<Vec3b>(yc,m)[0]= 0;
      matReturned.at<Vec3b>(yc,m)[1]= 0;
      matReturned.at<Vec3b>(yc,m)[2]= 255;
  	}
  	//zone gauche
     printf("dans draw simple avant b3\n");

  	for( int m = ya; m < yd+1; m++ ){
      matReturned.at<Vec3b>(m,xd)[0]= 0;
      matReturned.at<Vec3b>(m,xd)[1]= 0;
      matReturned.at<Vec3b>(m,xd)[2]= 255;
  	}
  	//zone droite
     printf("dans draw simple avant b4\n");

  	for( int m = yb; m < yc+1; m++ )
    {
      matReturned.at<Vec3b>(m,xb)[0]= 0;
      matReturned.at<Vec3b>(m,xb)[1]= 0;
      matReturned.at<Vec3b>(m,xb)[2]= 255;
  	}
   printf("dans draw simple avant b5\n");
  }

}

// void segmentationEvoMat( Mat matimage, Mat matToReturn, int rows, int cols )
// {
//
//   unsigned char **segmentedImage, **test;
//   int abdel[1000];//image binarisé
//   int i,j;
//   int attc=0,attb=0,atta=0;//initialisation des attributs
//   int tmp=0;
//   int k,l;
//   int p=0;
//   int tmpB,tmpA=0;//variable temporaire
//
//
//   //allocation memoire
//   segmentedImage=bmatrix(0,rows,0,cols);
//   test=bmatrix(0,rows,0,cols);
//
//   MatToUnsignedInGreyLvel( matimage, segmentedImage );
//
//   for(i=0;i< rows ;i++)
//   {
//   	for(j=0;j<cols ;j++)
//     {
//   	   segmentedImage[i][j] = 0;
//   	   test[i][j]=0;
//     }
//   }
//
//   for(int m=0;m<N_MAX;m++)
//   {
//   	abdel[m]=m;
//   }
//
//   for( i=1; i<rows;i++){
//   		for( j=1;j<cols;j++){
//   		//initialisation des valeurs des attibuts
//   			attc=noiseSuppressedImage[i][j];
//   			attb=noiseSuppressedImage[i-1][j];
//   			atta=noiseSuppressedImage[i][j-1];
//   		//Algorithme evolué du cours
//   			if(attc==255){
//   				if(attc == atta && attc != attb)
//   					segmentedImage[i][j]=segmentedImage[i][j-1];
//   				else if(attc == attb && attc != atta)
//   					segmentedImage[i][j]=abdel[segmentedImage[i-1][j]];
//   				else if(attc != attb && attc != atta){
//   						p++;
//   					//nouvelle étiquette
//   					segmentedImage[i][j]=p;
//   				}
//   				else if( attc == attb && attc == atta && segmentedImage[i][j-1]==segmentedImage[i-1][j])
//   					segmentedImage[i][j]=segmentedImage[i-1][j];
//   				else if(attc == attb && attc == atta && segmentedImage[i][j-1]!=segmentedImage[i-1][j]){
//   					segmentedImage[i][j]=min(abdel[segmentedImage[i-1][j]],segmentedImage[i][j-1]);
//   					abdel[segmentedImage[i][j]]=segmentedImage[i][j];
//   					abdel[segmentedImage[i][j-1]]=segmentedImage[i][j];
//   					abdel[max(abdel[segmentedImage[i-1][j]],segmentedImage[i][j-1])]=segmentedImage[i][j];
//   				}
//   			}
//
//
//   		}
//   }
//   int cpt=0;
//   for(int m=1;m<p;m++){
//   	if(abdel[m]==m){
//   		cpt++;
//   		abdel[m]=cpt;
//   	}else{
//   		abdel[m]= abdel[abdel[m]];
//   	}
//   }
//   float inter;
// 	for( i=0+1; i<rows;i++){
// 	   for( j=0+1;j<cols;j++){
//   	    segmentedImage[i][j]=abdel[segmentedImage[i][j]];
//   			inter =segmentedImage[i][j];
//   			test[i][j]= ( byte )((inter / cpt)*255);
//   	 }
//   }
//   UnsignedToMatInBlackAndWhite( test,  matToReturn);
// }
