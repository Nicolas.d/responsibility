/**
 * \file        rectangleDetection.cpp
 * \brief       detect special forms in an imageFromCamera
 * \author      Arif Abdek-Karim
 * \version     0.1
 * \date        Oct. 28th 2019
 *
 *
 *
 *
 */
 #include "rectangleDetection.h"

 #define TreshHoldBorderDecetion                  120
 #define TreshHoldBorderDecetionLowHysteresis     80
 #define TreshHoldBorderDecetionHighHysteresis    90
 #define TreshHoldHoughRo                         80
 #define TreshHoldSegment                         100

void drawLine( point_t p1, point_t p2 , unsigned char **imageWithRectangle, int maxSize)
{
  for (size_t i = 0 ; i < maxSize; i++)
  {
    imageWithRectangle[p2.y][p1.x] = 255;
  }
}

int mediane( unsigned char **imageReduceNoise, int mediane, int rows, int cols )
{
  int tab[9];
  for( int i=1; i<rows;i++){
    for(int  j=1;j<cols;j++){
      tab[0] = imageReduceNoise[i-1][j-1];
      tab[1] = imageReduceNoise[i-1][j];
      tab[2] = imageReduceNoise[i-1][j+1];
      tab[3] = imageReduceNoise[i][j-1];
      tab[4] = imageReduceNoise[i][j];
      tab[5] = imageReduceNoise[i][j+1];
      tab[6] = imageReduceNoise[i+1][j-1];
      tab[7] = imageReduceNoise[i+1][j];
      tab[8] = imageReduceNoise[i+1][j+1];
      sortTab( tab, 9 );
      mediane = tab[5];
    }
  }
 return mediane;
}
void ucGaussianFilter( unsigned char **gaussianFilter ,unsigned char **imageFromCamera , int rows,int  cols)
 {
	for( int i=1; i<rows;i++){
		for(int  j=1;j<cols;j++){
		    gaussianFilter[i][j]  = ( imageFromCamera[i-1][j-1] + 2*imageFromCamera[i-1][j] + imageFromCamera[i-1][j+1] +
         2*imageFromCamera[i][j-1] + 4*imageFromCamera[i][j] + 2*imageFromCamera[i][j+1]
          + imageFromCamera[i+1][j-1] + 2*imageFromCamera[i+1][j] + imageFromCamera[i+1][j+1] )/9;
		}
	}
 }

void ucBorderDetection( Mat imageFromCamera , unsigned char **borderDetection )
{
   int i,j,max=0;

   unsigned char **ucImageFromCameraInGreyLevel;
   unsigned char **tabTransition, **tabTransitionX, **tabTransitionY;
   unsigned char **gaussianFilter, **gaussianFilterX, **gaussianFilterY ;

   int nrh = imageFromCamera.rows;
   int nch = imageFromCamera.cols;
   int treshHoldLow = 0;
   int treshHoldHigh = 0;
   tabTransition	=	bmatrix( 0, nrh, 0, nch);
   tabTransitionX	=	bmatrix( 0, nrh, 0, nch);
   tabTransitionY	=	bmatrix( 0, nrh, 0, nch);
   gaussianFilter	=	bmatrix		( 0, nrh, 0, nch);
   gaussianFilterX	=	bmatrix		( 0, nrh, 0, nch);
   gaussianFilterY	=	bmatrix		( 0, nrh, 0, nch);
   ucImageFromCameraInGreyLevel	=	bmatrix( 0, nrh, 0, nch);

   MatToUnsignedInGreyLvel( imageFromCamera, ucImageFromCameraInGreyLevel );
   //ucGaussianFilter( gaussianFilter ,ucImageFromCameraInGreyLevel , nrh , nch);
   for( i=1; i<nrh;i++)
   {
     for( j=1;j<nch;j++)
     {
       if ( ucImageFromCameraInGreyLevel[i][j] > 110 )
       {
         gaussianFilterX[i][j] = ( ucImageFromCameraInGreyLevel[i-1][j-1] + 2* ucImageFromCameraInGreyLevel[i-1][j] + ucImageFromCameraInGreyLevel[i-1][j+1]
         + 2*ucImageFromCameraInGreyLevel[i][j-1] + 4*ucImageFromCameraInGreyLevel[i][j] + 2*ucImageFromCameraInGreyLevel[i][j+1]
         +ucImageFromCameraInGreyLevel[i+1][j-1] + 2* ucImageFromCameraInGreyLevel[i+1][j] + ucImageFromCameraInGreyLevel[i+1][j+1] )/16;

         gaussianFilterY[i][j] = ( ucImageFromCameraInGreyLevel[i-1][j-1] + 2* ucImageFromCameraInGreyLevel[i-1][j] + ucImageFromCameraInGreyLevel[i-1][j+1]
         + 2*ucImageFromCameraInGreyLevel[i][j-1] + 4*ucImageFromCameraInGreyLevel[i][j] + 2*ucImageFromCameraInGreyLevel[i][j+1]
         +ucImageFromCameraInGreyLevel[i+1][j-1] + 2* ucImageFromCameraInGreyLevel[i+1][j] + ucImageFromCameraInGreyLevel[i+1][j+1] )/16;

         gaussianFilter[i][j]=( abs( gaussianFilterX[i][j] ) + abs( gaussianFilterY[i][j] ) )/2;

       }
     }
   }
   for( i=1; i<nrh;i++)
   {
     for( j=1;j<nch;j++)
     {
       if (gaussianFilter[i][j] < 150 && gaussianFilter[i][j] >100 ) {

         tabTransitionX[i][j] = ( gaussianFilter[i-1][j-1] - gaussianFilter[i-1][j+1] +
         2*gaussianFilter[i][j-1] -2*gaussianFilter[i][j+1]+
         gaussianFilter[i+1][j-1] - gaussianFilter[i+1][j+1])/9;

         tabTransitionY[i][j] = ( gaussianFilter[i-1][j-1] + gaussianFilter[i-1][j] + gaussianFilter[i-1][j+1] -
           gaussianFilter[i-1][j-1] - gaussianFilter[i-1][j] - gaussianFilter[i-1][j+1] ) /9;


         tabTransition[i][j] = ( abs( tabTransitionX[i][j] )+abs( tabTransitionY[i][j] ) )/2;
         if(tabTransition[i][j] > max )
            max=tabTransition[i][j];

         // if (tabTransition[i][j] > TreshHoldBorderDecetion) {
         //    borderDetection[i][j] = 255;
         // }else
         //    borderDetection[i][j]= 0;         /* code */
         // }else
         //    borderDetection[i][j]= 0;
      }
    }
  }
  treshHoldHigh = (max * TreshHoldBorderDecetionHighHysteresis)/100;
  treshHoldLow  = (max * TreshHoldBorderDecetionLowHysteresis )/100;

  for( i=3; i<nrh-3;i++)
  {
    for( j=3;j<nch-3;j++)
    {
      if(tabTransition[i][j] < treshHoldLow)
        borderDetection[i][j]= 0;
      if(tabTransition[i][j] > treshHoldHigh)
        borderDetection[i][j]= 255;
      if(tabTransition[i][j] > treshHoldLow && tabTransition[i][j] < treshHoldHigh)
      {
        if(  tabTransition[i-1][j-1] == 255 || tabTransition[i-1][j] == 255 || tabTransition[i-1][j+1] == 255 ||
           tabTransition[i][j-1] == 255 || tabTransition[i][j] == 255 || tabTransition[i][j+1] == 255 ||
         tabTransition[i+1][j-1] == 255 || tabTransition[i+1][j] == 255 || tabTransition[i+1][j+1] == 255  )
          borderDetection[i][j] = 255;
      }
    }
  }
  //imageFromCamera = UnsignedToMatInGreyLevel( ucImageFromCameraInGreyLevel, imageFromCamera);
  imageFromCamera = UnsignedToMatInGreyLevel( borderDetection, imageFromCamera);

  free_bmatrix(tabTransition,0,nrh,0,nch);
  free_bmatrix(tabTransitionX,0,nrh,0,nch);
  free_bmatrix(tabTransitionY,0,nrh,0,nch);
  free_bmatrix(gaussianFilterX,0,nrh,0,nch);
  free_bmatrix(gaussianFilter,0,nrh,0,nch);
  free_bmatrix(gaussianFilterY,0,nrh,0,nch);
  free_bmatrix(ucImageFromCameraInGreyLevel, 0,nrh,0,nch);
}

 unsigned char **ucHoughDetection( unsigned char **borderTable, unsigned char **imageWithHough, int rows , int cols )
 {
  houghRectancgleDetected_t *houghRectancgleDetected;
  int i,j,x1,y1,x2,y2;
  point_t p1,p2;
  int rhomax = (int)sqrt( pow(cols,2) + pow(rows,2) );
  double max;
  int nmax=0;
  int thetaDetect,rhoDetect;
  int ximage,yimage;
  int **tab_acc;
  unsigned char **imageWithRectangle;

  houghRectancgleDetected = (houghRectancgleDetected_t*) malloc( sizeof(houghRectancgleDetected_t) );
  imageWithRectangle	=	bmatrix( 0, rows, 0, cols);

  if(rows <=cols)
  {
    max=cols;
  }
  else
  {
    max=rows;
  }
  printf("avant ro\n");

  int angle,roh;
  for( angle = 0;angle < 180; angle++ )
  {
    for( i = 0; i < rows; i++ )
    {
      for( j = 0; j < cols; j++ )
      {
          if( borderTable[i][j] == 255 )
          {
            roh = calculateRoHough(j, i, angle );
            houghRectancgleDetected[angle].teta = angle;
            houghRectancgleDetected[angle].ro = roh;
            houghRectancgleDetected[angle].val++;
            //tab_acc[angle][roh+rhomax]++;
        }
      }
    }
  }
  printf("apres ro\n");

  for( i = 0; i < max; i++ )
  {
    if( houghRectancgleDetected[i].val > TreshHoldHoughRo )
    {
      printf("apres calcul  px\n");

    }
    else
      printf("apres calcul  px\n");

    //   imageWithRectangle[yimage][i] = 0;

  }
  printf("apres  tracer\n");

 free_bmatrix( imageWithRectangle, 0, rows, 0, cols);
// free(houghRectancgleDetected);
}

void vHough2(unsigned char **tabBorderDetection,  Mat imageFinale ,int rows, int cols )
{
	unsigned char **tab_acc;
	int i,j;
	int rhomax = (int)sqrt( pow(cols,2) + pow(rows,2) );
	double max;
	int nmax=0;
	int thetaDetect,rhoDetect;
	int ximage,yimage;
  tab_acc =bmatrix(0, 180, 0, 2*rhomax+1);
  houghRectancgleDetected_t *houghRectancgleDetected;
  houghRectancgleDetected = (houghRectancgleDetected_t*) malloc( sizeof(houghRectancgleDetected_t) );
//  tab_acc= ( int** )malloc( sizeof( int* ) );
	if( rows <= cols )
		max=cols;
	else
		max=rows;

printf("test hough\n" );
//calcul de ro pour chaque pixel et chaque angles
//CHANGE LE PAS SI BESOINS
	int angle,roh;
	for(angle=0;angle<180;angle=angle+10){
		for(i=0;i<rows;i++){
			for(j=0;j<cols;j++){
				if(tabBorderDetection[i][j]==255){
          roh = calculateRoHough(j, i, angle );
					tab_acc[angle][roh+rhomax]++;
          houghRectancgleDetected[angle].ro=roh;
				}
			}
		}
	}
  printf("apres ro hough\n" );

//recherche du maximum
	for(i=0;i<2*rhomax+1;i++){
		for(j=0;j<180;j++){
			if(nmax<=tab_acc[j][i]){
				nmax=tab_acc[j][i];
				thetaDetect=j;
				rhoDetect=i;
			}
		}
	}
  printf("apres  ro max\n"  );

// //creation de l'image ppm finale
// 	for( i=0; i<rows;i++){
// 		for( j=0;j<cols;j++){
// 			if ( tabBorderDetection[i][j]==255 ){
// 				imageFinale.at<Vec3b>(i,j)[0]= 255;
// 				imageFinale.at<Vec3b>(i,j)[1]= 255;
// 				imageFinale.at<Vec3b>(i,j)[2]= 255;
// 			}else{
// 				imageFinale.at<Vec3b>(i,j)[0]= 0;
// 				imageFinale.at<Vec3b>(i,j)[1]= 0;
// 				imageFinale.at<Vec3b>(i,j)[2]= 0;
// 			}
// 		}
// 	}

  for( angle = 0; angle < 180 ; angle = angle + 10 )
  {
    int ro = houghRectancgleDetected[angle].ro;
		for( i=0; i < ro+rhomax; i++)
    {
      printf("test tab %d et angle %d \n" ,i,angle);
      if ( tab_acc[angle][i] > TreshHoldSegment )
      {
        //int teta = houghRectancgleDetected[angle].teta;
      //  int ro = ;
        yimage=( ro-rhomax-( i*sin( angle*M_PI/180 ) ))/cos( angle*M_PI/180 );

        imageFinale.at<Vec3b>(yimage,i )[0]= 0;
        imageFinale.at<Vec3b>(yimage,i )[1]= 0;
        imageFinale.at<Vec3b>(yimage,i )[2]= 255;
        printf("kldfkdlfkdf\n" );
      }
      else
      {
        imageFinale.at<Vec3b>(i,j)[0]= 0;
        imageFinale.at<Vec3b>(i,j)[1]= 0;
        imageFinale.at<Vec3b>(i,j)[2]= 0;
        printf("fdfdfdfdffd\n" );
      }
    }
  }
  free_bmatrix( tab_acc, 0, rows, 0, cols);

  //free(tab_acc);
}

// unsigned char **ucHoughCircleDetection( unsigned char **borderTable, unsigned char **imageWithHough, int rows , int cols )
// {
//   unsigned char **tab_acc;
// 	int i,j;
// 	int rhomax = (int)sqrt( pow(cols,2) + pow(rows,2) );
// 	double max;
// 	int nmax=0;
// 	int thetaDetect,rhoDetect;
// 	int ximage,yimage;
//   tab_acc =bmatrix(0, 180, 0, 2*rhomax+1);
//
//   CircleDetection_t *circledetect = ( CircleDetection_t* ) malloc( sizeof( CircleDetection_t ) );
//
// 	if( rows <= cols )
// 		max=cols;
// 	else
// 		max=rows;
//
//     printf("test hough\n" );
//
//   free_bmatrix( tab_acc, 0, rows, 0, cols);
// }
