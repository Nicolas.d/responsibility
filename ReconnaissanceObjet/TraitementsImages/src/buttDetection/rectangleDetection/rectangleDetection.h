/**
 * \file        rectangleDetection.h
 * \brief       detect special forms in an image
 * \author      Arif Abdek-Karim
 * \version     0.1
 * \date        Oct. 28th 2019
 *
 *
 *
 *
 */

#ifndef RECTANGLEDETECTION_H
#define RECTANGLEDETECTION_H


#include "util.h"

//g++ -o rectangleDetection rectangleDetection.cpp -w -lm $(pkg-config --cflags --libs opencv) nrio.c nrarith.c nralloc.c -lm
typedef struct point
{
       int x;
       int y;
}point_t;

/*________________________________________________________________________________*/
typedef struct houghRectancgleDetected
{
       float teta; // pour la valeur d'angle de -•/2 à 2•
       float ro;   // pour la valeur de la distance entre 0 et •(longueur_image²+largueur_image²)
       int val;    // nombre de fois où l'équation de droite est définie dans l'image
}houghRectancgleDetected_t;

typedef struct
{
  int a;    // abcisse centre du cercle
  int b;    // ordonnee centre du cercle
  float r;  // rayon du cercle
  int val;  // nombre de fois où l'équation de cercle est définie dans l'image
}CircleDetection_t;

/**
 * \brief
 * \param
 * \param
 * \param
 * \param
 */
void drawLine( point_t p1, point_t p2 , unsigned char **imageWithRectangle, int maxSize);

/**
 * \brief
 * \param
 * \param
 * \param
 * \param
 * \return
 */
int mediane( unsigned char **imageReduceNoise, int mediane, int rows, int cols );

/**
 * \brief Fuction to apply a gaussian filter
 * \param gaussianDetection the tab with gauss filter
 * \param imageFromCamera the original image issued from the caméra
 */
  void ucGaussianFilter( unsigned char **gaussianDetection , Mat imageFromCamera, int rows, int cols);

/**
 * \brief Fuction to detect the border on an image
 * \param imageFromCamera the original image issued from the caméra
 * \param borderDetection the final image with only the border
 */
  void ucBorderDetection( Mat imageFromCamera, unsigned char **borderDetection );

 /**
  * \brief Hough Generalised detection
  * \param borderTable all the value from the border detection
  * \return rectangleTable
  */
  unsigned char **ucHoughDetection( unsigned char **borderTable, unsigned char **imageWithHough, int rows , int cols );

  void vHough2( unsigned char **tabBorderDetection,  Mat imageFinale, int rows, int cols );

  unsigned char **ucHoughCircleDetection( unsigned char **borderTable, unsigned char **imageWithHough, int rows , int cols );

  void erosion( unsigned char **I, unsigned char**Resultat, int rows, int cols );

  void dilatation( unsigned char **I, unsigned char**Resultat, int rows, int cols );

#endif
