#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
using namespace cv;
using namespace std;

int main( void )
{
    // Declare the output variables
    Mat dst, cdst, cdstP,image;
    VideoCapture cap(0);//open default camera
  	if(!cap.isOpened()){
  		printf("error");
  		return -1;
  	}
    for(;;){
      // CHANGE la resolution  640/480 par defaut
      cap.set(CV_CAP_PROP_FRAME_WIDTH,320);
      cap.set(CV_CAP_PROP_FRAME_HEIGHT,240);
      cap >> image;
      // Edge detection
      Canny(image, dst, 50, 200, 3);
      // Copy edges to the images that will display the results in BGR
      cvtColor(dst, cdst, COLOR_GRAY2BGR);
      cdstP = cdst.clone();
      // Standard Hough Line Transform
      vector<Vec2f> lines; // will hold the results of the detection
      HoughLines(dst, lines, 1, 10*CV_PI/180, 70, 0, 0 ); // runs the actual detection
      // Draw the lines
      for( size_t i = 0; i < lines.size(); i++ )
      {
          float rho = lines[i][0], theta = lines[i][1];
          Point pt1, pt2;
          double a = cos(theta), b = sin(theta);
          double x0 = a*rho, y0 = b*rho;
          pt1.x = cvRound(x0 + 1000*(-b));
          pt1.y = cvRound(y0 + 1000*(a));
          pt2.x = cvRound(x0 - 1000*(-b));
          pt2.y = cvRound(y0 - 1000*(a));
          line( cdst, pt1, pt2, Scalar(0,0,255), 3, LINE_AA);
      }
      // // Probabilistic Line Transform
      // vector<Vec4i> linesP; // will hold the results of the detection
      // HoughLinesP(dst, linesP, 1, 10*CV_PI/180, 55, 50, 10 ); // runs the actual detection
      // // Draw the lines
      // for( size_t i = 0; i < linesP.size(); i++ )
      // {
      //     Vec4i l = linesP[i];
      //     line( cdstP, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(0,0,255), 3, LINE_AA);
      // }
      // Show results
    //  imshow("Source", src);
      //imshow("Detected Lines (in red) - Standard Hough Line Transform", cdst);
      imshow("Detected Lines (in red) - Probabilistic Line Transform", cdst);
      // Wait and Exit
      if(waitKey(33) == 27 ) break;
    }
    return 0;
}
