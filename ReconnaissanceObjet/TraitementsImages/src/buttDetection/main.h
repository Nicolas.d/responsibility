/**
 * \file        cigaretteButtDetection.h
 * \brief       detect special forms in an image
 * \author      Arif Abdek-Karim
 * \version     0.1
 * \date        Oct. 28th 2019
 *
 *
 *
 *
 */


 #ifndef CIGARETTEBUTTDETECTION_H
 #define CIGARETTEBUTTDETECTION_H

// #include "util.h"
 #include "rectangleDetection.h"


 /**
  * \brief Fuction to detect only the cigarette Butt
  * \param ucRectangleTable the table with all the rectangle value
  * \return cigaretteButtTable
  */
  unsigned char** ucCigaretteButtDetection(   unsigned char ** ucRectangleTable );

  #endif
