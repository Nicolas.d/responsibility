/**
 * \file        util.cpp
 * \brief       all the functiun used for the project
 * \author      Arif Abdek-Karim
 * \version     0.1
 * \date        Oct. 28th 2019
 *
 *
 *
 *
 */


 #include "util.h"
#define TAB_POINT_SIZE      10

void nErode ( int nbErode , Mat imEnter, Mat imExit )
{
  for( int t = 0; t < nbErode; t++ )
  {
    erode( imEnter, imExit, Mat(), Point(-1, -1) );
  }
}

void nDilate( int nbDilate , Mat imEnter, Mat imExit )
{
  dilate( imEnter, imExit, Mat(), Point(-1, -1), nbDilate, 1, 1 );

}

void normalizeValueTab( float tab[256], int nbPixel )
{
	for (int i = 0; i < 256; ++i)
	{
		tab[i] = tab[i]/nbPixel;
	}
}

void calculateAllHistogram( Mat image , float tabR[ 256 ], float tabG[ 256 ], float tabB[ 256 ], int rows, int cols )
{
  int bplan = 0, gplan = 0, rplan = 0, cpt = 0;
	for ( int i = 0; i < rows; ++i )
	{
		for (int j = 0; j < cols; ++j)
		{
      bplan = image.at<Vec3b>(i,j)[0];
      gplan = image.at<Vec3b>(i,j)[1];
      rplan = image.at<Vec3b>(i,j)[2];
      //printf(" %d %d %d \n",bplan, rplan, gplan );
      // if( rplan != 234 && gplan != 18 && bplan != 178 )
      if ( rplan != 255 && gplan != 255 && bplan != 255 )
      {
        cpt++;
				tabR[ rplan ]++;
				tabG[ gplan ]++;
				tabB[ bplan ]++;
			}
		}
	}
  // normalizeValueTab( tabR, cpt );
  // normalizeValueTab( tabG, cpt );
  // normalizeValueTab( tabB, cpt );
}

void labellingEvo( Mat imageNB, Mat segemented, int rows, int cols ){

int attc=0,attb=0,atta=0;//initialisation des attributs
int cpt=0;
int **tab = ( int** )malloc( imageNB.rows * sizeof( int* ) );//tableau d'étiquette, segmentation
for (size_t i = 0; i < imageNB.rows; i++) {
	tab[i] = (int*) malloc(imageNB.cols * sizeof(int));
	for (size_t j = 0; j < imageNB.cols; j++) {
		tab[i][j] = 0;
	}
}
int p=0;
int tmpB,tmpA=0;//variable temporaire

for( int i=0+1; i<imageNB.rows;i++){
		for( int j=0+1;j<imageNB.cols;j++){
		//initialisation des valeurs des attibuts
			attc = imageNB.at<Vec3b>(i,j)[0];
			attb = imageNB.at<Vec3b>(i-1,j)[0];
			atta = imageNB.at<Vec3b>(i,j-1)[0];
		//Algorithme intuitif du cours
			if( attc == 255 )
			{
				if( attc == atta && attc != attb )
					tab[i][j] = tab[i][j-1];
				else if( attc == attb && attc != atta )
					tab[i][j] = tab[i-1][j];
				else if( attc != attb && attc != atta )
				{
						p++;
					//nouvelle étiquette
					tab[i][j] = p;
				}
				else if( attc == attb && attc == atta && tab[i][j-1]==tab[i-1][j] )
					tab[i][j] = tab[i-1][j];
				else if(attc == attb && attc == atta && tab[i][j-1]!=tab[i-1][j] )
				{
					tab[i][j] = tab[i-1][j];
					tmpB=tab[i-1][j];
					tmpA=tab[i][j-1];
					//retour en arriere et changement des etiquettes
					//jusqu'à i
					for( int k = 1; k < i; k++ )
					{
						for( int l = 1; l < j; l++ )
						{
             				if( tab[k][l] == tmpA )
								tab[k][l] = tmpB;
						}
					}
					//fint par les colones de la ligne i
					for( int l = 1; l < j; l++ )
					{
             			if( tab[i][l] == tmpA )
							tab[i][l] = tmpB;
					}
				}
			}
		}
  }
  float inter;
	for( int i = 1; i < imageNB.rows; i++ )
	{
			for( int j = 1; j < imageNB.cols; j++ ){
				inter = tab[i][j];
				segemented.at<Vec3b>(i,j)[0] = (unsigned char)((inter / p)*255);
				segemented.at<Vec3b>(i,j)[1] = (unsigned char)((inter / p)*255);
				segemented.at<Vec3b>(i,j)[2] = (unsigned char)((inter / p)*255);
			}
	}
	for ( int i = 0; i < imageNB.rows; i++) {
		free(tab[i]);
	}
	free(tab);
}

void informations( int numeroRegion, InfoRegion *infoRegion, Mat imageGris, Mat imageDefault ){
	int  nbx,nby =0;
	infoRegion->taille = 0;
	infoRegion->x=0;
	infoRegion->y=0;

  for( int i = 0 ; i < imageGris.rows; i++ ){
		for( int j = 0; j < imageGris.cols; j++ ){
			if ( imageGris.at<Vec3b>(i,j)[0] == numeroRegion ){
				infoRegion->taille++;
				nbx += i;
				nby += j;
			}
		}
	}
	infoRegion->x=nbx/(infoRegion->taille);
	infoRegion->y=nby/(infoRegion->taille);
}

void colorSegmentation( Mat originImage, Mat imNoirEtBlanc, float refHistR[ 256 ], float refHistG[ 256 ], float refHistB[ 256 ],
float refHistROmbre[ 256 ], float refHistGOmbre[ 256 ], float refHistBOmbre[ 256 ], int nbPixels )
{
  int bplan = 0, gplan = 0, rplan = 0;
  float probahistoLight = 0, probahistoDark = 0;
  for ( int i = 0; i < originImage.rows; ++i )
  {
    for ( int j = 0; j < originImage.cols; ++j )
    {
      bplan = originImage.at<Vec3b>(i,j)[0];
      gplan = originImage.at<Vec3b>(i,j)[1];
      rplan = originImage.at<Vec3b>(i,j)[2];
      if ( rplan < SEUIL_PIXEL_ACCEPTED && gplan < SEUIL_PIXEL_ACCEPTED && bplan < SEUIL_PIXEL_ACCEPTED )
      {
        probahistoLight = (float) ( refHistR[ rplan ] * refHistG[ gplan ] * refHistB[ bplan ] )* nbPixels*4;
        probahistoDark = (float) ( refHistROmbre[ rplan ] * refHistGOmbre[ gplan ] * refHistBOmbre[ bplan ] )* nbPixels * 6;

        if ( probahistoLight > SEUIL_PROBA_HISTO || probahistoDark > SEUIL_PROBA_HISTO )
        {
        //	imageFinale[i][j] = 255;
          imNoirEtBlanc.at<Vec3b>(i,j)[0] = 255;
          imNoirEtBlanc.at<Vec3b>(i,j)[1] = 255;
          imNoirEtBlanc.at<Vec3b>(i,j)[2] = 255;
        }
      }
    }
  }
}

int  getEttiquetteValue( Mat imageSemented, int region[ 100 ] )
{
  int tmp1 = 0, etiquette = 0, currentValue = 0;

  for( int i = 0; i < imageSemented.rows; i++)
  {
    for( int j = 0; j < imageSemented.cols; j++)
    {
      currentValue = imageSemented.at<Vec3b>(i,j)[0];
      if ( currentValue != 0  && currentValue != 255  )
      {
        for ( int l = 0; l < etiquette+1; ++l )
        {
          if ( currentValue == region[l] )
          {
            tmp1 = 1;
            break;
          }
          else
          {
            tmp1 = 0;
          }
        }

        if ( tmp1 == 0 ){
            region[etiquette] = currentValue;
            etiquette++;
        }
      }
    }
  }
return etiquette;
}

void resetAllPoint ( Point Point[ 10 ] )
{

  for ( int i = 0; i < 10; i++)
  {
    Point[ i ].x = 0;
    Point[ i ].y = 0;
  }
}
