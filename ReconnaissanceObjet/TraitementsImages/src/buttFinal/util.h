/**
 * \file        util.h
 * \brief       all the functiun used for the project
 * \author      Arif Abdek-Karim
 * \version     0.1
 * \date        Oct. 28th 2019
 *
 *
 *
 *
 */

 #ifndef UTIL_H
 #define UTIL_H


 #include <stdio.h>
 #include <math.h>
 #include <opencv2/opencv.hpp>
 #include <stdbool.h>

 #define SEUIL_PROBA_HISTO   						  	           0.25
 #define SEUIL_PIXEL_ACCEPTED				            		   245

 using namespace cv;
 using namespace std;

 typedef struct InfoRegion
 {
 		int x; 					        //centre de gravité en x
 		int y; 					        //centre de gravité en y
 		int R; 					        //plan r
 		int G; 					        //plan g
 		int B; 					        //plan b
 		float moyNiveauGris; 		//moyenne des niveaux de gris
 		int taille; 				    //nombre de pixels de la region
 		int minX; 		    		  // valeur min en x
 		int maxX; 		    	    // valeur max en x
 		int minY; 		    		  // valeur min en y
 		int maxY; 		    		  //valeur max en y
 };

 typedef struct Order
 {
   int motorL;
   int motorR;
 };

typedef struct LabellingInfo
{
  int region[ 100 ];
  int ettiquetteValue;
};

 void nErode ( int nbErode , Mat imEnter, Mat imExit );
 void nDilate( int nbDilate , Mat imEnter, Mat imExit );
 void normalizeValueTab( float tab[256], int nbPixel );
 void calculateAllHistogram( Mat image , float tabR[ 256 ], float tabG[ 256 ], float tabB[ 256 ], int rows, int cols );
 void labellingEvo( Mat imageNB, Mat segemented, int rows, int cols );
 void informations( int numeroRegion, InfoRegion *infoRegion, Mat imageGris, Mat imageDefault );

 void colorSegmentation( Mat originImage, Mat imNoirEtBlanc, float refHistR[ 256 ], float refHistG[ 256 ], float refHistB[ 256 ],
 float refHistROmbre[ 256 ], float refHistGOmbre[ 256 ], float refHistBOmbre[ 256 ], int nbPixels );

 int  getEttiquetteValue( Mat imageSemented, int region[ 100 ] );

 void resetAllPoint ( Point Point[ 10 ] );

#endif
