 #ifndef SERIAL_H
 #define SERIAL_H
#include "util.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>


typedef struct{
	char *port;
	int descripteur;
} serial_com;

char buffer[10];



//Les procedures nécessaires au projet
void sendOrder(serial_com *sp,Order ordre);
void serial_open(serial_com*, char*);
void serial_close(serial_com*);
void serial_write(serial_com*, char*);

void sendOrder(serial_com *sp,Order ordre){
	sprintf(buffer,"%d:%d;\n",ordre.motorL,ordre.motorR);
	serial_write(sp,buffer);
}
//Initialise la structure serial_com
void serial_open(serial_com *sp,char *name){
	//Ouvre le port name en lecture/ecriture
	(*sp).descripteur = open(name, O_RDWR);
	(*sp).port = name;
	printf("%s desc %d",name,(*sp).descripteur);
}

//Ferme de descripteur
void serial_close(serial_com *sp){
	close((*sp).descripteur);
}

//Ecrit la chaine buff vers le descripteur
void serial_write(serial_com *sp, char *buff){
    int a = write ((*sp).descripteur, buff, 7);
	if(a<1){
		printf("Erreur ecriture\n");
	}
	else{
		printf("\n%s a été écrit\n", buff);
	}
}

//Lire la chaine buff vers le descripteur
void serial_read(serial_com *sp, char *buff){
    int a = read ((*sp).descripteur, buff, 20);
	if(a<1){
		printf("Erreur\n");
	}
	else{
		printf("\n%s a été lue\n", buff);
	}
}

#endif
