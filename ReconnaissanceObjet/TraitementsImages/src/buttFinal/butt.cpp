#include <iostream>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "util.h"
#include "serial.h"
#include <math.h>
#include <opencv2/opencv.hpp>
#include <stdbool.h>
#include <chrono>
#include <ctime>

#define TAILLE_MIN_REGION                              200
#define TAILLE_MAX_REGION                              3500
#define WINDOW_WIDTH                                   320
#define WINDOW_HEIGHT                                  240


static bool bIsimageEmpty( Mat image )
{
  if( image.empty() )
  {
    cout << "Path incorrectly or filename wrong, image not read!" << std::endl;
    return true;
  }
  return false;
}

static bool bIsCameraOpen( VideoCapture cap )
{
  if( !cap.isOpened() )
  {
    printf("error");
    return false;
  }
}

static Mat initMat(int rows, int cols )
{
  Mat image( rows, cols, CV_8UC3, Scalar(0,0,0) );
  return image;
}

Order arduinoOrder( int x )
{
  int margeMidLeft  = ( WINDOW_WIDTH / 2 ) - 30  ;
  int margeMidRight = ( WINDOW_HEIGHT / 2 ) + 30  ;
  Order order;
    // if( x  >= margeMidLeft && x <= margeMidRight )
    // {
    //     order.motorL = 2;
    //     order.motorR = 2;
    //     // printf(" Megot au milieu \n" );
    // }

    if( x  > margeMidRight )
    {
        order.motorL = 4;
        order.motorR = -4;
        // printf(" Megot a droite \n" );
    }

    if( x  < margeMidLeft )
    {
        order.motorL = -4;
        order.motorR = 4;
        // printf(" Megot a gauche  \n" );
    }
    return order;
}
static int printImage( )
{
  int printImage = 0;
  printf( " Voulez-vous afficher les images ? ( 0 = oui/ 1 = non ) ");
  scanf("%d", &printImage );
  return printImage;
}

static int activateSerialPort( )
{
  int activateSerialPort = 0;
  printf( " Voulez-vous activer le port série ? ( 0 = oui/ 1 = non ) ");
  scanf("%d", &activateSerialPort );
  return activateSerialPort;
}

int main( int argc, char** argv )
{
  serial_com	port; // Open serial port
  char	*port_name = "/dev/ttyACM0";
  clock_t t1, t2;

  Mat ImageMegotrefLight = imread( "./images/imrefL.jpg", CV_LOAD_IMAGE_COLOR );
  Mat ImageMegotrefDark = imread( "./images/imrefO.jpg", CV_LOAD_IMAGE_COLOR );

  if( bIsimageEmpty( ImageMegotrefLight ) || bIsimageEmpty( ImageMegotrefDark ) )
    return -1;

  float refHistR[ 256 ] = { 0 }, refHistG[ 256 ] = { 0 }, refHistB[ 256 ] = { 0 };
  float refHistROmbre[ 256 ] = { 0 }, refHistGOmbre[ 256 ] = { 0 }, refHistBOmbre[ 256 ] = { 0 };

  calculateAllHistogram( ImageMegotrefLight , refHistR, refHistG, refHistB, ImageMegotrefLight.rows, ImageMegotrefLight.cols );
  calculateAllHistogram( ImageMegotrefDark , refHistROmbre, refHistGOmbre, refHistBOmbre, ImageMegotrefDark.rows, ImageMegotrefDark.cols );

  FILE *fichierR, *fichierG, *fichierB;

  fichierR = fopen ("R.txt", "w");
  fichierG = fopen ("G.txt", "w");
  fichierB = fopen ("B.txt", "w");

  if ( !fichierR || !fichierG || !fichierB ) {
          return EXIT_FAILURE;
  }
  for (int i = 0; i < 255; i++) {
    fprintf( fichierR, "%d\n", (int ) refHistR[ i ] );
    fprintf( fichierG, "%d\n", (int ) refHistG[ i ] );
    fprintf( fichierB, "%d\n", (int ) refHistB[ i ] );
  }
  // fwrite ( refHistR, sizeof refHistR, fichierR);
  // fwrite ( refHistG, sizeof refHistR, fichierG);
  // fwrite ( refHistB, sizeof refHistR, fichierB);

  fclose (fichierR);
  fclose (fichierG);
  fclose (fichierB);


  int zeros, un, deux, trois, pixel, cinq, six, sept, huit, ettiquette = 0;
  int barycentreX = 0, barycentreY = 0, cpt = 0, cptGravite = 0;
  int region[ 100 ] = { 0 };
  float fps, labellingSize = 0;

  Mat ImageMegot, imageFinale, imNoirEtBlanc, imBorder, segemented,concat, concat2,test;
  Point barycentre[ 10 ];
  InfoRegion infoRegion;
  Order order;
  LabellingInfo labellingInfo;
  VideoCapture cap( 0 );

  int activateSerial = 0;
  int printimage = printImage();

  if ( activateSerialPort() == 1 )
  {
    activateSerial = 1;
    serial_open( &port,	port_name);
  }

  if( bIsCameraOpen( cap ) )
    return -1;

  cap.set(CV_CAP_PROP_FRAME_WIDTH,WINDOW_WIDTH);
  cap.set(CV_CAP_PROP_FRAME_HEIGHT,WINDOW_HEIGHT);
  int nbPixels = WINDOW_WIDTH * WINDOW_HEIGHT;
  float minSizeInPercent   = ( float ) ( TAILLE_MIN_REGION * 100 ) / nbPixels * 1.0;
  float maxSizeInPercent   = ( float ) ( TAILLE_MAX_REGION * 100 ) / nbPixels * 1.0 ;

  for(;;)
  {
    cap >> ImageMegot;
    t1 = clock();

    imageFinale   = ImageMegot.clone();
    imBorder      = initMat( ImageMegot.rows, ImageMegot.cols );
    imNoirEtBlanc = initMat( ImageMegot.rows, ImageMegot.cols );
    segemented    = initMat( ImageMegot.rows, ImageMegot.cols );
    concat        = initMat( ImageMegot.rows, ImageMegot.cols );
    concat2       = initMat( ImageMegot.rows, ImageMegot.cols );
    test          = initMat( ImageMegot.rows, ImageMegot.cols );

    colorSegmentation( ImageMegot, imNoirEtBlanc, refHistR, refHistG, refHistB, refHistROmbre, refHistGOmbre, refHistBOmbre, nbPixels );
    nDilate( 8 , imNoirEtBlanc, imBorder );
    nErode ( 4 , imBorder, imBorder );
    labellingEvo( imBorder, segemented, ImageMegot.rows, ImageMegot.cols );
    ettiquette = getEttiquetteValue( segemented, region );

    for ( int m = 0; m < ettiquette; ++m )
    {
		  informations( region[m] , &infoRegion, segemented, ImageMegot );
      labellingSize = ( float ) ( infoRegion.taille * 100 ) / nbPixels * 1.0;
      // printf("test taille = %f \n",labellingSize  );
      if ( labellingSize > minSizeInPercent  && labellingSize < maxSizeInPercent   )
      {
        barycentre[m].x = infoRegion.y;
        barycentre[m].y =infoRegion.x;
        cptGravite++;
      }
    }

    /** --------------------------------------- JUST FOR TEST ----------------------------------- **/

    if ( barycentre[0].x != 0 && barycentre[0].y != 0 )
    {
      circle( imageFinale, ( barycentre[0] ), 30, (255, 255, 0), 1);
      order = arduinoOrder(  barycentre[0].x );
      if( activateSerial == 1 )
        sendOrder( &port, order );
    }
    else
    {
	     order.motorL = 0;
	     order.motorR = 0;
       if( activateSerial == 1 )
        sendOrder( &port, order );
	  }

    /** --------------------------------------- JUST FOR TEST ----------------------------------- **/
    resetAllPoint ( barycentre );

		t2 = clock();
		float temps = (float)(t2-t1)/CLOCKS_PER_SEC;
    temps = temps * 1000;

    if ( printimage == 1)
    {
      vector<cv::Mat> matrices2 = {
              segemented, imBorder
      };

      vector<cv::Mat> matrices = {
              imageFinale, imNoirEtBlanc
      };

      hconcat( matrices, concat); // horizontal
      hconcat( matrices2, concat2); // horizontal
      vconcat( concat, concat2, test );
      imshow("imNoirEtBlanc", test);
    }

    if( waitKey(33) == 27 )
      break;
  }
  if( activateSerial == 1 )
    serial_close( &port );
  return 0;
}
