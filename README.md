### CY Cergy Paris Université

Master 2 Systèmes Intelligents et Communicants PARCOURS Intelligence Embarquée

### Rapport pour le projet de synthèse

# Responsibility

### Robot autonome en milieu urbain

```
Rapporteur :Sofiane BOUCENNA
```
```
Tuteur technique :Jordane LORANDEL
Encadrant de gestion de projet :Tianxiao LIU
```
```
Auteurs :
Abdel-Karim ARIF
Nicolas DALLERAC
Alaeddine HEDHLY
Chakib YOUSFI
```
```
11 juin 2020
```

# Remerciements

Nous tenons à remercier tout d’abord notre tuteur technique monsieur Jordane Lorandel, pour son
aide et ses conseils aussi bien sur le plan technique qu’au niveau des idées et directions à prendre tout
le long du projet. Nous tenons aussi à remercier monsieur Tianxiao Liu pour son aide dans le cadre de
la gestion de projet, qui nous a permis d’éviter certains pièges. Nous remercions également Zinedine
Galouz pour sa disponibilité et Ghiles Mostafoui pour les précieux conseils sur la direction à prendre
dans notre projet. Enfin nous souhaitons remercier monsieur Sofiane Boucenna de son intérêt vis-à-vis
de notre projet en ayant choisi de le rapporter.


## Table des matières





- 1 Introduction
   - 1.1 Contexte du projet
   - 1.2 Mise en scénario
   - 1.3 Objectifs du projet
   - 1.4 Environnement de travail
   - 1.5 Organisation du rapport
- 2 Présentation et spécification du projet
   - 2.1 Étude du marché
   - 2.2 Fonctionnalités attendues
   - 2.3 Conception globale du projet
   - 2.4 Architecture technique
   - 2.5 Problématiques identifiées et solutions envisagées
- 3 Déplacement du robot
   - 3.1 Analyse de la problématique
   - 3.2 État de l’art : études des solutions existantes
      - 3.2.1 Les moyens de déplacement
      - 3.2.2 Stratégies de navigation
      - 3.2.3 Les architectures de contrôle
   - 3.3 Solution proposée et mise en œuvre
      - 3.3.1 Moyen de déplacement choisit
      - 3.3.2 Moyen de validation choisit
      - 3.3.3 L’évitement d’obstacle avec une architecture réactive
      - 3.3.4 Performance des différentes solutions
- 4 Reconnaissance d’objet
   - 4.1 Analyse de la problématique
      - 4.1.1 La vision
      - 4.1.2 Du monde réel au numérique
      - 4.1.3 Les images numériques
   - 4.2 État de l’art : études des solutions existantes
      - 4.2.1 Les méthodes de traitements d’images
      - 4.2.2 Utilisation de l’intelligence artificielle
   - 4.3 Solution proposée et mise en œuvre
      - 4.3.1 YOLO
      - 4.3.2 Traitement d’images
   - 4.4 Tests et certifications de la solution
      - 4.4.1 Tests sur YOLO
      - 4.4.2 Tests sur Les méthodes de traitement d’images
- 5 Asservissement
   - 5.1 Analyse de la problématique
      - 5.1.1 La communication
      - 5.1.2 L’exécution
   - 5.2 État de l’art : études des solutions existantes
      - 5.2.1 Les transmissions de données
      - 5.2.2 Le temps réel
   - 5.3 Solution proposée et mise en œuvre
      - 5.3.1 La chaîne de communication
      - 5.3.2 La liaison série
      - 5.3.3 Le temps réel
      - 5.3.4 L’asservissement
   - 5.4 Tests et certifications de la solution
      - 5.4.1 Liaison série UART
      - 5.4.2 L’asservissement
- 6 Rendu final
   - 6.1 Application mobile
      - 6.1.1 Avant-propos
   - 6.2 Fonctionnalités de l’application
   - 6.3 Le robot
      - 6.3.1 Assemblage du robot
      - 6.3.2 Conception et fabrication des différentes pièces
      - 6.3.3 Réglage et Ajustement
      - 6.3.4 Schéma électrique
      - 6.3.5 Consommation théorique
      - 6.3.6 Consommation pratique
- 7 Gestion de projet
   - 7.1 Méthode de gestion
      - 7.1.1 Cycle de vie agile
      - 7.1.2 Planification
   - 7.2 Répartition des tâches
   - 7.3 Recueil et hiérarchisation des besoins du client
   - 7.4 Parallélisme du travail de développement assurée par une architecture avec modularisation
   - 7.5 Utilisation des outils de gestion de projet au sein de l’équipe
- 8 Conclusion et perspectives
   - 8.1 Conclusion
   - 8.2 Perspectives
- Bibliographie
- 1.1 Mégots jetés à même le sol en plein Paris Table des figures
- 1.2 Exemple de mise en scénario
- 2.1 Dustclean
- 2.2 Dustcart
- 2.3 Jellyfishbot
- 2.4 Diagramme de cas d’utilisation UML de notre système
- 2.5 Fonctionnement du système
- 2.6 Schéma du setup final
- 2.7 Schéma de l’architecture de notre système
- 3.1 Schéma des interactions d’un robot avec son environnement
- 3.2 Hiérarchique (A), Réactive (B) et Hybride (C)
- 3.3 Motoréducteur
- 3.4 Simulateur Webots
- 3.5 Exemple de robots dans Webots
- 3.6 Le robot Responsibility dans le simulateur Webots
- 3.7 Fenêtre de simulation Webots
- 3.8 Véhicule de braitenberg
- 3.9 360 mesures du lidar
- 3.10 Discrétisation en zone
- 3.11 Histogramme des zones
- 3.12 Principe de l’apprentissage par renforcement
- 3.13 Discrétisation en zone
- 3.14 Récompense durant 300 itérations
- 3.15 Historiques de positions avant et après apprentissage
- 3.16 Comparaison des performances temporelles en fonction de la méthode utilisée
- 4.1 Schéma de l’œil
- 4.2 Schéma de l’appareil photo
- 4.3 Image d’entrée divisée en imagettes
- 4.4 Réseau neuronal convolutif de YOLO [17]
- 4.5 Fonction de perte de YOLO
- 4.6 Exemple d’image utilisée pour l’apprentissage
- 4.7 Quelques types de mégots possibles
- 4.8 Algorithme de notre seconde solution
- 4.9 Image après segmentation
- 4.10 Images de référence
- 4.11 Histogramme du plan rouge
- 4.12 Histogramme du plan vert
- 4.13 Histogramme du plan bleue
- 4.14 Résultats tests statiques
- 4.15 Résultats temps réel intérieurs
- 4.16 Résultats temps réel extérieurs
- 4.17 Résultat morphologique
- 4.18 Résultats Étiquetage
- 4.19 Exemple de résultat sur une image contenant un mégot
- 4.20 Détection du mégot sur le flux de la webcam
- 4.21 Résultats des tests
- 4.22 Nouvelles images de référence ( Lumière/Ombre )
- 4.23 Tableau comparatifs de nos méthodes )
- 5.1 Schéma de la chaîne de communication
- 5.2 Schéma de la liaison UART
- 5.3 Capture d’écran du logiciel STM32 Cube MX
- 5.4 Schéma d’un signal PWM
- 5.5 Photo du dispositif du test de la liaison UART temps réel
- 5.6 Photo du dispositif du test d’aspiration
- 6.1 Interaction Application Robot
- 6.2 Écran d’accueil de l’application
- 6.3 Responsibility - Photo
- 6.4 Responsibility - Photo
- 6.5 Conception du support pour le servomoteur
- 6.6 Avant lissage
- 6.7 Après lissage
- 6.8 Schéma global
- 6.9 Raspberry pi idle
- 7.1 Cycle de vie agile ASD
- 7.2 Sommaire de notre compte-rendu numéro
- 7.3 Schéma de l’architecture avec modularisation du système
- 7.4 Contribution de l’équipe du 28 octobre 2019 au 02 juin
- 7.5 Résumé des outils de gestion utilisés
- 6.1 Protocole de communication Liste des tableaux
- 6.2 Consommation des différents modules
- 7.1 Calendrier de planification
- 7.2 Répartition des tâches entre les membres de l’équipe


## Chapitre 1

# Introduction

Dans cette section, nous allons établir le contexte du projet, puis nous présenterons un exemple de
mise en scénario de notre projet. Par la suite nous parlerons de notre environnement de travail et nous
finirons par présenter l’organisation du rapport.

### 1.1 Contexte du projet

Aujourd’hui, en France, les questions sur l’environnement et l’écologie font parties intégrantes de
notre vie. Comme la dit le président Emmanuel Macron en 2020, "La transition écologique n’est plus
un enjeu pour demain mais une priorité pour aujourd’hui", nous ne pouvons plus continuer à vivre
aujourd’hui comme nous vivions il y a de cela 40 ans.
Selon un rapport de l’ONU paru en 2015, les conditions environnementales sont responsables "d’environ
25% des morts et maladies mondiales", ce qui représente, pour l’année 2015, environ neuf millions de
morts liées aux pollutions environnementales.

L’une des principales sources de pollution dans le monde est la consommation de tabac, sur plus de
7 milliards d’humains sur Terre, 1.3 milliard d’individus fument ( 16 millions juste en France ) environ
6 000 milliards de cigarettes, 6 millions de personnes meurent chaque année parmi lesquelles plus de
5 millions sont des fumeurs et plus de 600 000 des non-fumeurs exposés au tabagisme passif (données
OMS). Cette consommation a un impact direct sur la pollution de l’air extérieur et domestique, mais
aussi et surtout celle de l’eau. Il n’y a qu’à regarder nos rues pour en voir le résultat, des dizaines, voir
des centaines de mégots à chaque coin de rue comme on peut le voir sur l’image suivante.

```
Figure1.1 – Mégots jetés à même le sol en plein Paris
```
On les regardes, on les côtoie, on ne pense pas aux nombreux effets négatifs amenés par de simples
mégots. Ce qu’il faut savoir, c’est que la pollution causée par les mégots de cigarette est extrêmement


présente dans notre vie. Chaque année, 25 000 tonnes de mégots sont jetés à même le sol, et chacun
de ces mégots peuvent polluer jusqu’à 500 litres d’eau.

Suite à la concertation des membres de notre équipe, l’ensemble de ces éléments nous ont donné
l’idée de proposer ce projet. Le projet a ainsi comme titre principal "Responsibility", car il est de notre
responsabilité d’agir contre ce phénomène. Notre but est donc de proposer une solution à ce fléau afin
d’améliorer au maximum notre quotidien et de celui des gens qui nous entourent.

Un certain nombre de projets visant à limiter la pollution de quelque manière que ce soit ont déjà
vu le jour. Notre projet se veut différent de ce qui a été proposé jusqu’ici, il se veut novateur de part
les méthodes utilisées et de part l’application souhaitée.

### 1.2 Mise en scénario

A présent, afin de faciliter la compréhension du projet, nous allons présenter un exemple concret typique
de l’utilisation de notre projet.

L’un des cas concrets d’utilisation possible de notre robot sera par exemple de pouvoir nettoyer un
lieu généralement fréquenté par des fumeurs. Le plus souvent, les lieux à forte concentration de fumeurs
se trouvent être pollués par les mégots de cigarettes. Le but sera donc de déposer notre robot dans cet
environnement, il devra donc dans un premier temps effectuer un "scan" de la zone. Par la suite, il se
déplacera alors de manière totalement autonome en évitant les obstacles. Enfin lorsqu’il détectera la
présence d’un mégot, il se déplacera pour le ramasser. Cette action sera répétée jusqu’à à un certain
niveau de batterie du robot. En effet, une fois que la batterie sera faible le robot retournera à sa
position d’origine. Le robot va donc " nettoyer" la zone afin de remédier à la négligence des fumeurs.

```
Afin de mieux comprendre, on peut regarder l’illustration suivante de notre scénario :
```
```
Figure1.2 – Exemple de mise en scénario
```
### 1.3 Objectifs du projet

Une fois le contexte posé et le sujet choisi que nous souhaitons développer, un certain nombre d’objectifs
se sont dégagés.


```
Le projet peut être divisé en trois parties indépendantes :
— Déplacement autonome : le but de cette partie est de trouver un moyen de faire avancer notre
robot en environnement urbain de manière totalement autonome. Afin d’être encore plus précis,
comme vue dans la section précédente, nous souhaitons pouvoir déposer notre robot dans la rue
et que celui-ci soit capable de se déplacer en évitant les obstacles qui se présenteront à lui. Mais
aussi, il doit être capable de revenir à sa position d’origine.
— Reconnaissance de mégots : ici le but sera de faire en sorte de reconnaître tous types de mégots
de cigarettes ( et seulement les mégots ), dans différents type d’environnement. En effet, le robot
va se déplacer, puis au fur et à mesure de sa course, il devra localiser les mégots de cigarettes,
et donc connaître leurs positions précise afin de se diriger vers eux.
— Ramassage des mégots : cette partie aura pour but d’expliquer la façon dont nous allons ramasser
les mégots qui ont été détectés. Une fois que le mégot sera localisé, que le robot sera assez proche,
ici le but sera donc de récupérer ce mégot et de le stocker.
```
L’objectif premier de ce projet est donc de faire en sorte que chacune de ces parties fonctionne
indépendamment les unes des autres. Puis par la suite, le but du projet va donc être de pouvoir
ramasser des mégots de cigarettes en reliant les trois parties entre elles.

Nous avons décidé de choisir un milieu urbain. En effet, il est beaucoup plus logique de ramasser des
mégots de cigarettes en extérieur (leurs présences étant quasiment assurées), d’autant plus que notre
projet se veut bénéfique pour l’environnement.

L’équipe est composée d’Abdel-Karim ARIF, Nicolas DALLERAC, Alaeddine HEDHLY, Chakib
YOUSFI. Pour rappel, nous avons soumis le projet le 21 septembre 2019, nous avons effectué la pré-
soutenance technique le 28/04/2020, nous avons remis le rapport le 12/06/2020 et enfin les dernières
dates importantes se trouvent être la soutenance de projet et la démonstration respectivement le
18/06/2020 et le 19/06/2020.

### 1.4 Environnement de travail

Pour ce qui est de notre environnement de travail, il est divisé en deux parties, tout d’abord le travail
que l’on effectue chacun de son coté et de l’autre le travail de groupe que nous avions pour habitude
d’effectuer dans la "salle orange" du département informatique. Suite aux évènements sanitaires, notre
routine à été quelque peu modifiée, en effet pour le travail en groupe nous avons eu recours à l’utilisation
du logiciel skype.

Pour tous les codes de tests, nous utilisons la suite Atom, connectée à Gitlab pour nous permettre
de partager les codes. Pour le développement de la simulation de nos algorithmes, nous utilisons le
logiciel Webots en développant sous Windows mais en générant des exécutables adaptés à Linux car
l’utilisation finale du projet se fera sous Linux.

```
De plus nous disposons de matériels nous permettant de développer notre robot :
— Plusieurs ordinateurs portable sous Linux et Windows
— Plusieurs Arduino
— Un Raspberry 4
— Plusieurs webcams
— Une batterie, des capteurs ultrasons, des leds et une stm
```
Enfin pour finir, pour tout ce qui est documentation, nous avons opté pour la solution utilisant le
langage Latex.


### 1.5 Organisation du rapport

En comptant ce chapitre, ce rapport est organisé en 8 chapitres avec dans un premier temps les
spécifications suivies de trois chapitres techniques, du rendu final ainsi que d’un chapitre gestion de
projet puis d’une conclusion.

Dans le chapitre 2, nous allons passer en revue les produits du marché similaires au nôtre, puis voir
en quoi notre projet diffère des solutions existantes, nous présenterons les spécifications du projet,
les fonctionnalités qui sont attendues, la manière dont nous avons conçu le projet, les différentes
problématiques ainsi que la manière dont nous allons y répondre.

Dans le chapitre 3, nous allons dans un premier temps analyser la problématique de déplacement.
Pour cela, on va se demander ce qu’est un "déplacement" et nous allons étudier les différentes solutions
existantes permettant de se déplacer. Puis, nous parlerons de la solutions qui a été mise en œuvre et
enfin nous parlerons des tests et des certifications mises en place.

Dans le chapitre 4, nous allons voir comment on peut reconnaître un objet particulier. Pour cela , on
va se poser des questions sur ce que l’on appel la "perception". Puis, nous allons étudier les différentes
solutions existantes permettant de reconnaître cet objet spécifique, on parlera du traitement d’images
ou encore de l’utilisation de l’intelligence artificielle. Par la suite, nous parlerons de la solution qui a
été mise en œuvre et enfin nous présenterons la méthodologie de validation de notre solution.

Dans le chapitre 5, nous allons encore une fois analyser la problématique, pour cela, nous nous
demanderons ce que signifie ramasser un objet. Ensuite, nous allons étudier les différentes solutions
existantes permettant de ramasser un objet, de l’utilisation d’un bras robotique à l’utilisation d’un
aspirateur. Enfin, nous parlerons de la solutions qui a été choisit pour ramasser notre objet et la
manière dont nous avons fonctionné pour valider notre solution.

Dans le chapitre 6, nous allons développer ce que l’on appelle le rendu final, c’est-à-dire les résultats
intégrants du projet, ce qui sera présenté lors de la démonstration finale. Nous allons donc parler de
comment les différents modules seront interconnectés tous les modules pour enfin tester et certifier le
tout.

Le chapitre 7 sera consacré à la gestion de projet, nous parlerons de notre méthode de gestion, c’est-
à-dire de son cycle de vie ou encore des planifications prévues. On parlera aussi de la répartition des
tâches, de la manière dont nous avons recueilli les besoins. Pour finir, on parlera de la manière dont
on a développé le projet et des différents outils qui ont été utilisés tout au long de ce projet.

Pour finir dans le chapitre 8, nous ferons le bilan de ce que le projet nous a apporté et de la manière
dont nous avons abordé le sujet. Nous ferons un comparatif entre nos attentes et les fonctionnalités qui
étaient attendues afin de savoir si ce projet a été mené à bien. Enfin, nous évoquerons les perspectives
d’amélioration en fonction de l’état final du projet.


## Chapitre 2

# Présentation et spécification du projet

Durant ce chapitre, nous allons décrire de manière précise les attentes de notre projet en d’autres
termes, le cahier des charges. A cela on ajoutera les spécifications techniques du projet.

### 2.1 Étude du marché

Le thème que nous allons développer durant notre projet fait l’objet de plusieurs réalisations dispo-
nibles ou non sur le marché. En effet, plusieurs projets ayant un but similaire ont été développés. Nous
pouvons par exemple parler des robots suivants :
— "DustClean", un robot de nettoyage équipé de brosses et de jets d’eau, capable de nettoyer la
rue en y circulant.

```
Figure2.1 – Dustclean
```
```
— "Dustcart" est un robot poubelle capable de collecter et transporter les ordures à la demande.
Il est capable de transporter 80 litres de déchets et possède différents conteneurs pour le tri
sélectif.
```

```
Figure2.2 – Dustcart
```
```
— Jellyfishbot un robot, qui collecte les déchets à la surface de l’eau.
```
```
Figure2.3 – Jellyfishbot
```
En effet bien que les produits finaux réalisés s’éloignent de notre objectif, il n’en reste pas moins
semblable sûr de nombreux aspects et c’est pourquoi nous avons décidé de réaliser cette brève étude.

Tout d’abord, concernant les projets "DustBot" (DustClean, Dustcart), la phase de développement
était du 1er décembre 2006 au 30 novembre 2009 et visait à créer plusieurs robots capables de collecter
les ordures provenants des habitations. Ce projet a été financé par la commission européenne à hauteur
de 1 898 000 euros.

Maintenant le "Jellyfishbot" a été conçu par la société "IADYS" mais nous n’avons pas plus d’in-
formations.

La particularité des trois robots précédents est qu’ils ne sont pas totalement autonomes, Dustbot
doit être guidé et ne peut pas se déplacer seule. De même pour Jellyfishbot qui doit être télécommandé.
De plus, il n’y a pas de différenciation entre les déchets.

Cette étude permet de mieux nous rendre compte vers quels aspects nous pouvons innover ou ap-
porter des améliorations. Ainsi nous nous dirigerons vers de toutes autres approches pour ce qui est
du déplacement de manière à le rendre totalement autonome. Quant aux déchets à récolter nous nous
concentrerons sur un type bien précis d’objet. Nous utiliserons un certain nombre de techniques simi-
laires que nous nous efforcerons de perfectionner.

### 2.2 Fonctionnalités attendues

Afin de satisfaire les besoins de l’utilisateur, c’est à dire d’avoir un dispositif capable de se déplacer
de manière autonome, être capable de se repérer dans un espace, de détecter un type d’objet particulier


et enfin notre système devra être capable de ramasser cet objet. Ces fonctionnalités attendues peuvent
tout d’abord être représentées dans une vue globale à travers le diagramme 2.4 suivant, qui répertorie
les différents blocs d’actions liés entre eux ainsi que les éléments externes ayant un lien dessus.

```
Figure2.4 – Diagramme de cas d’utilisation UML de notre système
```
Comme on peut le voir sur notre diagramme d’utilisation, on peut séparer les fonctionnalités de
notre système en deux parties importantes :
— Les fonctionnalités "utilisateur" liées au pilotage à distance de notre robot
— La connexion à distance au système
— Lancer/arrêter le système
— Gestion des logs
— Les fonctionnalités liées au robot
— Pilotage manuel ou autonome du robot
— Scanner l’environnement
— Analyse continue de l’environnement
— Reconnaissance, localisation et ramassage de mégots
Comme on peut le voir, on peut réunir ces fonctionnalités sous trois grandes parties. La première
consistera en le déplacement autonome du robot dans une zone(l’aide du LIDAR et des capteurs),
la seconde sera donc la reconnaissance et la géolocalisation du mégot(à l’aide de la caméra) dans
l’environnement et enfin le ramassage du mégot(à l’aide d’un aspirateur). Ensuite on peut y ajouter
une partie qui prend en compte le pilotage à distance de notre robot.


```
Figure2.5 – Fonctionnement du système
```
### 2.3 Conception globale du projet

Pour commencer nous allons voir le système d’un point de vue utilisateur c’est à dire sans trop de
détails techniques. Pour ce faire nous allons présenter l’idée de setup final avec les différents modules
fonctionnels qui le composent.

Le but du setup final est non seulement de pouvoir faire la démonstration de l’ensemble du travail
réalisé mais aussi de mettre en place un protocole expérimental nous permettant de faire nos essaies
et nos mesures dans l’environnement associé en temps réel.

Modules Notre dispositif est composé des modules suivants :
— Un châssis composé de quatre roues et quatre moteurs
— Un ensemble de capteurs ( LIDAR + ultrason )
— Deux microcontrôleurs afin de gérer les déplacements
— Une machine centrale afin de gérer les différents traitements
— Une caméra pour la partie vision
— Un dispositif pour ramasser les objets
— Un container pour conserver les objets ramassés

Schéma Le schéma 2.3 suivant permet de mieux rendre compte de l’allure de notre setup final :


```
Figure2.6 – Schéma du setup final
```
### 2.4 Architecture technique

Ici nous allons entrer dans une vue plus technique où nous allons expliquer comment sont réalisées les
fonctionnalités et comment elles se raccordent entre elles. On retrouve ainsi comme modules :
— Le système de déplacement
— Le système de vision
— Le système de ramassage
— Le système de détection d’obstacle
— Le système de décision

Ainsi, on retrouve tout d’abord un module de prise de décision, qui à l’aide d’un algorithme d’in-
telligence artificielle et des données issues des différents capteurs (système de vision et d’évitement
d’obstacle) sera capable de définir les actions à exécuter. Ainsi, le système de vision sera capable d’in-
diquer une direction à prendre en cas de détection d’objet cible, qui à l’aide du module de détection
d’obstacle sera capable de naviguer en évitant toute collision. Une fois la décision prise, elle sera trans-
mise au système de déplacement ou au dispositif de ramassage si le robot se trouve devant l’objet
cherché.


```
Figure2.7 – Schéma de l’architecture de notre système
```
### 2.5 Problématiques identifiées et solutions envisagées

Maintenant que nous avons bien identifié et conceptualisé les différentes fonctionnalités du système, on
peut ainsi ressortir trois problématiques associées à chacune des capacités du système, accompagnées
de premiers éléments de solutions envisagées, de contraintes ainsi que de besoins spécifiques.

```
Comment se déplacer de manière autonome? Pour répondre à cette question, on pourra :
— Dans un premier temps, étudier le déplacement, comment il se caractérise ou encore comment
il s’applique. Ensuite, afin de répondre au mieux à cette question, il faudra par exemple voir
quels sera le matériel à utiliser ( roues, chenilles etc...).
— Dans un second, rendre se déplacement autonome, cette fois pour répondre il faut une nouvelle
fois étudier le matériel à utiliser ( LIDAR, capteurs etc ...), mais surtout il faudra voir les
différents algorithmes à mettre en place en prenant en compte le matériel utilisé ( logique
floues, A*, apprentissage par renforcement etc...).
— Enfin, il faudra prendre en considération le fait que le robot doit pouvoir se déplacer pendant
un certain temps.
```
Comment reconnaître un objet spécifique dans un environnement? Ici, pour répondre,
nous devrons étudier la notion de perception afin de comprendre ce que l’on cherche réellement à
faire. Nous devrons déjà dans un premier temps étudier le matériel que l’on souhaite utiliser pour
notre système ( raspberry pi, raspicam etc ...). On devra alors étudier les techniques à mettre en
œuvre, que ce soit le traitement d’images pur ou encore l’apprentissage, on devra se renseigner sur
les différents algorithmes à mettre en place. On devra encore une fois prendre en considération les
différentes contraintes ( majoritairement matériel dans notre cas ) afin de pouvoir faire tourner nos
différents algorithmes sur le matériel associé tout en respectant les différentes contraintes de temps
réel.

Comment ramasser un objet? Une fois que notre objet est détecté, il faut alors le ramasser,
pour cela on va de la même manière chercher les différentes méthodes afin de ramasser cet objet (
aspiration, pince, bras robotisé etc ...). En fonction de la méthode choisit, la difficulté et l’impact sur
le projet n’est pas le même, il faudra donc bien prendre le temps de réfléchir, en effet la complexité
des algorithmes varie grandement en fonction de la méthode.


## Chapitre 3

# Déplacement du robot

Dans cette partie, nous présenterons les différentes méthode et stratégies de navigation utilisable par
notre robot. Nous présenterons ensuite un comparatif de ces méthodes.

### 3.1 Analyse de la problématique

Le but du robot est qu’il puisse se déplacer dans un environnement urbain en toute autonomie. Ces
lieux d’action privilégiés sont les zones extérieures piétonnes. Le robot sera capable de se déplacer en
toute autonomie et de se diriger vers les mégots. Il pourra franchir des obstacles, monter des pentes et
les descendre.

```
Tout d’abord,le déplacement peut être caractériser par trois composantes :
— La vitesse, la vitesse de déplacement du robot.
— Un couple, en effet la puissance d’un moteur procure un couple qui peut être visualisé comme
une force appliquée à un bras de levier, l’ensemble tournant autour de son axe. Le couple est en
quelque sorte un moment de force dynamique qui résulte de la puissance divisée par la vitesse
de rotation.
— Une accélération, se caractérise par l’augmentation de la vitesse dans le temps.
```
### 3.2 État de l’art : études des solutions existantes

Un robot mobile, est un système mécanique, électronique et informatique se déplaçant vers un but qui
lui a été attribué. Il peut ou doit être capable de l’atteindre même si les conditions de son environnement
change.

```
Un robot est capable d’agir sur son environnement en fonction de la perception qu’il en a.
```
```
Figure3.1 – Schéma des interactions d’un robot avec son environnement
```
#### 3.2.1 Les moyens de déplacement

```
Il existe plusieurs manière de se déplacer :
```

```
— Roues : Méthode de déplacement le plus commun. Le robot est équipé d’au moins deux roues
et d’un patin pour l’équilibre. Les roues sont régulièrement utilisées pour leur simplicité d’utili-
sation et le grand de choix possible de diamètre de roue permet de facilement trouver des roues
adaptées à un projet. Pour tourner il suffit de créer une différence de vitesse entre la ou les
roues motrices d’un côté. Le robot tournera dans le sens où les roues tourneront le moins vite.
— Pattes : Ce système de déplacement est le plus compliqué et donc le moins répandu. La difficulté
réside à prendre en compte la stabilité du robot, surtout pour un robot bipède. En plus de cela,
la conception du robot doit prendre en compte si les pattes propulsent le robot par l’avant
ou l’arrière. L’avantage est qu’un robot à pattes de complexité élevé permet de franchir des
obstacles plus facilement.
— Chenilles : les chenilles agissent comme de grandes roues de chaque côté du robot augmentant
la surface de contact au sol augmentant la force de frottement et donc on obtient un meilleur
couple. Pour l’orientation le principe est le même que pour les roues, il suffit de créer une
différence de vitesse entre les deux côtés.
Ces 3 moyens de déplacement sont les plus utilisés en robotique, ils ont chacun leurs avantages
et leurs défauts.
```
#### 3.2.2 Stratégies de navigation

Dans ce chapitre, nous allons évoquer le déplacement du robot et voir comment faire en sorte que
notre robot se déplace de manière autonome dans un environnement.

Il existe énormément de stratégies de navigation, et de classification qui peut en être fait. Nous allons
reprendre une classification établie par Trullier et Meyer.

Approche d’un objet

Cette capacité de base permet de se diriger vers un objet visible depuis la position courante du robot.
Elle est en général réalisée par une remontée de gradient basée sur la perception de l’objet, comme dans
l’exemple célèbre des véhicules de Valentino Braitenberg^1 , stratégie qui utilise des actions réflexes.

Guidage

Cette capacité permet d’atteindre un but qui n’est pas un objet visible, mais un point caractérisé
par un ensemble d’amers^2. Cette capacité consiste alors à se diriger dans la direction qui permet de
reproduire la configuration d’amers (but).

Action associée à un lieu

Cette capacité est la première capacité réalisant une navigation globale, c’est-à-dire qui permet
de rejoindre un but depuis des positions pour lesquelles ce but ou les amers qui caractérisent son
emplacement sont invisibles. Elle requiert une représentation interne de l’environnement qui consiste
à définir des lieux comme des zones de l’espace dans lesquelles les perceptions restent similaires, et à
associer une action à effectuer à chacun de ces lieux

Navigation topologique

Cette capacité est une extension de la précédente qui mémorise dans le modèle interne les relations
spatiales entre les différents lieux. Ces relations indiquent la possibilité de se déplacer d’un lieu à un
autre, mais ne sont plus associées à un but particulier. Ainsi le modèle interne est un graphe qui
permet de calculer différents chemins entre deux lieux arbitraires. Ce modèle ne permet toutefois que
la planification de déplacements parmi les lieux connus et suivant les chemins connus

1. expert en cybernétique allemand
2. Points remarquables


Navigation métrique

Cette capacité est une extension de la précédente car elle permet au robot de planifier des chemins
au sein de zones inexplorées de son environnement. Elle mémorise pour cela les positions métriques
relatives des différents lieux, en plus de la possibilité de passer de l’un à l’autre. Ces positions relatives
permettent, par simple composition de vecteurs, de calculer une trajectoire allant d’un lieu à un autre,
même si la possibilité de ce déplacement n’a pas été mémorisée sous forme d’un lien

Les 3 premières stratégies utilisent des mécanismes réflexes proche de ce qu’utilise les insectes, mais
ces stratégies sont vite limitées. Nous pouvons les classer sous le terme générique de stratégies réactives.

Les 2 derniers modèles sont des modèles qui ont besoins de connaître l’environnement. Il a besoin
d’un certain temps pour cartographier l’environnement mais ils seront beaucoup plus efficace pour
planifier une trajectoire vers un but visible ou non visible.

#### 3.2.3 Les architectures de contrôle

Un robot nécessite un arbitrage par exemple pour définir comment atteindre son but et faire la part
des choses entre le plan préétabli et un obstacle imprévu.

Cette arbitrage est appelé architecture de contrôle. En fonction de la manière d’implémenter les
différentes règles de contrôle on peut classifier les architectures en 3 catégories.

```
Figure3.2 – Hiérarchique (A), Réactive (B) et Hybride (C)
```
Architecture hiérarchique (A) : Le robot se représente son environnement grâce à ses capteurs, et
ensuite le robot planifie ses actions. ( Introduit dans Shakey, The first AI robot, 1967-70).

Architecture réactive (B) : La représentation de l’environnement perçu par le robot est directement
connecté à ses actions.

Architecture hybride (C) : L’architecture hybride combine architecture hiérarchique et architecture
réactive.

### 3.3 Solution proposée et mise en œuvre

#### 3.3.1 Moyen de déplacement choisit

Motoréducteur : Un motoréducteur est une unité composée d’un moteur et d’un réducteur. Un
moteur est une machine électromécanique capable de transformer un courant électrique en énergie
mécanique. Un réducteur réduit la vitesse en sortie via un système d’engrenage et augmente le couple.


```
Figure3.3 – Motoréducteur
```
Pour effectuer des expérimentations dans un environnement virtuel pour tester, et affiner des algo-
rithmes nous avions besoin d’un simulateur robotique. Nous avons opté pour le simulateur Webots.

#### 3.3.2 Moyen de validation choisit

```
Figure3.4 – Simulateur Webots
```
Webots est un simulateur open-source de robotique. Il est utilisé dans l’industrie, dans la recherche
et l’éducation. Le projet Webots a commencé en 1996, initialement développé par le Dr Olivier Michel.
Il utilise la librairie ODE (Open dynamics Engine), qui lui permet de simuler la vitesse, l’inertie, les
frottements, les collisions. Les programmes de contrôle du robot peuvent être écrites en C, C++, Java,
Python et Matlab au travers d’une API simple et complète. Il est facile d’importer un robot avec ses
capteurs ou de créer un nouveau robot.

```
Figure3.5 – Exemple de robots dans Webots
```
Le simulateur Webots va nous permettre de tester et d’affiner en simulation nos algorithmes de
navigation. Ils nous permet de créer un environnement, et de simuler certains robots. Nous pouvons
facilement récupérer les valeurs des capteurs et concevoir un algorithme pour interagir avec l’environ-
nement virtuel.

Nous avons conçu une réplique de notre robot en simulateur. Il possède les même dimensions que
notre robot réel, 16x26 cm. Il est composé de quatre roues, du tube d’aspiration et du lidar.


```
Figure3.6 – Le robot Responsibility dans le simulateur Webots
```
Pour se rapprocher le plus possible de la réalité nous avons aussi recrée un parc urbain en simulateur,
avec des bancs, une fontaine, des barrières, ainsi que des mégots de cigarette. La dimension du parc
est de 10x10m.

```
Figure3.7 – Fenêtre de simulation Webots
```
Les faisceaux bleus sont les faisceaux émanent du lidar. Le point bleu indique qu’il a rencontré un
obstacle ou que la portée maximale a été atteinte.

#### 3.3.3 L’évitement d’obstacle avec une architecture réactive

Pour respecter nos contraintes temps réel nous allons utiliser des méthodes réactives, qui en fonction
de l’acquisition détermine la puissance des moteurs. La stratégie que nous allons adopter est le guidage
vers un non objets, les obstacles.

Méthode de Braitenberg

```
Le véhicule de Braitenberg est populaire et simple.
```

```
Figure3.8 – Véhicule de braitenberg
```
Par exemple un robot avec deux capteurs de lumière et deux moteurs. Pour aller vers la lumière,
les capteurs vont directement inhiber ou stimuler les moteurs. C1 va stimuler le moteur M2 et C2 va
stimuler le moteur M1. Cela va avoir pour effet que le robot se dirige ver la lumière. En effet si le
but est à gauche le robot va tourner à gauche et inversement. Avec cette technique on peut facilement
additionner ou pondérer d’autres comportements utilisant les mêmes capteurs ou non.

Pour une première expérimentation d’évitement d’obstacle nous avons mis en œuvre un contrôle
réactif via la technique de braitenberg. Le lidar influt directement sur les moteurs. Les scans à gauche
stimule le moteur de gauche et les scans à droite stimule les moteurs de droite. Cette une technique qui
montre ses limites, dans certaines configations le robot va quand même être bloqué par un obstacle.
C’est une analogie de l’experience initiale de Braitenberg en utilisant les obstacles en tant que "non
buts".

Méthode de l’histogramme

Le lidar balaie son champ d’action et nous retourne 360 mesures correspondant aux 360 degrés
autour de lui. La méthode Vector Field Histogram consiste à discrétiser ces mesures pour les découper
en secteurs angulaires. On en déduit alors un espace d’occupation autour du robot.

```
Figure3.9 – 360 mesures du lidar
```

```
Figure3.10 – Discrétisation en zone
```
Dans notre expérimentation, on a discrétisé en 17 zones.

On en déduit un histogramme normalisé. Plus il y a d’obstacles dans la zone plus la valeur est proche
de 1 et inversement.

```
Figure3.11 – Histogramme des zones
```
En fonction de l’histogramme et du but, on déduit la direction. Par exemple, si on pouvait prendre
choisir la direction la moins encombrée et celle qui est la plus proche de l’avant du robot (c’est-à-dire
entre les zones 8 et 9), on choisirait alors la zone 9. Ce qui correspond à tourner légèrement à droite.

En terme de performance, cette méthode est un peu moins performante que celle de braitenberg, mais
pourrait être plus efficace (moins de collisions). Il est cependant difficile d’évaluer le temps d’exécution
d’une itération avec webots.


Méthode par Apprentissage par renforcement

L’apprentissage par renforcement permet d’apprendre un comportement à notre robot, c’est une
méthode très utilisé dans les jeux par exemple. En effet nous pouvons facilement contrôler l’environ-
nement. Dans le simulateur nous pouvons comme dans les jeux contrôler facilement l’environnement.
Par exemple, nous pouvons facilement : détecter une collision, réinitialiser le robot après une collision,
récupérer les informations des capteurs.

```
Figure3.12 – Principe de l’apprentissage par renforcement
```
```
Figure3.13 – Discrétisation en zone
```
L’état du robot est composé d’un tableau de 7 éléments (les 7 zones) et chacune peut prendre 3
valeurs possibles : 1 si la zone est peu obstruée, 2 si elle est moyennement obstruée et 3 sur elle est
très peu obstruée.

```
Les 7 zones sont définis par la formule suivante :
```
```
Zi=
```
```
∑lenghtZone
i=idxi valj
lenghtZone
```
##### ∗

##### 1

```
lidarMaxRange/ 3
```

```
A chaque Etat Z1 Z2 Z3 Z4 Z5 Z6 Z7 est associé un score
Soit :
```
```
— i la zone
— idxi le debut de la zone
— lenghtZone la taille de la zone
— lidarMaxRange la portée max du lidar
```
```
Fonction de mise a jour
```
```
Q[s,a] := (1−α)Q[s,a] +α
```
##### (

```
r+γmax
a′
```
```
Q[s′,a′]
```
##### )

```
Soit :
```
```
— alpha le taux d’apprentissage
— gamma le facteur d’actualisation
```
```
Figure3.14 – Récompense durant 300 itérations
```
La courbe des récompenses ne converge pas de manière franche, les résultats sont donc perfectible.
Même après 300 itérations il arrive au robot de heurter un obstacle très rapidement. On remarque
néanmoins une augmentation de longue itération sans collision.


```
Figure3.15 – Historiques de positions avant et après apprentissage
```
Par exemple durant la première itération le robot ne connait pas les actions à effectuer en fonction
de son état, il va rapidement heurter un obstacle. Alors qu’après l’apprentissage le robot arrive à faire
un plus long chemin.
L’apprentissage peut être réalisé sur simulateur, cela nous permet de gagner du temps, l’appren-
tissage est environ 7 fois plus rapide. Ensuite nous utilisons la table Q, qui associe l’état aux actions,
dans notre robot réel.

#### 3.3.4 Performance des différentes solutions

Temps d’exécution

Le temps d’exécution est un critère de choix dans un projet temps réel, nous avons donc comparé
le temps d’exécution de chaque méthode testée. Nous avons mesuré le temps de traitement (sans le
temps d’acquisition) durant 50 itérations. Ci-dessous les résultats de nos mesures.

```
Figure3.16 – Comparaison des performances temporelles en fonction de la méthode utilisée
```
Nous voyons que la méthode d’histogramme est la moins rapide, Braitenberg et Qlearning sont
quand à elles presque aussi rapide. Le temps d’exécution est négligeable sachant que notre objectif est
d’avoir un délai entre l’acquisition et l’action inférieur à 600ms.


## Chapitre 4

# Reconnaissance d’objet

Dans ce chapitre nous allons développer les différentes recherches et réalisations accompagnées des
tests sur le thème de la reconnaissance d’un objet spécifique grâce à l’utilisation de la vision.

### 4.1 Analyse de la problématique

Le but de cette partie, comme son nom l’indique est d’analyser la problématique. Pour cela, nous
allons dans un premier temps parler de nos recherches sur la vision. Puis, on fera le lien entre la vision
au sens physiologique et la vision à l’aide des capteurs, et enfin nous parlerons des objectifs que nous
souhaitons réaliser en adéquation avec nos recherches précédentes.

#### 4.1.1 La vision

Ce chapitre se base sur l’utilisation de la vision, nous allons donc commencer par parler de la vision
au niveau de l’être humain. Comment l’homme fait t-il pour voir un objet et même le reconnaître?

Chez l’homme la vision se fait naturellement, il nous suffit de regarder autour ne nous et on peut
voir toute sortes d’objet et même les reconnaître. Nous allons donc parler le l’organe de la vision qui
est l’œil et étudier son fonctionnement.

L’œil nous permet de capter la lumière de notre environnement et de la convertir en message nerveux,
lequel est transmis au cerveau qui l’analyse.

```
Figure4.1 – Schéma de l’œil
```
```
Comme on peut le voir l’œil est principalement composé de :
— La cornée : membrane transparente et résistante qui protège le globe oculaire.
— L’iris : diaphragme dont d’ouverture centrale est la pupille qui permet de doser la quantité de
lumière qui pénètre dans l’œil.
```

```
— Le cristallin (lentille molle) a pour but de focaliser le stimulus lumineux grâce à sa capacité à
modifier sa courbure.
— La rétine : membrane mince qui tapisse le fond de l’œil et sur laquelle se forment les images des
objets.
— Le nerf optique : il conduit les informations au cerveau, en passant par un relais appelé corps
grenouillé latéral situé à la base du cerveau qui joue le rôle d’amplificateur des signaux.
```
En résumé, la lumière de l’image en face de nous entre dans les yeux à travers la cornée. Puis, elle
aide à diriger la lumière vers la pupille et l’iris. Ces deux parties travaillent ensemble pour contrôler la
quantité de lumière entrant dans l’œil.

Une fois que la lumière rentre par la pupille, elle va traverser le cristallin qui va faire "la mise au
point d’un objet" et dirige la lumière vers la partie arrière de l’œil. Cette partie est appelée la rétine
et est composées de deux parties :
— Les cônes : permettent la vision diurne (vision photopique) et la vision des couleurs.
— Les bâtonnets : permettent la vision nocturne (vision scotopique).

Une fois que les cônes et les bâtonnets ont été exposés à la lumière, ils traduisent l’information visuelle
en information électrique. Le nerf optique va envoyer cette information au cerveau. Les minuscules
cellules nerveuses sont capables de prendre la forme électrique de l’image en face de nous et de l’envoyer
au cortex visuel du cerveau ou "Aire visuelle".

#### 4.1.2 Du monde réel au numérique

Maintenant que l’on a vu comment fonctionne la vision, on peut se demander comment ce principe a
pu être appliqué au monde du numérique.

Pour obtenir des images, on peut par exemple utiliser un appareil photo, mais alors quel est le lien
entre cette appareil et notre œil. Tous les deux sont des récepteurs de lumière et la comparaison peut
aller plus loin, car les deux sont munis de différentes parties remplissant des fonctions très proches.
Pour cela nous allons étudier le fonctionnement d’un appareil photo et analyser les différents liens.

```
Un appareil photo est constitué de :
```
```
Figure4.2 – Schéma de l’appareil photo
```
```
On peut donc assimiler certains éléments de l’organe visuel à l’appareil photo :
— La cornée avec la lentille d’entrée de l’appareil.
— Le cristallin avec l’objectif, réglables pour la mise au point.
— L’iris/pupille avec le diaphragme pour moduler la lumière entrante.
— La rétine avec le capteur photosensible pour recueillir l’image donnée de l’objet photographié.
```

Ainsi, ce qu’on a pu voir, c’est qu’en se rapprochant au maximum des mécanismes humain , on est
capable d’obtenir des images numériques qui pourront être analysées par la suite.

#### 4.1.3 Les images numériques

```
Maintenant, nous allons nous intéresser aux images numériques, il en existe deux types :
— Les images matricielles : tableau à plusieurs dimensions( "D = RGB, YUV etc...), dont chaque
case contient la valeur d’un pixel. Le terme pixel est la contraction de picture element. Les
images fournies par les systèmes d’acquisition (microscope, appareil photo...) sont de ce type.
Elles sont stockées typiquement dans des fichiers aux formats .tif, .bmp, .png, .jpg...
— Les images vectorielles représentent une image sous la forme d’une série de primitives géomé-
triques : segment, point, cercle, polygone... Leur gros avantage est que contrairement aux images
matricielles, on peut agrandir une image vectorielle autant que l’on veut sans perte de qualité.
Elles sont souvent utilisées pour sauvegarder le résultat de graphiques ou de dessins techniques.
```
### 4.2 État de l’art : études des solutions existantes

Avant d’arriver à un résultat favorable, nous avons penser à un certain nombre de solutions. En effet,
nous avons étudié deux méthodes principales, la première se basant sur le traitement d’image, et la
seconde sur de l’intelligence artificielle.

#### 4.2.1 Les méthodes de traitements d’images

Dans la section précédente, nous en sommes arrivés à la conclusion suivante : "Afin de reconnaître
des objets, nous devons travailler sur des images numériques".

Ainsi c’est la que s’adapte parfaitement le traitement d’image, en effet, il sert à "étudier les images
numériques et leurs transformations, dans le but d’améliorer leur qualité ou d’en extraire de l’informa-
tion".

Acquisition
La première étape du traitement d’images est ce que l’on appel l’acquisition. Cette étape va nous
permettre de récupérer l’image que l’on souhaite analyser, la première question à se poser est donc
"Qu’est ce qu’est une image ?".

Ainsi, suite aux différentes informations récupérées, notamment pendant nos cours de traitements
d’image, une image " est la représentation de l’intensité lumineuse à un moment donné dans l’espace".

```
Traitement
Elle est composée de plusieurs étapes :
— Amélioration de l’image : L’idée derrière les techniques d’amélioration est de faire ressortir
les détails qui sont obscurcis, ou simplement de mettre en évidence certaines caractéristiques
intéressantes d’une image. Tel que, changer la luminosité et le contraste, etc.
— Restauration d’image : Il s’agit également de l’amélioration de l’apparence d’une image. Cepen-
dant, contrairement à l’amélioration, qui est subjective, la restauration d’image est objective,
dans le sens où les techniques de restauration ont tendance à être basées sur des modèles ma-
thématiques ou probabilistes de dégradation d’image.
— Traitement d’image couleur : Il s’agit d’inclure la modélisation et le traitement des couleurs
dans un domaine numérique.
```

```
— Ondelettes et traitement multirésolution : C’est la technique de subdivision d’images succes-
sivement en régions plus petites pour la compression des données et pour la représentation
pyramidale.
— Compression : Elle concerne la réduction de taille requis pour enregistrer une image ou de la
bande passante pour la transmettre.
— Traitement morphologique : Il traite des outils d’extraction de composants d’image utiles à la
représentation et à la description de la forme.
— Segmentation : C’est le partitionnement d’une image en ses parties ou objets constitutifs
— Représentation et description : Ils suivent presque toujours la sortie d’une étape de segmentation,
qui est généralement constituée de données brutes de pixels, constituant soit la frontière d’une
région, soit tous les points de la région elle-même. Le choix d’une représentation n’est qu’une
partie de la solution pour transformer les données brutes en une forme adaptée au traitement
informatique ultérieur. La description traite de l’extraction d’attributs qui donnent lieu à des
informations quantitatives intéressantes ou sont de base pour différencier une classe d’objets
d’une autre.
— Reconnaissance d’objets : Le processus qui attribue une étiquette, telle que «véhicule» à un
objet en fonction de ses descripteurs.
```
#### 4.2.2 Utilisation de l’intelligence artificielle

Les méthodes de détection d’objets relèvent généralement d’approches basées sur l’apprentissage au-
tomatique ou d’approches basées sur l’apprentissage profond. Pour les approches de Machine Learning,
il devient nécessaire de définir d’abord les entités en utilisant l’une des méthodes ci-dessous, puis en
utilisant une technique telle que la machine à vecteurs de support (SVM) pour effectuer la classifica-
tion. D’un autre côté, les techniques d’apprentissage en profondeur sont capables de détecter des objets
de bout en bout sans définir spécifiquement les caractéristiques, et sont généralement basées sur des
réseaux de neurones convolutifs (CNN).
— Approches d’apprentissage automatique :
— Cadre de détection d’objets Viola – Jones basé sur les fonctionnalités de Haar
— Transformation d’entité invariante à l’échelle (SIFT)
— Histogramme des gradients orientés (HOG)
— Approches d’apprentissage en profondeur :
— Propositions de région (R-CNN, R-CNN rapide, R-CNN plus rapide, R-CNN en cascade)
— Détecteur MultiBox (SSD) à un coup
— You Only look once YOLO
— Réseau de neurones de raffinement à un seul coup pour la détection d’objets (FineDet)
— Retina-Net

### 4.3 Solution proposée et mise en œuvre

#### 4.3.1 YOLO

You only look once (YOLO) est un système de détection d’objets destiné pour le traitement
d’image en temps réel. YOLO est écrit pour Linux mais dans un premier temps, nous avons décidé de
l’utiliser sous windows via le port ( Darknet ).

Le vecteur des prédictions nous recadrons notre photo originale. YOLO divise l’image d’entrée
en une grille S * S. Chaque cellule de la grille prédit un objet. Par exemple, la cellule de grille jaune
ci-dessous essaie de prédire l’objet "mégot" dont le centre (le point bleu) tombe à l’intérieur de la
cellule de grille.


```
Figure4.3 – Image d’entrée divisée en imagettes
```
Pour chaque cellule de la grille,
— YOLO prédit B cases et chaque case a un score de confiance.
— Il détecte un seul objet quel que soit le nombre de cases B.
— Il prédit C probabilités de classes conditionnelles (une par classe pour la vraisemblance de la
classe d’objet).

Chaque case contient 5 éléments : (x, y, w, h) et un score de confiance. Le score de confiance reflète
la probabilité que la case contienne un objet et la précision de la case. Nous normalisons la largeur du
cadre englobant w et la hauteur h par la largeur et la hauteur de l’image. x et y sont des décalages
vers la cellule correspondante. Par conséquent, x, y, w et h sont tous compris entre 0 et 1.

Le réseau La structure du réseau ressemble à un réseau neuronal convolutif normal, avec des couches
de regroupement convolutionnelles et maximales, suivi par 2 couches entièrement connectées à la fin :

```
Figure4.4 – Réseau neuronal convolutif de YOLO [17]
```
Notez que l’architecture a été conçue pour être utilisée dans l’ensemble de données Pascal VOC,
où S = 7, B = 2 et C = 20. Cela explique pourquoi les cartes de fonctionnalités finales sont 7*7, et
explique également la taille de la sortie (7*7* (2 * 5 + 20)). L’utilisation de ce réseau avec une taille
de grille différente ou un nombre différent de classes peut nécessiter un réglage des dimensions de la
couche.


Les séquences de couches de réduction 1*1 et de couches convolutionnelles 3*3 ont été inspirées par
le modèle GoogLeNet (Inception).

La couche finale utilise une fonction d’activation linéaire. Toutes les autres couches utilisent un
RELU qui fuis
φx=x, si x >0; 0, 1 x sinon

Fonction de perte YOLO prédit plusieurs cases englobantes par cellule de grille. Pour calculer la
perte du vrai positif, nous voulons seulement que l’un d’eux soit responsable de l’objet. À cet effet,
nous sélectionnons celui qui a le plus haut IoU (intersection sur l’union) avec la vérité de terrain. Cette
stratégie conduit à une spécialisation parmi les prédictions du cadre englobant. Chaque prédiction
s’améliore pour prédire certaines tailles et proportions.

YOLO utilise une erreur de somme au carré entre les prédictions et la vérité du terrain pour calculer
la perte. La fonction de perte se compose de :
— Perte de classification : si un objet est détecté, la perte de classification à chaque cellule est
l’erreur quadratique des probabilités conditionnelles de classe pour chaque classe.
— Perte de localisation : la perte de localisation mesure les erreurs dans les emplacements et tailles
de zone de limite prédits. On ne compte que la case chargée de détecter l’objet.
— Perte de confiance : la perte associée au score de confiance pour chaque prédicteur de la case
englobante.

```
Figure4.5 – Fonction de perte de YOLO
```
Apprentissage Tout d’abord, nous pré-entraînons les 20 premières couches convolutives à l’aide du
jeu de données Cigarette Butt Dataset de Immersive Limit , en utilisant une taille d’entrée de 224*224.

```
Ensuite, nous augmentons la résolution d’entrée à 448*448.
```
Nous entraînons le réseau complet pendant environ 135 époques en utilisant une taille de lot de 64,
un élan de 0,9 et une décroissance de 0,0005.

Pour les premières époques, le taux d’apprentissage est lentement passé de 0,001 à 0,01. Nous l’en-
trainons pendant environ 60 époques, puis puis nous commençons à le diminuer.


Nous utilisons l’augmentation des données avec une mise à l’échelle et des translations aléatoires et
nous ajustons l’exposition et la saturation de manière aléatoire.

```
Figure4.6 – Exemple d’image utilisée pour l’apprentissage
```
Résultats Après 4 heures et 23 minutes d’apprentissage nous avons atteint 1000 itérations, avec une
perte moyenne de 0,0921. Cela peut-être considérée comme lent mais il est dû au GPU utilisé pour
notre test (AMD Vega 8).

#### 4.3.2 Traitement d’images

Dans cette section, nous allons expliquer les différentes méthodes de traitement d’images qui ont été
utilisées, leurs avantages et leurs inconvénients.

Avant-propos Avant de commencer, nous allons décrire les conditions dans lesquelles nous avons
réalisé ce qui nous a été demandé, pour cela nous disposions du matériel suivant :
— Un raspberry pi 4
— Une caméra pour raspberry
— Un robot
— La librairie OpenCV et NRC (que l’on utilise dans le module Traitement d’images)
— Une route nous permettant de tester nos différents algorithmes

Concernant la librairie NRC, nous avons quand même dû apporter certaines modifications. En effet
le type byte n’étant pas reconnu , nous avons dû modifier les fonctions "bmatrix()" pour l’allocation,
free_bmatrix() pour la libération mémoire, afin qu’elle prenne en compte un "unsigned char" à la place
des byte. De plus, nous avons créé deux fonctions :
— La première, MatToUnsigned(Mat image) qui va convertir l’image issue de la caméra en une
matrice d’unsigned char. On pourra donc par la suite effectuer le traitement sur cette matrice.
— La seconde UnsignedToMat(unsigned char **resultat) qui va effectuer l’opération contraire. Une
fois le traitement terminé, on reconvertit la matrice résultat en type cv : :Mat afin que la caméra
puisse l’afficher.

```
Première solution
```
Filtre de Canny Afin de commencer, la détection de contour est la première et la plus importantes
des étapes, elle va nous permettre de retourner une image en noir et blanc et qui sera utilisée en début
de deuxième étape.

Il nous est demandé d’utiliser le "Filtre de canny", en effet, il permet une meilleure détection des
contours et ceci pour plusieurs raisons. Pour commencer, nous avons d’abord dû réduire le bruit, pour


cela on a appliqué le filtre Gaussien suivant :



##### 1 2 1

##### 2 4 2

##### 1 2 1

##### 

##### 

```
Comme on peut le voir l’application d’une gaussienne va donc lisser le signal et donc réduire le bruit.
```
```
Par la suite on a appliqué un masque de Sobel en x (Gx) :


```
##### −1 0 1

##### −2 0 2

##### −1 0 1

##### 

##### 

```
et y (Gy) : 
```
```

```
##### − 1 − 2 − 1

##### 0 0 0

##### 1 2 1

##### 

##### 

```
Cela nous a donc permis de calculer le gradient d’intensité.
√
Gx^2 +Gy^2
```
.

Enfin, pour finir on a appliqué un Hystérésis, c’est à dire afin de seuiller les contours. Pour cela, on
a recherché la valeur maximum de notre image puis on a appliqué un seuil haut :

```
max∗
```
##### 60

##### 100

Et un seuil bas :

```
max∗
```
##### 30

##### 100

On a accepté les points supérieurs au seuil haut, on a rejeté les points inférieurs au seuil bas et pour
finir, entre le seuil bas et le seuil haut, nous avons accepté le point s’il était connecté à un point déjà
accepté.

Optimisations Afin d’optimiser notre code, nous avons décidé d’utiliser des threads afin de paral-
léliser certaines étapes de calculs, notamment pour le calcul de gradient.

Détection de rectangles Une fois que nous avons notre image de contours en niveaux de gris, il
nous faut maintenant élaborer un algorithme nous permettant de détecter les lignes droites dans une
image. C’est ce que l’on appelle la "transformée de Hough".

Transformée de Hough Le principe de cette transformée est assez simple, il faut parcourir chacun
des pixels de l’image, et pour chacun des angles choisis, il faut regarder le nombre de droites passantes
par celui-ci.

Donc pour chacun des pixels (Image(i,j)) et pour chacun des angles ([0 :180], par pas de 1 pour
commencer) on applique alors la formule suivante :

```
x∗sin(
θ∗π
180
```
```
) +y∗cos(
θ∗π
180
```
##### )


Puis, on parcourt le tableau et on cherche l’angle ayant obtenu le plus de vote. Et pour finir on
reconstruit la droite que l’on a trouvée :

```
yimage=
```
```
(rhoDetect−rhomax−(ximage∗sin(thetaDetect 180 ∗pi)))
cos(thetaDetect 180 ∗π)
```
```
Avec
— rhoDetect la valeur maximum du tableau
— thetadect l’angle qui y est associé.
— Et rhomax = √
image.cols^2 +image.rows^2
```
Dans notre cas, notre but était de détecter les formes rectangulaires, nous avons donc décidé de
dessiner toutes les droites supérieures à un certain seuil. Ensuite, afin d’obtenir nos rectangles, on a
délimiter nos droites afin de retrouver les formes recherchées de l’image initiale. Pour cela, on part
des bords de l’image (haut, bas, droite, gauche) et on noircit l’image jusqu’à obtenir un pixel de forte
intensité (qui appartient donc à une intersection). On en obtient ainsi nos formes rectangulaires.

Optimisations Comme optimisation de cet algorithme on a opté pour la diminution du pas de
discrétisation, initialement nous avons testé un pas de 1 mais afin d’augmenter au maximum la fluidité
on a décidé d’utiliser un pas de 10.

Mise en relation Dans cette section nous allons maintenant voir qu’elle est l’utilité des deux étapes
précédentes, en effet le but final du projet est donc de pouvoir faire avance le robot de la manière la
plus fluide possible.

Avantages/Désavantages Pour commencer, cette méthode est assez avantageuse, en effet elle ne
met que très peu de temps à être implémenté. Elle nous permet d’avoir un résultat assez rapidement
et ainsi pouvoir tester certains de nos algorithmes. Pour rappel, nous sommes dans une situation de
temps réel où les échéances à respecter sont importantes, cette méthode qui demande tout de même
un certain taux de complexité reste relativement gérable à l’échelle de notre système. En effet, une fois
la méthode implémentée, nous avons un nombre d’images par seconde égal à 12.

Maintenant, parlons de l’efficacité de notre algorithme, le but n’étant pas de simplement détecter
des rectangles mais bel et bien de reconnaître des mégots de cigarettes. Dans un premier temps, le fait
de reconnaître des rectangles représente un premier cas parfait et tout à fait optimiste où notre mégot
de cigarette n’est pas abîmé. Par la suite, un mégot est un objet assez petit, donc beaucoup plus dur
a détecter, il n’est généralement pas en mouvement donc la plupart des algorithmes de détections sont
assez difficile à mettre en place. Dans notre cas, nous avions deux choix :
— Ne réduire que très peu le bruit afin de détecter au maximum les objets de très petites taille.
On augmente alors la complexité de l’algorithme.
— Réduire le bruit, on obtient alors une carte des contours assez net sur laquelle on peut alors
rechercher nos objets.

C’est à ce moment là que la problématique de la taille de l’objet entre en compte, dans un premier
on détecte beaucoup trop d’élément, ce qui devient ingérable pour notre système. Dans un second,
cette méthode n’est pas efficace pour des objets de trop petite taille. En effet, lorsque l’objet est très
proche de la caméra ( ce qui ne nous convient absolument pas ), alors notre mégot est détecté, ce qui
représente un cas précis mais aussi optimiste et qui ne pourra être appliqué en temps-réel.

De plus une nouvelle problématique entre alors en jeu, une fois les rectangles détectés, comment
s’assurer qu’il s’agit bien de mégot de cigarettes?


Utilisation de descripteurs C’est alors que se pose la question de l’utilisation de descripteur. Un
descripteur est une caractéristique spécifique associée à un élément ( l’écart-type, la moyenne des pixels
en niveaux de gris etc..)

Dans notre cas, nous avons pensé à certains descripteurs comme la moyenne des couleurs sur les
plans RGB ( RED, GEEN, BLEU), la moyenne en niveau de gris ou encore la taille. Pour cela, nous
avons pris un certains nombre d’images de mégot de cigarettes sur internet et nous avons alors calculé
ses descripteurs et enfin nous avons moyenné ses valeurs afin d’obtenir notre descripteur "MÉGOT"
qui sera utilisé en tant que descripteur test.

Avantages/Désavantages Maintenant, on peut se demander si cette méthode est avantageuse ou
non. Avant de commencer, il faut alors se demander quel type de mégot nous voulons détecter? Dans
notre cas, nous aimerions détecter tous type de mégots ( voir image ci-dessous).

```
Figure4.7 – Quelques types de mégots possibles
```
Cette méthode nous permet alors de chercher un élément de manière plus efficace dans une image.
Il y a donc maintenant plusieurs inconvénients à utiliser cette méthode :
— Très difficile à utiliser seule, en effet il faut alors prendre une matrice suffisamment grande
et ainsi balayer l’image pixel par pixel, la complexité est alors très grande et le nombre de
traitement assez conséquent.
— Le nombre de descripteur est alors très influent, il faut dans un premier temps en choisir suf-
fisamment et dans un second ne pas trop en prendre afin de ne pas surcharger le système(
similaire aux notions de sur et sous-apprentissages).
— Le fait que l’on souhaite détecter tous les types de mégot, le descripteur se trouve erroné, il
faudrait alors créer des descripteurs pour chaque type de mégots.

Détection de rectangle + utilisation de descripteurs Maintenant si l’on décide d’utiliser nos
deux méthodes de manière combinée, c’est à dire que dans un premier temps, on récupère nos rectangles
puis dans un second on utilise nos descripteurs pour vérifier la présence d’un mégot.

Prenons l’exemple d’un cas parfait, notre mégot est rectangulaire et que nos descripteurs sont bon.
Dans ce cas là, il est alors légitime d’utiliser cette méthode et elle fonctionnerais très probablement.

Maintenant, suite à plusieurs tests, il y a beaucoup trop d’inconvénient à utiliser cette méthode, en
effet :
— Le mégot ne doit pas être trop abîmé


```
— La taille ou encore la distance sont beaucoup trop influentes
— L’environnement n’est en aucuns cas pris en compte( taux de luminosité principalement)
— Le nombre de descripteur est trop important
```
Il est donc possible de mettre en œuvre cette méthode, mais pour la rendre vraiment efficace (
surtout en temps-réel ) tous en prenant en compte les contraintes matériel.

Il semble donc plus efficace de se tourner vers des méthodes ayant un niveau similaire de complexité
mais avec un résultat plus optimal.

```
Deuxième solution
```
Maintenant, nous allons étudier les différentes méthodes se basant sur une détection de couleur qui
peuvent nous amener aux résultats souhaités.

```
Afin de mieux comprendre cette solution, on peut regarder le schéma suivant de notre algorithme :
```
```
Figure4.8 – Algorithme de notre seconde solution
```
```
Nous allons maintenant détailler chacune de ces étapes et expliquer leur principe.
```

Segmentation par couleur La première étape effectué a donc été une segmentation par couleur,
c’est à dire que nous avons, pour chaque pixel de l’image, rechercher les pixels dont la couleur était
assez proche de la couleur d’un mégot de cigarette.

```
Pour ce faire, nous avons procédé comme suit :
— Nous avons préalablement définis la couleur d’un mégot de cigarette, pour cela nous avons
choisit une couleur se rapprochant le plus d’un mégot et récupérer les valeurs RGB. Dans le cas
actuel, nous avons conserver les valeurs R=220, G=212, B=3.
— Ensuite, nous avons donc pour chaque pixel effectué un calcul de distance entre les 3 plans de
notre pixel et nos valeurs choisit, on a donc :
```
```
distanceColor=
```
##### √

```
(b−colorOrange.bplan)^2 + (g−colorOrange.gplan)^2 + (r−colorOrange.rplan)^2
```
```
— Une fois la distance obtenue, nous avons établie un seuil qui en premier lieue a été définis de ma-
nière arbitraire, puis affiné par la suite. Si la distance est inférieur à ce seuil le point est accepté,
sinon il est mis à O ( distanceColorOrange < COLOR_ORANGE_DISTANCE_TRESHOLD
).
— On obtient alors au final une image en niveaux de gris ne contenant que les pixels les plus
proches de la couleurs d’un mégot.
```
Détection de contour Pour cette étape, nous avons cette fois utilisé une détection de contours
simple, c’est à dire que l’on a appliqué un filtre de Sobel en x et en y, puis nous avons calculer le
gradient.

```
Masque de Sobel en x (Gx) : 
```
```

```
##### −1 0 1

##### −2 0 2

##### −1 0 1

##### 

##### 

```
et y (Gy) : 
```
```

```
##### − 1 − 2 − 1

##### 0 0 0

##### 1 2 1

##### 

##### 

```
Cela nous a donc permis de calculer le gradient d’intensité
√
Gx^2 +Gy^2
```
.

On a alors de nouveaux appliqué un seuil encore une fois de manière aléatoire que l’on a affiné suites
au expérimentations. On a mis à 255 si le pixel est supérieur au seuil et à 0 sinon. ( gradientSobel[i][j]
> TreshHoldBorderDecetionCOLOR ).

Réduction de bruit Ici, nous avons utilisé la morphologie mathématique binaire, c’est à dire que
l’on a décidé d’utilisé des opérations appelés " dilatation " et " érosion ". Pour faire simple et court,
la dilatation peut alors être interprétée comme une croissance de 1 pixel et va donc pour avoir effet de
boucher les trous. L’érosion peut être interprétée comme une contraction de 1 pixel et va supprimé les
fausses alarmes comme les pixels isolés.


Une fois ces deux opérations implémentées, nous avons alors pu mettre en place les ouvertures et
fermetures :
— L’ouverture va dans un premier temps effectué un certains nombre d’érosions puis a la suite
un certains nombre de dilatation. L’ouverture servira donc à supprimer les fausses alarmes et
réduire très fortement le bruit.
— La fermeture va dans un premier temps effectué un certains nombre de dilatation puis a la suite
un certains nombre d’érosions. La fermeture supprimera les trous, ce qui est très important pour
l’étape suivante.

```
Segmentation en régions
```
On a alors effectué une segmentation par étiquetage des régions, le but est donc de regroupé les
pixels similaires afin de créer des régions qui deviendrons nos zone d’intérêts a développer

Suite à la segmentation on ne retrouve alors que quelques régions ( donc nos zones d’Intérêts ), on
a alors :

```
Figure4.9 – Image après segmentation
```
Application des descripteurs aux différentes régions Au final on obtient un certain nombre de
zones correspondant à ce qui pourrait correspondre à nos zones de traitements, il faut alors appliqué
ce que l’on appel des descripteurs à nos zones afin de déterminer quelles zones sont le plus proche
d’être un mégot. Pour l’instant nous n’avons pris que la taille ( nous ne pas prendre les pixels isolés
qui persisterai ou encore les trop grandes zones dans de cas très rare ) et la couleur.

Avantages et inconvénients Ici, on a une méthode qui semble être assez efficace, en effet on
arrive à ne reconnaître que les mégots et à les situer notamment grâce au calcul du barycentre des
zones d’intérêts.

Le gros inconvénient de cette méthode est que l’intensité lumineuse est beaucoup trop importante,
en effet, si l’on teste cette méthode sur des images simple alors le résultat est probant. Mais lorsque
que l’on passe en temps-réel, les contraintes de luminosité entrent en jeu, en effet d’un lieu à un autre
en journée ou le soir la méthode marche assez correctement mais des correctifs sont alors à appliquer.
En effet, nous avons utilisé une batterie de seuil en salle de cours et le résultat était convenable, mais
lorsque l’on change, par exemple dans un endroit plus lumineux, il faut alors modifier nos seuils afin
de refaire marcher notre méthode.

De plus, cette méthode marche assez convenablement pour les mégots dit "Orange" mais cela beau-
coup trop compliqué pour les mégots blancs.


Teinte, saturation, luminosité (ou valeur) sont les trois paramètres de description d’une couleur dans
une approche psychologique de cette perception. Cette expression désigne des modèles de description
des couleurs utilisés en graphisme informatique et en infographie.
permet de séparer des composantes la luminance

```
Troisième solution
```
Le principal problème quand à la solution précédente est la trop grande importance de la luminosité.
Nous en sommes donc au point où nous avons une solution qui marche mais seulement lorsque certaines
conditions sont réunies. Si l’on change d’endroit où d’environnement alors il faut aussi changer les
conditions ( les conditions se traduisent par les différents seuils utilisés ).

Changement d’espace colorimétrique Pour toutes les solutions précédentes, nous étions dans
l’espace colorimétrique RGB ( RED, GREEN, BLUE ) qui est l’espace le plus utilisé. Suites à diverses
recherches, nous avons apris l’existence de l’espace HSV ( HUE, SATURATION, VALUE ), qui comme
la plupart des espace est constitué de trois composantes :
— HUE ou teinte ( de 0 à 360 °)
— Saturation ( de 0 à 100% )
— Brillance ( de 0 à 100% )

Pourquoi avoir choisit cet espace? Pour une raison simple, il n’est en aucun cas dépendant de la
luminosité qui était notre principal problème.

```
Pour ce faire, on a suivis a peu près la méthode utilisée dans l’espace RGB :
— On a converti la couleur RGB dans l’espace HSV
— Segmentation par couleur, on a accepté les couleurs entre un seuil bas et un seul haut ( choisit
arbitrairement mais tout de même assez proche des valeurs exactes de la couleur ).
— Affichage des pixels pris en compte.
```
Avantages/Inconvénients Le principal avantage est le fait de se passer de la luminosité, ce qui
peut être efficace. Mais dans notre cas, après plusieurs tentatives, modifications des seuils etc... Il s’avère
que nous n’avons pas réussi à reconnaître le mégot de cigarette. Cela s’explique, par le fait qu’un mégot
n’est pas suffisamment texturer et sa saturation est beaucoup trop faible pour être détectée.

Quatrième solution
Comparaison d’histogramme Pour les deux algorithmes qui suivent, nous devons dans un pre-
mier temps prendre ce que l’on va appeler l’image de référence, dans notre cas, nous allons donc
prendre une image d’un mégot de cigarette que nous allons recadrer. Suite à cela, nous allons calculer
l’histogramme couleur de cette image ( un histogramme par plan RGB ).

Probabilités d’histogramme : principe Nous avons dans un premier temps pris ce que l’on
va appeler l’image de référence, dans notre cas, nous allons donc prendre une image d’un mégot de
cigarette que nous allons recadrer. Nous avons détouré notre mégot de cigarette et nous avons mis en
blanc ( R=255, G=255, B=255) tous le reste de l’image, afin de garder seulement le mégot. Nous avons
essayé avec les différentes images suivantes, afin d’avoir un résultat optimal :

```
Figure4.10 – Images de référence
```

Nous avons alors calculer l’histogramme couleur de chacune des images de référence. C’est à dire,
pour chacun des pixels de notre image de référence, on regarde le nombre de fois qu’apparaît cette
valeur, et ce sur les trois plans ( RGB ). Par la suite, on a normalisé l’ensemble des valeurs des
histogrammes entre [0 ;1] afin de pouvoir faire un calcul de probabilité par la suite.

```
On obtient donc les histogrammes suivants :
```
```
Figure4.11 – Histogramme du plan rouge
```
```
Figure4.12 – Histogramme du plan vert
```

```
Figure4.13 – Histogramme du plan bleue
```
Ensuite, nous avons testé cette méthode sur des photos que nous avons nous même prise dans
différents lieux de la fac. Pour ce faire, nous avons donc pris un image, et pour chacun des pixels, on
regarde le nombre de fois qu’il apparaît dans l’histogramme couleur de notre image de référence. Nous
avons effectué nos premiers tests seulement a l’aide de la bibliothèque NRC.

```
Pour cela, nous avons utilisé le calcul de probabilité suivant :
```
```
ProbaPixel[i][j] =HistoR[Pixel[i][j].r]∗HistoG[Pixel[i][j].g]∗HistoB[Pixel[i][j].b]∗nbPixels
```
HistoR, HistoG, HistoB sont les histogrammes sur les composantes RGB de l’image de référence (
normalisés en divisant par le nombre de pixels).

```
Pixel correspond au pixel de l’image à comparer.
```
```
Ici, on multiplie par le nombre de pixels afin d’obtenir des résultats satisfaisants.
```
Puis, nous avons utilisé un seuil ( choisit arbitrairement pour commencer ), si la probabilité est
supérieur à ce seuil alors le point est accepté.

```
Test sur des images statiques On obtient donc les premiers résultats suivant :
```

```
Figure4.14 – Résultats tests statiques
```
Test en temps réel Nous avons donc implémenté cet algorithme en temps réel, pour cela nous
avons préenregistré les histogrammes des images de référence, et nous avons alors utilisé le même


algorithme. Nous avons testé en intérieur :

```
Figure4.15 – Résultats temps réel intérieurs
```
```
Figure4.16 – Résultats temps réel extérieurs
```
Nous avons réussis à localiser les mégots de cigarettes, il nous reste maintenant à trouver l’emplacement
précis de ce mégot dans l’image.

Réduction du bruit Au début, nous avions un résultat un peu trop bruité, en effet il y avait pas
mal de pixels isolées qui venait nuire à la qualité du résultat et nous empêchait de trouver efficacement
la position du mégot.

Pour ce faire, nous avons donc modifié certains éléments, dans un premier temps appliqué l’algorithme
suivant :
— On parcours l’image pixel par pixel
— Pour chacun des pixels, détectés comme possible pixel d’un mégot.
— On regarde chacun de ses voisins( connexes 9 ), et si aucun de ceux là n’est détecté
— Alors, on le considère comme non détecté


Ensuite, les mégots sont convenablement détecté, mais le résultat n’est pas suffisamment convenable
afin de procéder à la localisation. En effet, comme on a pu le voir précédemment, nous avons une zone
contenant une forte concentration de pixel détecté. Cette zone n’étant pas compacte et ne formant pas
de figure particulière, il est alors très difficile de s’en servir comme tel.

Nous avons donc utilisé ce que l’on appel les opérations de morphologie mathématiques. Plus pré-
cisément, nous avons décidé d’utiliser les mécanismes d’ouverture et fermeture qui sont composés des
mécanisme appelé érosion et dilatation :
— Érosion : Un peu comme notre méthode utilisée précédemment, on va regarder le voisinage
d’un pixel et si un seul de ses voisins n’est pas détecté, alors le pixel sera considéré comme non
détecté.
— Dilatation : Au contraire, cette fois si un seul des voisins est détecté alors le pixel sera considéré
comme détecté.
— On peut maintenant parler de l’ouverture, elle sert à supprimer les fausses alarmes, on applique
dans un premier temps un certain nombre d’érosions pour supprimer les pixels isolés, puis le
même nombre de dilatation afin de conserver les proportions de notre objet.
— La fermeture, elle comme son nom l’indique, à pour but de boucher les trous, on applique donc
en premier, des dilations, puis le même nombre d’érosion.

```
Figure4.17 – Résultat morphologique
```
On applique donc 2 fermeture afin de boucher les trous, puis 4 ouverture afin de récupérer les
proportions. On obtient ainsi, une zone compacte de forme rectangulaire.

Étiquetage des régions Nous avons donc maintenant, une image avec simplement notre objet.
Le but va donc être de décomposer notre image en plusieurs zones ( par exemple, une zone par mégot
de cigarette ). On utilise donc l’algorithme d’étiquetage des régions vu et implémenté en cours. On
obtient donc un certain nombre de zones, sur lesquelles on peut travailler assez facilement.


```
Figure4.18 – Résultats Étiquetage
```
Utilisation de descripteurs Dans notre cas, nous avons, pour chaque région susceptible d’être
un mégot, calculer un certain nombre de données. Dans un premier temps, nous avions choisi :
— La taille de la région
— La moyenne en niveaux de gris
— La moyenne sur le plan R, G et B
— Le barycentre de la région

### 4.4 Tests et certifications de la solution

#### 4.4.1 Tests sur YOLO

En premier lieu, nous avons testé notre YOLO sur des images statiques. Nous avons réussi à avoir
des résultats satisfaisants. Le temps d’exécution pour la détection de mégot sur des images statiques
est inférieur à 2s.


```
Figure4.19 – Exemple de résultat sur une image contenant un mégot
```
Ensuite, nous avons testé notre implémentation de YOLO sur un flux vidéo temps-réel de la webcam.

Avec une résolution de 1920*1080, nous avons réussi à avoir des FPS entre 4 et 6. Une résolution de
800*600 nous a permis d’avoir un FPS entre 8 et 9.

Le FPS dépend aussi de la luminosité. Dans un endroit sombre le FPS est entre 1 et 2, peu importe
la résolution de la webcam.

Nous avons réussi à avoir une détection de mégot avec un pourcentage de confiance supérieur à 18%
dans des endroits bien éclairés. Le pourcentage de confiance diminue si le mégot est dans un endroit
sombre.


```
Figure4.20 – Détection du mégot sur le flux de la webcam
```
#### 4.4.2 Tests sur Les méthodes de traitement d’images

Test de la méthode basée sur la transformée de Hough
Comme expliqué dans la section précédente, les inconvénients que nous avions prévus ont bien été
au rendez-vous. Les principaux problèmes rencontrés sont :
— La reconnaissance du mégot est assez faible, en effet comme expliqué précédemment, le mégot
se doit d’être d’une forme quasi-parfaite afin de pouvoir être détecté comme rectangle. De plus,
le mégot doit être assez visible, étant de petite taille il n’était pas toujours détecté par notre
système.
— Ensuite l’autre problème majeure est le fait de détecter " beaucoup trop de rectangles. Si l’on
met un mégot sur une feuille blanche alors il n’y a aucun soucis pour notre système. Pa contre
en extérieur, par exemple sur une route pavé, beaucoup trop d’éléments sont détectés il est alors
impossible d’analyser les zones pour savoir laquelle peut être un mégot. De ce fait l’algorithme
n’est pas efficace et pèse bien trop sur notre système globale qui plus est n’est pas constitué que
de la vision.
— On atteint donc 13 FPS sur nos ordinateurs personnel pour les rares cas où un mégot semble
être détecté.

Test de la méthode basée sur la segmentation par couleur
Ici, le principal problème se trouve être la luminosité. En effet, la détection se trouve être assez
bonne mais les seuil choisi doivent en permanence être modifié en fonction du lieu où on se trouve. Par
exemple, une fois les seuils définis la détection était convenable, mais si on changeait de lieu dans la
fac alors il fallait modifier les seuils.

De plus, la couleur du mégot que nous avons choisit est arbitraire( sur ce qui selon nous se rapprochait
le plus de la couleur du mégot ). La méthode ne pouvait pas être extrêmement précise. Lorsque les
conditions étaient parfaitement réunies, la détection était assez fluide ( environ 40 FPS ) mais ne
semblait pas envisageable pour la suite.


Test de la méthode basée sur les probabilités d’histogramme
Nous avons effectué tous nos tests en utilisant deux mégots de cigarette en simultané. Suite à nos
tests, nos différents algorithmes fonctionnent, néanmoins le calculs du barycentre de la zone ne donne
pas de résultat précis, il faut donc trouver une solutions afin d’améliorer nos résultats. Actuellement,
pour cette méthode nous avons un totale de 60 FPS, la fluidité peut encore être améliorée.

```
Voici quelques screens de nos résultats
```
```
Figure4.21 – Résultats des tests
```
Afin d’optimiser nos résultats, nous avons effectué un certain nombre de changements dans notre
code :
— Tout d’abord, nous avons changé notre image de référence, que nous avons remplacé par deux
nouvelles images. La première photo a été prise dans un endroit ensoleillé, et la seconde à
l’ombre.

```
Figure4.22 – Nouvelles images de référence ( Lumière/Ombre )
```
```
— On a modifié l’utilisation des seuils, avant l’utilisation était brut ( 1500 pixels etc.. ), on les a
donc converti en pourcentage.
— On demande maintenant à l’utilisateur si il souhaite afficher les images résultantes ( afin d’aug-
menter les performances lorsque l’affichage n’est pas nécessaire) et si il souhaite activer la liaison
série.
```

```
— Ajout d’une fonction permettant de transmettre la position du mégot, et capable de déduire si
il se trouve à gauche, à droite ou au milieu.
```
Nous avons ensuite transféré le code sur notre Raspberry afin de prendre la température et de tester
les performances. Ainsi, nous avons pu voir que les performances n’étaient pas suffisantes en 720p, nous
avons donc diminué la résolution par 2 (360/240). Ainsi les performances obtenues précédemment ont
été conservé et nous restons aux alentours des 40 FPS. L’effet immédiat de cette baisse de résolution
est que le robot doit être assez proche du mégot afin de le détecter.

```
Comparatifs des différentes méthodes
Tableau comparatif de nos méthodes
```
```
Figure4.23 – Tableau comparatifs de nos méthodes )
```

## Chapitre 5

# Asservissement

En robotique, l’asservissement consiste à donner un objectif au robot, en général un mouvement (avan-
cer, lever le bras) qui devra alors l’exécuter le plus rapidement possible. Nous allons donc dans ce
chapitre analyser les problématiques liées au cas de notre robot, ainsi qu’exposer nos solutions et les
tests réalisés pour les valider.

### 5.1 Analyse de la problématique

#### 5.1.1 La communication

L’enjeu est de mettre en œuvre un moyen de mettre en action le robot à partir des données collectées
sur son environnement. La solution doit être fiable afin que le robot n’ait aucun problème de psycho-
motricité entre ce qu’il voudrait faire et le mouvement exécuté, mais elle doit être en plus réactive. Car
si un obstacle est détecté alors que le robot met du temps à réagir entre les mesures des capteurs, la
décision et l’exécution, il risque de rentrer en collision avec l’obstacle.

#### 5.1.2 L’exécution

Lorsque l’ordre est reçu par le microcontrôleur, il faut alors l’exécuter. Alors que nous avons trois
types de composants à asservir ; la turbine pour l’aspiration, les cartes moteurs pour le déplacement
et le servomoteur pour le bras d’aspiration, il faut alors savoir différencier l’ordre reçu.

### 5.2 État de l’art : études des solutions existantes

#### 5.2.1 Les transmissions de données

La portée de transmission

Il existe plusieurs manières d’établir une transmission de données entre de périphériques, on peut le
faire sans fil par exemple via Bluetooth ou wifi. Ou alors nous pouvons employer une méthode physique
que l’on privilégiera puisque nos deux périphériques à savoir le Raspberry et le microcontrôleur n’ont
pas de contraintes de distance puisque dans le même robot, ce sera ainsi un gain en coût de fabrication
et de développement.

La catégorie de transmission

On peut également diviser tous ces types de transmission en deux autres catégories, les transmissions
série et parallèle. Ainsi, la transmission parallèle permet l’envoi de données sur plusieurs voies parallèle.
C’est donc un gain en vitesse, mais un coût énergétique et de développement supplémentaire dont
on pourrait se passer puisqu’il n’y aura que très peut de donner à transmettre. C’est pourquoi à
contrario, la liaison série qui envoie les données les unes après les autres suffit largement à notre robot
Responsibility.


De même, il existe une sous-catégorie de transmission supplémentaire. Il s’agit des transmissions
séries synchrones qui, dans les paquets de données nécessite un signal de synchronisation pour le péri-
phérique de réception. En revanche, la transmission asynchrone n’en nécessite pas, elle seulement que
les périphérique émetteur et soit en phase sur le débit de transmission. Les transmissions asynchrones
sont suffisantes dans les cas ou la quantité de données à échanger n’est pas trop importante, ce qui est
pour rappel notre cas.

#### 5.2.2 Le temps réel

Pour conserver l’intégrité du robot, c’est à dire qu’il ne ce dégrade pas du à des erreurs de trajectoire,
il est nécessaire qu’il fonctionne en temps réel avec son environnement. Il existe alors deux types de
temps réel, le temps réel dur et le temps réel mou.

Le temps réel dur

Le temps réel dur est stricte vis-à-vis des échéances de l’exécution des tâches, c’est pourquoi le
temps réel dur exige de respecter une limite temporelle. Les tâches à exécuter doivent pour cela êtres
interruptibles et prioritaires sur les autres tâches non-essentielles pour garantir que le système soit
temps réel. Une tâche prioritaire va donc interrompre la tâche courante pour garantir le respect de
la limite temporelle. Les systèmes temps réel sont par conséquent très employés dans les domaines de
l’aéronautique et de l’automobile pour les systèmes de pilotages embarqués.

Le temps réel mou

Le temps réel mou est plus tolérant sur les dépassements de limite temporelle, il doit cependant
garantir un sentiment de temps réel pour l’utilisateur qui souhaite que sont système soit réactif à
ses actions. Il est donc plus employés dans des applications tel que les messageries instantanées, de
visioconférence, les jeux vidéo.

### 5.3 Solution proposée et mise en œuvre

#### 5.3.1 La chaîne de communication

Le choix d’employer un microcontrôleur permet de séparer les deux tâches essentielles à un robot
intelligent, la prise de décision et l’exécution de l’action.

```
Figure5.1 – Schéma de la chaîne de communication
```

Ainsi, comme présenté sur le schéma ci-dessus de la chaîne de communication, la première étape
consiste à l’envoi de l’ordre décidé par la Raspberry au microcontrôleur. L’étape suivante est l’inter-
prétation du message reçu pour ensuite faire exécuter la bonne action au bon membre. Une fois ceci
fait, un message d’acquittement est retourné au Raspberry afin qu’il ait connaissance de la bonne
réalisation de l’action.

#### 5.3.2 La liaison série

La transmission du message entre le Raspberry et le microcontrôleur se fait via une liaison série
physique de type UART.

Universal Asynchronous Receiver-Transmitter (UART) , permet la transmission et la récep-
tion asynchrone entre deux périphériques différents puisque universelle. L’UART a été choisit pour sa
simplicité de mise en œuvre, son efficacité et le peut de quantité de données à échanger.

```
Figure5.2 – Schéma de la liaison UART
```
Comme dit précédemment, la mise en œuvre physique de la liaison UART est simple, le port de
réception (RX) de chacun des deux est connecté au port de transmission (TX) de l’autre, avec en plus
une liaison sur la masse pour éviter les masses communes.

Pour faciliter la configuration de leurs microcontrôleurs, STM32 a mis à disposition un outil (STM32
Cube MX) que nous avons utilisé pour établir la liaison UART. Ainsi nous avons pu l’utiliser pour
attribuer les ports utilisés par la liaison, ainsi que mettre une vitesse de transmission et de réception
(Baud Rate) de 115 200 Bits/s. Ainsi que la taille des mots (Word Length) de 8bits.

```
Figure5.3 – Capture d’écran du logiciel STM32 Cube MX
```
#### 5.3.3 Le temps réel

Afin de d’assurer la réactivité abordée précédemment, nous avons utilisé des interruptions externes
sur le microcontrôleur, afin que chaque fois que le Raspberry transmet une consigne, le microcontrôleur
l’exécute en temps réel.


Une interruption externe correspond à l’interruption du processus courant par un évènement exté-
rieur au système, pour exécuter la fonction liée à l’interruption. Dans notre cas nous avons alors établie
une interruption externe sur la liaison UART, de manière à ce que chaque fois que le microprocesseur
reçoit de l’information sur la liaison, il décode immédiatement le message reçu par le raspberry pour
traiter l’ordre en priorité.

#### 5.3.4 L’asservissement

une fois la consigne décodée par le microprocesseur, il n’y a plus qu’à l’exécuter. Pour cela, le
microprocesseur doit asservir les composants du robot via des signaux appelés PWM.

Pulse Width Modulation (PWM) , il s’agit d’un signal dont le rapport cyclique peut être modulé.
Ainsi la largeur de l’impulsion correspond à la consigne, par exemple un angle ou une vitesse.

```
Figure5.4 – Schéma d’un signal PWM
```
Le signal PWM est généré via des interruptions de l’horloge du processeur du microcontrôleur pour
contrôler l’instant où le front doit être montant ou descendant.

### 5.4 Tests et certifications de la solution

#### 5.4.1 Liaison série UART

Afin de certifier le bon fonctionnement de la liaison série, nous avons utilisé une carte arduino car
nous n’avions pas encore reçu la raspberry. Ainsi, via la carte arduino nous envoyons le message "test"
lorsque le bouton poussoir de l’arduino est pressé, afin d’être certain du moment où le message est
envoyé. Sur le microcontrôleur stm32 nous avons mis une led qui s’allume lorsque qu’il reçoit le message
"test". De plus nous avons bloqué le microcontrôleur dans une boucle de code infinie pour vérifier qu’il
en sort bien lors de l’interruption générée par la liaison UART.

```
Figure5.5 – Photo du dispositif du test de la liaison UART temps réel
```
#### 5.4.2 L’asservissement

Afin de valider que le signal PWM généré par le microcontrôleur pour asservir le servomoteur du
bras soit bon, nous avions utilisé un bouton poussoir qui a chaque pression modulait le signal pour que
le servomoteur tourne de 10°jusqu’à l’angle maximum avant de revenir à la position initiale.


Pour tester le fonctionnement de la turbine, nous l’avons connecté au microcontrôleur et à l’aide
d’un tube auquel on a ajouté un filtre (pour que le mégot ne rentre pas dans la turbine) nous avons
aspiré un mégot.

```
Figure5.6 – Photo du dispositif du test d’aspiration
```

## Chapitre 6

# Rendu final

Maintenant que nous avons décrit et réalisé les trois modules de notre système, il est question à présent
de regrouper toutes ces parties afin de présenter les résultats intégrants du projet.

### 6.1 Application mobile

Dans cette section nous allons parler de l’application mobile de contrôle à distance, dans un premier
temps, nous présenterons le fonctionnement, puis la manière dont nous l’avons implémenté et enfin
nous parlerons de nos tests et certifications.

#### 6.1.1 Avant-propos

Nous avons besoin d’avoir un moyen pour contrôler notre robot à distance et vu que les smartphones
sont plus populaires que jamais, ils présentent un outil intuitif et intéressant pour interagir avec le
robot : le contrôler, afficher son état et avoir des rapports sur son fonctionnement.

```
Pour cela, nous avons développé une application Android pour répondre à ce besoin.
```
```
Figure6.1 – Interaction Application Robot
```
### 6.2 Fonctionnalités de l’application

L’application propose trois fonctionnalités majeures.
— Connexion : permet de se connecter sur le réseau wifi émis par le robot.
— Contrôle : permet d’envoyer les commandes à notre robot.
— Logs : permet d’avoir des rapports sur le fonctionnement du robot notamment dans le cas
d’erreur ou de dysfonctionnement.


```
Figure6.2 – Écran d’accueil de l’application
```
Connexion

Pour notre système, nous allons utiliser la wifi émise par la raspberry 4. Nous nous connectons sur
ce réseau à travers un bouton dédiée sur l’écran de d’accueil de l’application.

```
La raspberry émettra la wifi en continue. La portée du réseau peut aller jusqu’à 30m.
```
Contrôle

A partir du menu Control nous pouvons envoyer des commandes vers notre robots. Plusieurs com-
mandes sont possibles.
— Start search : Démarrer la recherche des mégots.
— Stop : Arrêter la recherche des mégots.
— Start vacuum : Retourner à la position de départ.
— Up,Right et Left : Directions

```
Les messages envoyés à partir de l’application sont des codes à 2 numéros :
```
```
Type Sens Message envoyé Message reçu
Lancer le robot App ->Robot 10 ; Started
Lancer l’aspiration App ->Robot 11 ; Vaccum On
Stop App->Robot 12 ; Stoped
Directions App->Robot 90 ; 91 ; 92 ; Going straight..
```
```
Table6.1 – Protocole de communication
```
Logs

Cette partie nous permet d’avoir un accès au logs générés au cours de l’utilisation du robot. Ceci
nous permettra de corriger les erreurs et les dysfonctionnements plus facilement.

Tests et certifications

Nous avons pu interfacer notre application avec le serveur qu’on a mis sur Webots. Le robot répond
aux commandes reçus durant la simulation et renvoi des logs à l’application.


### 6.3 Le robot

#### 6.3.1 Assemblage du robot

Nous avons suivi notre schéma électrique pour réaliser l’assemblage. Nous avons rajouté une prise
bec à la batterie pour pouvoir la déconnecter/connecter plus aisément.

```
Figure6.3 – Responsibility - Photo 1
```
```
Figure6.4 – Responsibility - Photo 2
```
#### 6.3.2 Conception et fabrication des différentes pièces

Pour éviter les courts-circuits nous avons imprimé avec une imprimante 3D des coques pour la
raspberry Pi et le convertisseur de tension.
Nous avons conçu un support de servo-moteur adapté au chassis ServoCity Scout.

```
Figure6.5 – Conception du support pour le servomoteur
```

#### 6.3.3 Réglage et Ajustement

Réglage du régulateur de tension

Le régulateur de tension LM2596 est un abaisseur de tension de type Buck. C’est une alimentation
a découpage qui permet d’abaisser la tension d’entrée. La tension d’entrée peut être compris entre
4,5V et 40V et la tension de sortie entre 1,5V et 35V. Le réglage est réalisé grâce a un potentiomètre
intégré. Nous avons donc réglé la valeur de sortie à 5V.
L’alimentation de la raspberry et l’arduino a fonctionné correctement.

Lissage des tensions aux entrées des moteurs

Nous envoyons deux signaux PWM aux carte moteur :
— Pour la vitesse de rotation
— Un autre pour le sens, si le rapport cyclique est inférieur a 50% le moteur tournera dans un
sens et si il est supérieur le moteur tournera dans l’autre sens.
Nous avons remarqué lors de changement brusque de direction/vitesse, cela forçait sur les axes
moteurs. Nous avons donc lisser pour que l’accélération ce fasse progressivement.

```
Figure6.6 – Avant lissage
```
```
Figure6.7 – Après lissage
```
Actuellement nous découpons la translation entre deux valeurs en 5 pas, avec 100ms entre chaque
changement de valeur. Les accélérations se font bien plus progressivement sans forcer sur les axes.


#### 6.3.4 Schéma électrique

Nous avons réalisé le schéma électronique avec toute les connexions entre nos différents composant.
Pour cela nous avons utilisé l’outil EasyEDA qui est un outil de CAO pour électronique (EDA : Elec-
tronic design automation). Nous avons choisi d’utiliser un arduino à la place du SM32 pour l’instant.

```
La connexion entre le raspberry et l’arduino n’est pas affiché.
```
```
Figure6.8 – Schéma global
```
#### 6.3.5 Consommation théorique

Dans un projet embarqué, maîtriser la consommation est un point crucial. Ce document à donc pour
but de détailler la consommation de notre robot. Notre objectif est d’atteindre une autonomie entre
45 min et 2 heures.

Les composants du robot

Les consommations des composants du robot sont théorique et extraites des documentations officielles
ou de test.


```
Module Voltage Consommation
au repos
```
```
Source
```
```
Les moteurs 3V - 12VDC 0.19A https ://www.servocity.com/624-
rpm-premium-planetary-gear-
motors
La Raspberry Pi 4 - 4go 5V 0,56A https ://www.elektormagazine.fr/news/banc-
d-essai-raspberry-pi-4-tout-
nouveau-tout-beau-mais-encore-
au-top
STM32F103C8T6 5V 0.20mA
Turbine 5 - 24 V 600mA https ://fr.aliexpress.com/item/32917548330.html
RPLIDAR A2 5V 400mA
Capteurs ultrasons 5V 20mA
```
```
Table6.2 – Consommation des différents modules
```
Consommation globale

```
Moteurs :
4 ∗(0. 19 ∗ 1 .25)∗12 = 11, 4 W
```
[ On ajoute 25 % car la valeur était sans charge ]

```
Turbine :
(0, 6 ∗ 1 .25)∗12 = 9W
```
.[ On ajoute 25 % car la valeur était sans charge ]

```
Le lidar, les ultrasons, et le STM32 :
```
```
(0,4 + 4∗ 0 ,02 + 0,02)∗ 5 ∗ 1 ,1 = 2, 5 W
```
[+ les 10% de perte par l’abaisseur de tension ]

```
Raspberry PI
(1∗ 0 ,7 + 0, 56 ∗ 0 ,3)∗ 5 ∗ 1 .1 = 4, 34 W
```
[ Ici on estime qu’elles est 70% en activité intense et 30% au repos+ les 10% de perte par l’abaisseur
de tension ]

Notre système consomme environ

```
11 ,4 + 9 + 2,5 + 4,34 = 27Wh
```
.

Calcul de la capacité pour qu’une batterie tienne une heure sans la décharger en dessous de 20%.

```
12 ∗Capa∗ 0 .8 = 27 ⇐⇒ Capa= 2, 8 Ah
```
Avec une batterie de 12V 3000mAh on peut espérer produire au moins 27W et donc tenir une
heure.


#### 6.3.6 Consommation pratique

Nous avons réalisé les tests de consommation dans 3 situations. Pour cela nous avons utilisé un
multimètre USB Keiweisi il fonctionne avec un voltage entre 3-9V et entre 0-3A. Il se branche avant
entre l’alimentation et la raspberry.

Première situation : La raspberry est juste connecté sans aucun périphérique branché juste en
état "idle". La consommation du raspberry est stable après la phase de boot avec une consommation
de 500 mA.

```
Figure6.9 – Raspberry pi idle
```
Deuxième situation : La raspberry est branchée à un clavier, souris écran. La consommation est
stable autour de 1 A.

Troisième situation : La raspberry alimente les servos et la carte arduino, la consommation
dépend de l’activité des servos et est de environ 0.9 - 1.2 A.


## Chapitre 7

# Gestion de projet

Dans ce chapitre, nous allons présenter l’ensemble du déroulement et de la gestion de notre projet tout
le long de l’année à travers les différentes méthodes, répartitions et organisations mises en place.

### 7.1 Méthode de gestion

Dans cette section, nous allons présenter les aspects généraux de notre gestion de projet à travers son
cycle de vie ainsi que les aspects de planification.

#### 7.1.1 Cycle de vie agile

Le cycle de vie que nous avons opté tout le long de notre projet est le modèle agileASD7.1 (Adaptive
Software Development) :

```
Figure7.1 – Cycle de vie agile ASD
```
Ce modèle s’adapte très bien dans le cadre de notre projet, en effet, après une première phase
d’initialisation (définitions des besoins et des objectifs), des cycles de travail sont mis en place avec
une phase de planification concrète des tâches avec notre tuteur, suivi d’une phase de recherche et de
développement et enfin une phase de "contrôle", d’évaluation du travail réalisé avec ce même tuteur.

Ce modèle nous a ainsi permis d’avoir une grande souplesse dans les changements d’orientation
du projet, tout en ayant une certaine rapidité de manœuvre et de respect des délais en impliquant
beaucoup notre client (notre tuteur) dans les choix des pistes à suivre.

#### 7.1.2 Planification

Afin d’avoir une gestion des risques la plus optimale possible, nous avons planifié notre travail(releases,
tâches, documentation, versions intermédiaires...) de la manière suivante. Dans ce calendrier 7.1, nous


faisons aussi apparaître l’état final de réalisation des tâches ainsi que les détails qui mériteraient d’être
soulignés.

```
Date Intitulé Détails Etat de réalisation
22/09/19 Première prise de
contact avec notre
tuteur
```
```
Discussion sur le thème
et les objectifs du projet
```
```
01/10/19 Soumission du sujet
08/10/19 Première release Ce rendez-vous a prin-
cipalement porté sur la
création du cahier des
charges, et les directions
à prendre
22/11/19 Deuxième release Avancées plus confirma-
tions des choix choisie
05/12/19 Première version du
rapport
```
```
La structure générale
du rapport ainsi qu’une
première version du ca-
hier des charges doit
être réalisée
```
```
Réalisé en temps
```
```
21/12/19 Troisième release Dernières questions sur
le cahier des charges et
discussions sur les re-
cherches réalisées
09/01/20 Quatrième release POint d’avancement
projet + v1 rapport
29/01/20 Cinquième release Discussions sur les tests
réalisés et résultats ob-
tenus
8/02/20 Premier rendez-vous
gestion de projet
```
```
Discussion sur l’état
d’avancement du projet,
la gestion globale etc
19/02/20 Sixième release Discussions sur les tests
réalisés et résultats ob-
tenus
19/03/20 Septième release Discussions sur les amé-
liorations possibles ainsi
que sur la quantification
des performances
28/04/20 Pré-soutenance tech-
nique
```
```
Prendre en compte les
avis et critiques du jury
30/04/20 Huitième release Discussions sur la direc-
tion à prendre au vue de
la situation sanitaire
14/05/20 Neuvième release Etat d’avancement de la
nouvelle solution basée
sur webots
```

```
26/05/20 Neuvième release Discussions sur les diffé-
rents scénario à envisagé
pour la démonstrations
finale du projet
12/06/20 Rendu rapport final
12/06/20 Soutenance finale de
projet
12/06/20 Démonstration finale de
projet
```
```
Table7.1 – Calendrier de planification
```
### 7.2 Répartition des tâches

Comme nous avons pu le voir précédemment, notre projet s’axe autour de quatre grandes parties.
Il était donc tout naturel d’attribuer une partie à chaque membre de l’équipe. Le tableau 7.2 suivant
permet de mieux rendre compte de la répartition des différentes tâches concrètes et organisationnelles :

```
Abdel-Karim ARIF Nicolas DALLERAC Alaeddine HEDHLY Chakib Yousfi
Développement et docu-
mentation de la Recon-
naissance d’objet
```
```
Développement et docu-
mentation de la partie
Navigation
```
```
Développement et docu-
mentation de la Recon-
naissance d’objet
```
```
Développement et docu-
mentation de la partie
Navigation
Gestion des commandes
de matériel/Rédaction
du cahier des charges
```
```
Gestion des commandes
de matériel
```
```
Gestion des commandes
de matériel
```
```
Gestion des commandes
de matériel
```
```
Réalisation des slides de
pré-soutenance
```
```
Réalisation des slides de
pré-soutenance
```
```
Réalisation des slides de
pré-soutenance
```
```
Réalisation des slides de
pré-soutenance
Rédaction de l’introduc-
tion
```
```
Rédaction de la partie
gestion de projet
```
```
Rédaction de l’introduc-
tion
```
```
Rédaction de l’introduc-
tion
Rédaction de la conclu-
sion
```
```
Développement et docu-
mentation de la partie
rendu final (ihm)
```
```
Développement et docu-
mentation de la partie
rendu final (serveur et
communication)
```
```
Rédaction de la conclu-
sion
```
```
Réalisation des slides de
soutenance finale
```
```
Réalisation des slides de
soutenance finale
```
```
Réalisation des slides de
soutenance finale
```
```
Réalisation des slides de
soutenance finale
Prise de contact et de
rendez-vous avec les tu-
teurs
```
```
Table7.2 – Répartition des tâches entre les membres de l’équipe
```
Évolution Malgré cette répartition "préétablie", certains membres de l’équipe ont été amenés à
travailler ensemble (en pairprogramming), notamment lors du développement du dispositif sur le si-
mulateur webots entre Abdel-Karim et Nicolas et aussi lors de la réalisation du dispositif de contrôle
du robot entre Chakib et Nicolas.

### 7.3 Recueil et hiérarchisation des besoins du client

Comme nous avons pu le voir dans la section 7.1.2, nous avons effectué un certain nombre de rendez-
vous avec notre tuteur technique tout le long de l’année. La problématique qui ressort alors est :
comment bien recueillir et hiérarchiser les besoins exprimés par le client (notre tuteur dans ce cas) à
chaque à point d’avancement.


Ainsi, afin d’avoir une bonne gestion des risques et donc de ne pas passer à coté de certaines attentes
ou de négliger des aspects importants, nous avons mis en placeun système de compte-rendu,
l’ensemble des comptes rendus sont structurés d’une certaine manière avec dans un premier temps une
section "discussions" qui peut regrouper plusieurs thèmes comme la gestion de projet, le travail réalisé
par chaque membre (une section par chapitre technique), des idées de pistes pour le setup final etc.
L’important dans cette partie est de bien noter dans les détails et de regrouper toutes les informations
énoncées afin d’avoir des idées claires pour la suite. Enfin une dernière section prenant la forme d’une
liste, regroupe toutes les tâches à effectuer pour les membres de l’équipe et fait ainsi office de sorte de
résumé du rendez-vous. Pour illustrer nos propos, on peut regarder la photo suivante afin d’avoir un
aperçu typique de nos compte-rendu :

```
Figure7.2 – Sommaire de notre compte-rendu numéro 5
```
En plus de toute cette structuration, il faut prendre soin de recueillir les informations de la meilleure
des manières. En effet lors des rendez-vous, il faut prendre en note tous les éléments des divers discus-
sions sans pour autant rédiger le tout sur place, au risque de louper ou de ne pas saisir des éléments. La
meilleure chose à faire est de cerner les mots clefs/importants et de les noter de manière qu’à posteriori
(après le rendez-vous), la personne chargée du compte-rendu (le chef de projet) puisse le rédiger au
mieux.

### 7.4 Parallélisme du travail de développement assurée par une architecture avec modularisation

### tecture avec modularisation

Comme nous avons pu le dire dans la section 7.2 répartition des tâches, notre projet se subdivisant
en trois grands modules majeurs, chaque membre de l’équipe a pu travailler en parallèle sur un bloc du
projet. Ainsi chaque partie a pu avancer indépendamment avec ses propres recherches, documentations,


tests et programmes. On se retrouve ainsi avec trois "petits" projets indépendant ayant chacun leurs
objectifs et problématiques techniques.

Ensuite, l’architecture globale du système est fait de telle manière que les différents blocs peuvent
s’interconnecter facilement autour d’un sujet sans pour autant qu’il y ait une trop forte interdépendance
entre les modules et qu’un problème sur l’un impacte tous les autres. Cela, en plus de permettre de
gérer certains risques, permet d’avoir une bonne gestion de la qualité du système en garantissant une
certaine robustesse. Le schéma 7.3 permet de mettre en valeur cette pratique :

```
Figure7.3 – Schéma de l’architecture avec modularisation du système
```
Cette aspect de séparation en plusieurs modules s’est aussi retrouvé sur le dépôt du groupe (voir
section 7.5), où chaque membre avait son lieu/dossier de travail lui permettant de développer de manière
totalement autonome des autres et ainsi éviter tous conflits et garantir une certaine qualité de travail.

### 7.5 Utilisation des outils de gestion de projet au sein de l’équipe

L’utilisation de certains outils spécifiques peut grandement aider à la gestion d’un projet au sein
d’une équipe. Ils permettent en effet d’organiser et de structurer le travail d’une manière plus saine et
ainsi garantir des bonnes pratiques et une gestion de qualité.

Git [7] Tout d’abord, l’utilisation d’un dépot GitLab [9] est quelque chose de capital non seulement
afin de stocker le travail réalisé, mais aussi et surtout dans un soucis de synchronisation entre les
membres de l’équipe afin qu’ils puissent avoir accès à tout moment aux différentes ressources.

Bien entendu pour que tout cela fonctionne de la meilleur des manières, certaines règles doivent être
appliquées. Tout d’abord, le dépôt doit avoir une certaine hiérarchie avec des dossiers et sous-dossiers
contenant la documentation, les différentes parties, les tests etc. Ensuite, afin de limiter au plus les
problèmes de conflits, les utilisateurs doivent autant que possible éviter de travailler sur un même
fichier/partie en même temps. Ce problème est solutionné en grande partie grâce à l’architecture avec
modularisation (cf section 7.4). Enfin, il est important de "commit" régulièrement, c’est à dire dès
qu’une partie d’une tâche est réalisée, pour des raisons de sécurité déjà ainsi que pour laisser une trace
de travail réalisé.


Pour terminé, le git nous permet aussi de garder une trace de l’évolution du travail réalisé et d’obtenir
des statistiques sur la contribution de l’équipe comme le montre le graphique 7.4 ci-desous :

```
Figure7.4 – Contribution de l’équipe du 28 octobre 2019 au 02 juin 2020
```
A noter aussi qu’afin de faciliter l’utilisation du git, nous avons utilisé le logiciel GitKraken [8], qui
est une interface graphique permettant d’avoir accès au dépôt et d’y faire les différentes manipulations
de manière plus commode.

Autres outils Afin de communiquer en dehors des cours, nous avons crée une conversation de groupe
messenger [4], nous permettant entre autre d’organiser des rendez-vous, de se donner des "deadlines"
ou bien tout simplement de poser des questions d’ordre technique.

Nous avons aussi mis un en place un système de journal de bord à l’aide du site trello [19], servant
à regrouper toutes les taches à faire, en cours ou réalisées par chaque membre de l’équipe. Pour cela
chaque personne n’avait qu’à ajouter des nouveaux items à partir de la liste présente à chaque fin de
compte-rendu de rendez-vous (cf section 7.3). Ensuite, au fil de l’évolution de la tâche, il suffisait de
la déplacer d’un tableau à un autre.

Enfin, pour ce qui est de la réalisation des slides des différentes soutenances, nous avons utilisé l’outil
collaboratif en ligne Google Slides [14], qui permet de travailler en temps réel sur un même document
et ainsi gagner en efficacité.
L’utilisation dans l’équipe des outils cités précédemment est résumée dans le schéma 7.5 suivant :

```
Figure7.5 – Résumé des outils de gestion utilisés
```

## Chapitre 8

# Conclusion et perspectives

Dans ce dernier chapitre, nous allons dans un premier temps conclure sur le projet en synthétisant
le travail réalisé par rapport à ce qu’il était prévu initialement. Dans un second temps, nous allons
présenter les extensions possibles pour notre projet.

### 8.1 Conclusion

Ce projet étant le dernier de notre formation, nous voulions qu’il soit ambitieux et à la hauteur de
ces 5 années. C’est pourquoi nous avons choisis de s’orienter vers des sujets de l’actualité, à savoir l’en-
vironnement tout en conservant un challenge technique tel que les voitures autonomes. C’est pourquoi
l’objectif de ce projet était de concevoir, développer et monter un robot autonome pour qu’il puisse
naviguer en milieu urbain, détecter et collecter les mégots de cigarettes.

Nous savons que la réussite d’un projet nécessite une base solide mais surtout qu’aucune étape ne
soit négligée, surtout lorsque le projet demande des compétences transverses en intelligence artificielle,
traitement d’image, et robotique. C’est pourquoi, nous avons consacré beaucoup de temps et d’impor-
tance à l’étape de conception, ainsi que le bonne définition des tâches afin d’anticiper un maximum les
problèmes que nous pourrions rencontrer. Notamment pour mener à bien le projet qui nécessitais du
matériel, il fallait donc prendre en compte les contraintes temporelles liées au délais de la réception du
nécessaire.

Ainsi, nous avons pu extraire du cahier des charges préalablement rédigé, trois grandes parties qui
sont, la vision pour reconnaître un objet, le déplacement pour qu’il navigue en conservant son intégrité,
ainsi que l’asservissement pour mettre en action le robot. Nous avons donc naturellement suivis ce
schéma pour répartir le travail, ce qui à permis de faire avancer ces trois thématiques parallèlement.

Pourle déplacement du robot, nous avons choisi d’utiliser quatre roues qui seront tournées
avec motoréducteurs. Pour l’évitement d’obstacle, nous avons expérimenté avec plusieurs méthodes :
braitenberg, l’histogramme et le qlearning. Nous avons testé ces solutions sur le simulateur Webots.
Suite aux tests nous avons décidé d’utiliser le qlearning pour notre système.

En ce qui concernela reconnaissance d’objet, nous avons tester quatre méthodes. Trois parmi
lesquelles sont des méthodes de traitement d’image : transformée de Hough généralisée, segmentation
par couleur et les probabilités d’histogramme. La dernière méthode est une solution d’intelligence
artificielle utilisant YOLO. Après avoir testé ces solutions, nous avons choisi la méthode des probabilité
d’histogramme car elle nous a donné une bonne reconnaissance du mégot et elle n’était pas lourde pour
notre système.

En ce qui concernel’asservissement, nous avons utilisé un microcontrôleur pour laisser le raspberry
ce dédier à la prise de réflexion en déléguant l’asservissement via une liaison UART. Nous avons utilisé
les interruptions du microcontrôleur pour garantir que le robot bénéficie d’un system temps.


Malheureusement, suite à la crise du Covid-19 nous nous sommes retrouvé dans l’impossibilité de
poursuivre le projet dans les locaux de l’université, alors que nous n’avions pas reçus l’intégralité
du matériel nécessaire. Cet évènement est venue nous imposer un réel challenge. Nous nous somme
retrouvé entravé dans notre progression, avec toujours la même échéance. Nous avons dû donc, prendre
rapidement des décisions sur le chemin que nous allions emprunter tout en souhaitant conserver l’esprit
du projet initial. C’est pourquoi nous sommes passé sur le simulateur Webot. Cette mésaventure qui
a affecté à la fois notre vie personnel et le déroulement du projet nous aura permis de ressortir plus
grand et plus soudé.

### 8.2 Perspectives

Nous allons à présent nous pencher sur les extensions possibles de chaque partie en nous concentrons
sur les plus pertinentes et en précisant le niveau de difficulté en temps et en technique de chacune.

Naturellement la première évolution réalisable serais de finir l’implémentation physique et le montage
du robot afin de concrétiser nos travaux sur le simulateur.

La seconde amélioration pourrait être une amélioration de la géolocalisation, afin d’avoir une resti-
tution du parcours effectué et de pouvoir programmer un parcours également. Ceci permettrait en plus
de d’ouvrir le champ des fonctionnalités envisageables. Nous pouvons aussi ajouter d’autres fonction-
nalités dans l’application, comme un streaming de la caméra du robot et porter l’application elle-même
vers les systèmes iOS.

Nous pouvons également expérimenter plus avec des solutions d’intelligence artificielle qui peuvent
être plus précises en terme de reconnaissance d’objet. Cela dit, il faudra bien évidement l’optimiser
pour notre système car il est limité en terme de ressources matérielles.

Finalement, on pourrait imaginer de développer un second robot qui, plutôt qu’être équipé d’un
aspirateur, il pourrait être équipé d’un diffuseur, qui serait utile pour par exemple diffuser du désin-
fectant pour les crises tel que l’on a connu avec le covid-19. Mais qui pourrait également disperser du
sel sur les trottoirs l’hiver pour éviter la chute des piétons.


## Bibliographie

```
[1] Massimo Banzi. Arduino. https ://www.arduino.cc/.
```
```
[2] Microsoft Corporation. Microsoft windows, 1985. https ://www.microsoft.com/fr-fr/windows.
```
```
[3] IDE Eclipse. The eclipse foundation, 2007.
```
```
[4] Facebook. Messenger, 2011. https ://www.messenger.com/.
```
```
[5] Freedesktop.org. Gstreamer, 1999. https ://gstreamer.freedesktop.org/.
```
```
[6] David Gilbert. Jfreechart, 2005. http ://www.jfree.org/jfreechart/.
```
```
[7] Git. http ://www.eclipse.github.io.
```
```
[8] Gitkraken, 2014. https ://www.gitkraken.com/.
```
```
[9] gitlab, 2008. https ://www.gitlab.com.
```
[10] GNU. Timidity. https ://doc.ubuntu-fr.org/timidity.

[11] Google. Draw.io. https ://www.draw.io.

[12] Intel. Opencv, 2000. https ://opencv.org/.

[13] Latex. https ://www.latex-project.org.

[14] Google LLC. Google slides, 2006. https ://www.google.fr/intl/fr/slides/about/.

[15] Sun Microsystems. Java, 1995. http ://www.java.sun.com/.

[16] Sun Microsystems. Swing, 2000. https ://docs.oracle.com/javase/7/docs/api/javax/swing/package-
summary.html.

[17] Joseph Redmon. Yolo.

[18] Dennis Ritchie. C, 1972.

[19] Fog Creek Software. Trello, 2011. https ://trello.com/.

[20] Richard Stallman. Linux, 1991. https ://www.kernel.org/.

[21] Guido van Rossum. Python, 1991. https ://www.python.org/.

[22] Guido van Rossum. Idle, 1998. https ://docs.python.org/3/library/idle.html.


