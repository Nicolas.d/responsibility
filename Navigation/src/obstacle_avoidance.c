
#include <webots/lidar.h>
#include <webots/motor.h>
#include <webots/robot.h>

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define TIME_STEP 64
#define BASE_SPEED 1.5

clock_t t1, t2;

double gaussian(double x, double mu, double sigma) {
  return (1.0 / (sigma * sqrt(2.0 * M_PI))) * exp(-((x - mu) * (x - mu)) / (2 * sigma * sigma));
}

void braitenberg(WbDeviceTag left_motor,WbDeviceTag right_motor,WbDeviceTag lidar){

  const int lidar_width = wb_lidar_get_horizontal_resolution(lidar);
  const double lidar_max_range = wb_lidar_get_max_range(lidar);

  double *braitenberg_coefficients = (double *)malloc(sizeof(double) * lidar_width);
  int i;
  for (i = 0; i < lidar_width; i++)
    braitenberg_coefficients[i] = 6 * gaussian(i, lidar_width / 4, lidar_width / 12);

  while (wb_robot_step(TIME_STEP) != -1) {
    t1 = clock();
    double left_speed = BASE_SPEED, right_speed = BASE_SPEED;

    const float *lidar_values = wb_lidar_get_range_image(lidar);
    for (i = 0.25 * lidar_width; i < 0.5 * lidar_width; i++) {
      const int j = lidar_width - i - 1;
      const int k = i - 0.25 * lidar_width;
      left_speed +=
        braitenberg_coefficients[k] * ((1.0 - lidar_values[i] / lidar_max_range) - (1.0 - lidar_values[j] / lidar_max_range));
      right_speed +=
        braitenberg_coefficients[k] * ((1.0 - lidar_values[j] / lidar_max_range) - (1.0 - lidar_values[i] / lidar_max_range));
    }

    t2 = clock();
    printf("%lf\n",(double)(t2-t1)/(double)CLOCKS_PER_SEC );
    wb_motor_set_velocity(left_motor, left_speed);
    wb_motor_set_velocity(right_motor, right_speed);
  };
}

/*
  Method by histogram
*/

void histogram(WbDeviceTag left_motor,WbDeviceTag right_motor,WbDeviceTag lidar){

  const int lidar_width = wb_lidar_get_horizontal_resolution(lidar);
  const double lidar_max_range = wb_lidar_get_max_range(lidar);
  const int discretisation_zones = 18;
  int discretisation_pas = lidar_width / discretisation_zones;

  float histogramme[discretisation_zones];
  int left_stim, right_stim;


  int i,index_zone = 0,pas=0,min,id_min;
  printf("START\n");
  while (wb_robot_step(TIME_STEP) != -1) {
    wb_robot_step(TIME_STEP);
    t1 = clock();
    double left_speed = BASE_SPEED, right_speed = BASE_SPEED;

    const float *lidar_values = wb_lidar_get_range_image(lidar);

    for (i = 0; i <  discretisation_zones; i++) {

      histogramme[i] = 0;

    }

    left_stim = 0;
    right_stim = 0;

    index_zone = 0;
    for (i = 0; i <  lidar_width; i++) {
      //printf("aa %d \n",i%10);
      if(i%discretisation_pas == 0 && i!=0){
        index_zone++; // zone changement
      }
      //printf("i %d lidar %f zone %d %d \n",i,lidar_values[i],index_zone,histogramme[index_zone] );
      histogramme[index_zone] = histogramme[index_zone] + lidar_values[i];

    }
    id_min = 0;
    min = 900;
    for (index_zone = 0; index_zone <  discretisation_zones; index_zone++) {

      //printf("zone %d %f \n",index_zone,histogramme[index_zone] );
      if(min > histogramme[index_zone]){
        min = histogramme[index_zone];
        id_min = index_zone;
      }
    }
    //printf("min %d\n",id_min);
    if(id_min>8){
      right_stim =1;
    } else if (id_min<8){
      left_stim =1;
    }


    t2 = clock();
    printf("%lf\n",(double)(t2-t1)/(double)CLOCKS_PER_SEC );
    wb_motor_set_velocity(left_motor, 1 + left_stim);
    wb_motor_set_velocity(right_motor, 1 + right_stim);
  };

}

int main(int argc, char **argv) {
  wb_robot_init();

  WbDeviceTag lidar = wb_robot_get_device("LDS-01");
  wb_lidar_enable(lidar, TIME_STEP);
  wb_lidar_enable_point_cloud(lidar);

  WbDeviceTag lidar_main_motor = wb_robot_get_device("LDS-01_main_motor");
  WbDeviceTag lidar_secondary_motor = wb_robot_get_device("LDS-01_secondary_motor");
  wb_motor_set_position(lidar_main_motor, INFINITY);
  wb_motor_set_position(lidar_secondary_motor, INFINITY);
  wb_motor_set_velocity(lidar_main_motor, 30.0);
  wb_motor_set_velocity(lidar_secondary_motor, 60.0);

  WbDeviceTag right_motor = wb_robot_get_device("right wheel motor");
  WbDeviceTag left_motor = wb_robot_get_device("left wheel motor");
  wb_motor_set_position(right_motor, INFINITY);
  wb_motor_set_position(left_motor, INFINITY);
  wb_motor_set_velocity(right_motor, 0.0);
  wb_motor_set_velocity(left_motor, 0.0);

  //braitenberg(left_motor,right_motor,lidar);
  histogram(left_motor,right_motor,lidar);


  wb_robot_cleanup();

  return 0;
}
