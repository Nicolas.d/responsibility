
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <webots/keyboard.h>
#include <webots/robot.h>
#include <webots/keyboard.h>
#include <webots/motor.h>
#include <webots/position_sensor.h>
#include <webots/robot.h>
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <webots/keyboard.h>
#include <webots/motor.h>
#include <webots/position_sensor.h>
#include <webots/robot.h>
#include <webots/camera.h>
#include <webots/gps.h>
#include <webots/robot.h>
#include <webots/supervisor.h>
#include <webots/lidar.h>

#define TIME_STEP 64

#define BASE_SPEED 2


double gaussian(double x, double mu, double sigma) {
  return (1.0 / (sigma * sqrt(2.0 * M_PI))) * exp(-((x - mu) * (x - mu)) / (2 * sigma * sigma));
}


/*
  Method with braitenberg
*/


void braitenberg(WbDeviceTag left_motor,WbDeviceTag right_motor,WbDeviceTag lidar){
  
  const int lidar_width = wb_lidar_get_horizontal_resolution(lidar);
  const double lidar_max_range = wb_lidar_get_max_range(lidar);
 
  double *braitenberg_coefficients = (double *)malloc(sizeof(double) * lidar_width);
  int i;
  for (i = 0; i < lidar_width; i++)
    braitenberg_coefficients[i] = 6 * gaussian(i, lidar_width / 4, lidar_width / 12);

  while (wb_robot_step(TIME_STEP) != -1) {
    double left_speed = BASE_SPEED, right_speed = BASE_SPEED;

    const float *lidar_values = wb_lidar_get_range_image(lidar);
    for (i = 0.25 * lidar_width; i < 0.5 * lidar_width; i++) {
      const int j = lidar_width - i - 1;
      const int k = i - 0.25 * lidar_width;
      left_speed +=
        braitenberg_coefficients[k] * ((1.0 - lidar_values[i] / lidar_max_range) - (1.0 - lidar_values[j] / lidar_max_range));
      right_speed +=
        braitenberg_coefficients[k] * ((1.0 - lidar_values[j] / lidar_max_range) - (1.0 - lidar_values[i] / lidar_max_range));
    }
    
    // t2 = clock();
    // printf("%lf\n",(double)(t2-t1)/(double)CLOCKS_PER_SEC );
    wb_motor_set_velocity(left_motor, left_speed);
    wb_motor_set_velocity(right_motor, right_speed);
  };
}


int main() {

  /*** INIT ***/
  srand(time(NULL));
  wb_robot_init();
  WbDeviceTag _camera;
  _camera = wb_robot_get_device("camera");
  wb_camera_enable(_camera, 64);
  
  
  WbDeviceTag lidar = wb_robot_get_device("LDS-01");
  wb_lidar_enable(lidar, TIME_STEP);
  wb_lidar_enable_point_cloud(lidar);

  WbDeviceTag lidar_main_motor = wb_robot_get_device("LDS-01_main_motor");
  WbDeviceTag lidar_secondary_motor = wb_robot_get_device("LDS-01_secondary_motor");
  wb_motor_set_position(lidar_main_motor, INFINITY);
  wb_motor_set_position(lidar_secondary_motor, INFINITY);
  wb_motor_set_velocity(lidar_main_motor, 30.0);
  wb_motor_set_velocity(lidar_secondary_motor, 60.0);

  WbDeviceTag right_motor = wb_robot_get_device("wheel1");
  WbDeviceTag left_motor = wb_robot_get_device("wheel2");
  wb_motor_set_position(right_motor, INFINITY);
  wb_motor_set_position(left_motor, INFINITY);
  wb_motor_set_velocity(right_motor, 0.0);
  wb_motor_set_velocity(left_motor, 0.0);
  

  braitenberg(left_motor,right_motor,lidar);

  
  printf("a");
  wb_robot_cleanup();


  return 0;
}

