#include <iostream> 
#include <thread> 
#include <unistd.h>
#include <time.h>
using namespace std; 

pthread_mutex_t mutexAddOrder;

int cpt = 0;

void Serial(int Z) 
{ 
    for (int i = 0; i < 100000; i++) { 
        
        pthread_mutex_lock(&mutexAddOrder);

        cpt++;

        pthread_mutex_unlock(&mutexAddOrder);
    } 
    
} 

void Vision(int Z) 
{ 
    for (int i = 0; i < 100000; i++) { 
        
        pthread_mutex_lock(&mutexAddOrder);

        cpt++;

        pthread_mutex_unlock(&mutexAddOrder);
    } 
} 

void Subsumption(int Z) 
{ 
    for (int i = 0; i < 100000; i++) { 
        
        pthread_mutex_lock(&mutexAddOrder);

        cpt++;

        pthread_mutex_unlock(&mutexAddOrder);
    } 
} 
  
 

int main() 
{ 
  
    pthread_mutex_init(&mutexAddOrder, NULL);

    thread th1(Serial, 3); 
    thread th2(Vision, 3); 

    th1.join();
    th2.join();
  
    cout << " \n " << cpt;
    return 0; 
} 