/*
Author : Nicolas DALLERAC
Date : 1/11/19
Header of robot
*/

#ifndef __ROBOT_H
#define __ROBOT_H

class Robot{
	public :
		// Name of the robot
		const char* _name;
	public :
		// constructor
		Robot(const char* _name);
		// Function for init the robot
		void init();

};

#endif