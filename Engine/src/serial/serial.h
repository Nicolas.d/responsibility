#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>



typedef struct{
	char *port;
	int descripteur;
} serial_com;

void serial_open(serial_com*, char*);
void serial_close(serial_com*);
void serial_read(serial_com*, char*);
void serial_read_mot(serial_com*, char*);
void serial_write(serial_com*, char*);
void serial_write_entier(serial_com*, int*);

void serial_open(serial_com *sp, char *name){
	printf("open \n");
	(*sp).descripteur = open("/dev/tty.usbmodem14101", O_RDWR );// | O_NOCTTY|O_NONBLOCK 
	(*sp).port = name;
	printf("DESC %d ",(*sp).descripteur);
}


void serial_close(serial_com *sp){
	close((*sp).descripteur);
}


void serial_read(serial_com* sp, char *buff){
    int a;
    a = read((*sp).descripteur, buff, 1);
	
	if(a!=-1){
		printf("nombre de caracteres : %d caractere lu : %s\n", a, buff);
	}else{
		printf("read ok\n");
	}
}

void serial_read_mot(serial_com* sp, char *buff){
    char controle = 'o';
	serial_write(sp, &controle);
	sleep(1);
	int a;
    a = read((*sp).descripteur, buff, 7);
}


void serial_write(serial_com *sp, char *buff){
    int a = write ((*sp).descripteur, buff, strlen(buff));
	if(a<1){
		printf("Erreur\n");
	}
	else{
		printf("%s a été écrit \n", buff);
	}
}


void serial_write_entier(serial_com *sp, int *buff){
    int a = write ((*sp).descripteur, buff, 1);
	if(a<1){
		printf("Erreur\n");
	}
	else{
		printf("%d a été écrit\n", *buff);
	}
}

