#!/usr/bin/env bash
cd /boot
touch SSH

chmod +x *.sh
./download-opencv.sh
./install-deps.sh
./build-opencv.sh
cd ~/opencv/opencv-4.1.2/build
sudo make install